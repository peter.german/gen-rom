/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    PrecDEIM

Description
    Class with the implementation of the Discrete Empirical Interpolation
    Method for the precursor solvers (PrecDEIM).

\*---------------------------------------------------------------------------*/

void Foam::PrecDEIM::saveCrossMassTensor()
{
    word folder("ROMFiles/redOperators/PrecDEIM/");
    mkDir(folder);

    forAll(crossMassTensorT_, zIndex)
    {
        for(label fromIndex=0; fromIndex < xsDB_.noEnergyGroups(); fromIndex++)
        {
            forAll(crossMassTensorT_[zIndex][fromIndex], bIndex)
            {
                OFstream os
                (
                    folder + "crossMassTensorT_"+name(selfGroupNo_)+"_"+name(zIndex)+"_"+name(fromIndex)+"_"+name(bIndex)+"_"+name_, 
                    runTime_.writeFormat()
                );
                os << crossMassTensorT_[zIndex][fromIndex][bIndex];
            }   
        }
    }
}

void Foam::PrecDEIM::readCrossMassTensor()
{
    word folder("ROMFiles/redOperators/PrecDEIM/");

    crossMassTensorT_.clear();
    crossMassTensorT_.resize(xsDB_.matZones().size());

    forAll(crossMassTensorT_, zIndex)
    {
        crossMassTensorT_[zIndex].resize(xsDB_.noEnergyGroups());

        for(label fromIndex=0; fromIndex < xsDB_.noEnergyGroups(); fromIndex++)
        {
            crossMassTensorT_[zIndex][fromIndex].resize(ownBase_.size());
            forAll(ownBase_, bIndex)
            {
                IFstream is
                (
                    folder + "crossMassTensorT_"+name(selfGroupNo_)+"_"+name(zIndex)+"_"+name(fromIndex)+"_"+name(bIndex)+"_"+name_, 
                    runTime_.writeFormat()
                );
                is >> crossMassTensorT_[zIndex][fromIndex][bIndex];
            }
        }
    }
}

// ************************************************************************* //
