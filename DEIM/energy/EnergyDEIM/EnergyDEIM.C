/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    EnergyDEIM

Description
    Class with the implementation of the Discrete Empirical Interpolation
    Method for the energy solvers (EnergyDEIM).

\*---------------------------------------------------------------------------*/

#include "EnergyDEIM.H"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

//- Construct using all of the matrices
Foam::EnergyDEIM::EnergyDEIM
(
    fvMesh& baseMesh,
    Time& runTime,
    const XSDataBase& xsDataBase
)
    :
    ScalarDEIM(baseMesh, runTime),
    xsDB_(xsDataBase)
{
    if(xsDB_.fastNeutrons())
        name_ = "logTNeutro";
    else
        name_ = "sqrtTNeutro";
}

Foam::EnergyDEIM::EnergyDEIM
(
    fvMesh& baseMesh,
    Time& runTime,
    const XSDataBase& xsDataBase,
    word name
)
    :
    ScalarDEIM(baseMesh, runTime),
    xsDB_(xsDataBase)
{
    name_=name;
}
 
// * * * * * * * * * * * * * * * * Destructors * * * * * * * * * * * * * * * //

Foam::EnergyDEIM::~EnergyDEIM()
{}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * Public Member Functions  * * * * * * * * * * * //

void Foam::EnergyDEIM::createCrossMassTensorT(autoPtr<meshToMesh>* mapping)
{
    const PtrList<scalar>& ff = xsDB_.fuelFraction();

    if(!((*mapping).valid()))
    {
        Foam::error e("Cannot create cross-flux tensor, initalie mapping first!");
        e.exit(100);
    }

    const fvMesh& fluidMesh((*TBase_)[0].mesh());

    crossMassTensorT_.clear();
    crossMassTensorT_.resize(xsDB_.noMatZones());

    forAll(xsDB_.matZones(), zoneI)
    {
        label zone = zoneI;
        const word& zoneName = xsDB_.matZones()[zone].keyword();
        label zoneID = baseMesh_.cellZones().findZoneID(zoneName);

        crossMassTensorT_[zone].clear();
        crossMassTensorT_[zone].resize(xsDB_.noEnergyGroups());

        tmp<volScalarField> invFuelFractionTmp
        (
            new volScalarField
            (
                IOobject
                (
                    "invFuelFraction",
                    runTime_.timeName(),
                    baseMesh_,
                    IOobject::NO_READ,
                    IOobject::NO_WRITE
                ),
                baseMesh_,
                dimensionedScalar("", dimensionSet(0,0,0,0,0,0,0), 0.0),
                zeroGradientFvPatchScalarField::typeName
            )
        );
        volScalarField& invFuelFraction = invFuelFractionTmp.ref();            
           
        forAll(baseMesh_.cellZones()[zoneID], cellIlocal)
        {
            label cellIglobal = baseMesh_.cellZones()[zoneID][cellIlocal];
            invFuelFraction[cellIglobal] = min(1.0/ff[zone], 1.0/SMALL);
        }
        invFuelFraction.correctBoundaryConditions();

        for(label fromI=0; fromI < xsDB_.noEnergyGroups(); fromI++)
        {
            crossMassTensorT_[zone][fromI].resize(ownBase_.size());

            forAll(ownBase_, TbaseI)
            {
                crossMassTensorT_[zone][fromI][TbaseI].setSize
                (
                    TBase_->size(),
                    crossFluxBases_[fromI]->size()
                );
                crossMassTensorT_[zone][fromI][TbaseI] = 
                              crossMassTensorT_[zone][fromI][TbaseI] * 0.0;

                forAll((*crossFluxBases_[fromI]), baseIndex1)
                {
                    tmp<volScalarField> tempField
                    (
                        invFuelFraction * ownBase_[TbaseI]*(*crossFluxBases_[fromI])[baseIndex1]
                    );

                    volScalarField tempFieldFluid
                    (
                        IOobject
                        (
                            "tempField",
                            runTime_.timeName(),
                            fluidMesh,
                            IOobject::NO_READ,
                            IOobject::NO_WRITE
                        ),
                        fluidMesh,
                        dimensionedScalar("", dimensionSet(0,0,0,0,0,0,0), 0.0),
                        zeroGradientFvPatchScalarField::typeName
                    );
    
                    (*mapping)().mapTgtToSrc
                    (
                        tempField.ref(), 
                        plusEqOp<scalar>(), 
                        tempFieldFluid.primitiveFieldRef()
                    );
                    tempFieldFluid.correctBoundaryConditions();

                    forAll(*TBase_, baseIndex2)
                    {
                        crossMassTensorT_[zone][fromI][TbaseI][baseIndex2][baseIndex1] = 
                            POD::projection
                            (
                                (*TBase_)[baseIndex2],
                                tempFieldFluid
                            );
                    }

                    tempField.clear();
                }
            }
        }

        invFuelFractionTmp.clear();
    }

    saveCrossMassTensor();
}

void Foam::EnergyDEIM::calcCoeffs()
{
    RectangularMatrix<scalar> ownTemp(ownBase_.size(),1);
    RectangularMatrix<scalar> nonlinTemp(ownBase_.size(),1);

    List<scalar> T(ownBase_.size(), 0.0);
    forAll(redNonlinBase_, baseIndex)
    {
         T = T + (*redNonlinSol_)[baseIndex]*redNonlinBase_[baseIndex];
    }

    forAll(T, eIndex)
    {
        if(xsDB_.fastNeutrons())
            nonlinTemp[eIndex][0] = log(T[eIndex]);
        else
            nonlinTemp[eIndex][0] = sqrt(T[eIndex]);
    }

    multiply(ownTemp, redInterpolMx_, nonlinTemp);

    summCoeffs_ = ROMUtilities::matrixToList(ownTemp); 
}

void Foam::EnergyDEIM::calcCoeffs(std::function<scalar(scalar)> evalFunction)
{
    RectangularMatrix<scalar> ownTemp(ownBase_.size(),1);
    RectangularMatrix<scalar> nonlinTemp(ownBase_.size(),1);

    List<scalar> T(ownBase_.size(), 0.0);
    forAll(redNonlinBase_, baseIndex)
    {
         T = T + (*redNonlinSol_)[baseIndex]*redNonlinBase_[baseIndex];
    } 
    forAll(T, eIndex)
    {
        nonlinTemp[eIndex][0] = evalFunction(T[eIndex]);
    }

    multiply(ownTemp, redInterpolMx_, nonlinTemp);

    summCoeffs_ = ROMUtilities::matrixToList(ownTemp); 
}

void Foam::EnergyDEIM::sumTensors(bool adjoint)
{
    crossMassTensor_.clear();
    crossMassTensor_.resize(xsDB_.noMatZones());

    label TBaseSize = TBase_->size();

    if(!adjoint)
    {
        forAll(xsDB_.matZones(), zoneI)
        {
            label zone = zoneI;
    
            crossMassTensor_[zone].resize(xsDB_.noEnergyGroups());
    
            for(label fromI=0; fromI < xsDB_.noEnergyGroups(); fromI++)
            {
                label crossSize = crossFluxBases_[fromI]->size();
    
                crossMassTensor_[zone][fromI] = RectangularMatrix<scalar>(TBaseSize, crossSize, 0.0);
    
                forAll(ownBase_, TbaseI)
                {
                    crossMassTensor_[zone][fromI] = crossMassTensor_[zone][fromI]
                                        + crossMassTensorT_[zone][fromI][TbaseI]*summCoeffs_[TbaseI];
                }
            }
        }
    }
}

#include "EnergyDEIMIO.C"

// ************************************************************************* //
