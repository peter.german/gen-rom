/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    EnergyDEIM

Description
    Class with the implementation of the Discrete Empirical Interpolation
    Method for the energy solvers (EnergyDEIM).

Author
    Peter German, peter.german@tamu.edu, grmnptr@gmail.com

SourceFiles
    EnergyDEIM.C

\*---------------------------------------------------------------------------*/

#ifndef EnergyDEIM_H
#define EnergyDEIM_H

#include <functional>
#include "meshToMesh.H"

#include "volFields.H"
#include "fvCFD.H"
#include "fvMatrixExt.H"
#include "RectangularMatrix.H"
#include "POD.H"
#include "ScalarDEIM.H"
#include "XSDataBase.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class Reduced Model Declaration
\*---------------------------------------------------------------------------*/


class EnergyDEIM : public ScalarDEIM
{

protected:

    // Private Attributes

        // Pointers to temperature bases
        PtrList<volScalarField>* TBase_;

        // Basis vectors corresponding to the cross flux bases
        List<PtrList<volScalarField>* > crossFluxBases_;
    
        // Zone/crossGroup/temperatureMode
        List<List<List<RectangularMatrix<scalar> > > > crossMassTensorT_;
        List<List<RectangularMatrix<scalar> > > crossMassTensor_;
    
         // Cross-section data base
        const XSDataBase& xsDB_;

    // Private Member Functions    

public:

    // Constructors

        EnergyDEIM
        (
            fvMesh& baseMesh,
            Time& runTime,
            const XSDataBase& xsDataBase
        );

        EnergyDEIM
        (
            fvMesh& baseMesh,
            Time& runTime,
            const XSDataBase& xsDataBase,
            word name
        );

    // Destructor

        ~EnergyDEIM();

    // Public Member Functions

        //- Calculating summ coefficients
        void calcCoeffs();

        //- Calculating summ coefficients with given function
        void calcCoeffs(std::function<scalar(scalar)> evalFunction);

        //- Summing the temsors with available coefficients
        void sumTensors(bool adjoint=false);

        //- Creating cross-group fission tensor
        void createCrossMassTensorT(autoPtr<meshToMesh>* mapping);

        //- Updating the bases of the cross fluxes
        void updateCrossFluxBases
        (
            List<PtrList<volScalarField>* > crossFluxBases
        )
        {
            crossFluxBases_ = crossFluxBases;
        }

        //- Updating the bases of the temperature
        void updateTBase
        (
            PtrList<volScalarField>* TBase
        )
        {
            TBase_ = TBase;
        }

        const List<List<RectangularMatrix<scalar> > >& crossMassTensor() const 
        {
            return crossMassTensor_;
        }

        #include "EnergyDEIMIO.H"

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
