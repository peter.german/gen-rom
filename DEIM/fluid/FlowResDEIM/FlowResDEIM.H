/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    FlowResDEIM

Description
    Class with the implementation of the Discrete Empirical Interpolation
    Method for the flow resistances (FlowResDEIM).

Author
    Peter German, peter.german@tamu.edu, grmnptr@gmail.com

SourceFiles
    FlowResDEIM.C

\*---------------------------------------------------------------------------*/

#ifndef FlowResDEIM_H
#define FlowResDEIM_H

#include "fvCFD.H"
#include "RectangularMatrix.H"
#include "VectorDEIM.H"
#include "ROMUtilities.H"

#include "FluidPropertiesDataBase.H"
#include "PorousMediumDataBase.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class Reduced Model Declaration
\*---------------------------------------------------------------------------*/


class FlowResDEIM : public VectorDEIM
{

private:

    // Private Attributes

        List<List<scalar> > redFlowResSet_;

        List<scalar> redFlowRes_;

        //- Viscosity at the selected cells
        List<scalar> mu_;

        //- Density at the selected cells
        List<scalar> rho_;

        //- Hydraulic diameter at the selected cells
        List<scalar> D_;

        //- Void fraction at the selected cells
        List<scalar> gamma_;

        //- Laminar A at the selected cells
        List<scalar> darcyConst_;

        //- Laminar B at the selected cells
        List<scalar> darcyExp_;

        //- Rotation between coordinate systems
        List<tensor> rotate_;

        //- reference to the fluid properties
        const FluidPropertiesDataBase& fluidDB_;

        //- reference to the porous meidum properties
        const PorousMediumDataBase& porousDB_;

    // Private Member Functions
    
        void initVectors();

        List<scalar> getMu();
    
        List<scalar> getRho();
    
        List<scalar> getHydrDiam();
    
        List<scalar> getGamma();
    
        List<scalar> getDarcyConst();
    
        List<scalar> getDarcyExp();

        List<tensor> getRotate();

public:

    // Constructors

        FlowResDEIM
        (
            word name,
            fvMesh& baseMesh,
            Time& runTime,
            const FluidPropertiesDataBase& fluidDB,
            const PorousMediumDataBase& porousDB,
            bool leastSquares
        );

        FlowResDEIM
        (
            word name,
            word zoneName,
            fvMesh& baseMesh,
            Time& runTime,
            const FluidPropertiesDataBase& fluidDB,
            const PorousMediumDataBase& porousDB,
            bool leastSquares
        );

    // Destructor

        ~FlowResDEIM();

    // Public Member Functions

        void calcCoeffs();

        void calcCoeffsComponentBased();

        void calcCoeffsLeastSquares();

        void sumVectors();

        void createRedFlowResVectors();

        void createRedFlowResVectorsNonNative();

        void selectMaterialProperties();

        void saveSelectedMaterial(word folder);

        void readSelectedMaterial(word folder);

        void saveReducedFlowRes(word folder);

        void readReducedFlowRes(word folder);

        List<scalar>& redFlowRes()
        {
            return redFlowRes_;
        }

        void testFlowResDEIM(PtrList<volVectorField>* Usnapshots);

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
