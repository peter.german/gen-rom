/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    DEIM

Description
    Class with the implementation of the Discrete Empirical Interpolation
    Method (DEIM).

\*---------------------------------------------------------------------------*/

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

//- Construct using all of the matrices
template <typename T>
Foam::DEIM<T>::DEIM
(
    fvMesh& baseMesh,
    Time& runTime
)
    :
    baseMesh_(baseMesh),
    runTime_(runTime),
    assembled_(false)
{}

// * * * * * * * * * * * * * * * * Destructors * * * * * * * * * * * * * * * //

template <typename T>
Foam::DEIM<T>::~DEIM()
{}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * Public Member Functions  * * * * * * * * * * * //

template <typename T>
void Foam::DEIM<T>::printSelected()
{
    volScalarField selectedPoints
    (
        IOobject
        (
            name_+zone_+"DEIMPoints",
            "0",
            baseMesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        baseMesh_,
        dimensionedScalar("", dimensionSet(0,0,0,0,0,0,0), 0.0),
        zeroGradientFvPatchScalarField::typeName
    );

    selectedPoints *= 0.0;

    forAll(selectedCells_, cellIndex)
    {
        selectedPoints.primitiveFieldRef()[selectedCells_[cellIndex]] = 1.0;
    }

    selectedPoints.write();
}

template <typename T>
void Foam::DEIM<T>::saveSelectedIndices(word path)
{
    mkDir(path);
    OFstream os1(path + "cells" + name_ + zone_, runTime_.writeFormat());
    os1 << selectedCells_;
}

template <typename T>
void Foam::DEIM<T>::readSelectedIndices(word path)
{
    selectedCells_.clear();

    mkDir(path);
    IFstream is(path + "cells" + name_ + zone_, runTime_.writeFormat());
    is >> selectedCells_;
}

template <typename T>
void Foam::DEIM<T>::saveRedInterpolMx(word path)
{
    mkDir(path);
    OFstream os1(path + "redInterpolMx" + name_ + zone_, runTime_.writeFormat());
    os1 << redInterpolMx_;
}

template <typename T>
void Foam::DEIM<T>::readRedInterpolMx(word path)
{
    mkDir(path);
    IFstream is(path + "redInterpolMx" + name_ + zone_, runTime_.writeFormat());
    is >> redInterpolMx_;
}

template <typename T>
void Foam::DEIM<T>::createBasesGeneral
(
    PtrList<GeometricField<T, fvPatchField, volMesh> >& snapshots,
    PtrList<GeometricField<T, fvPatchField, volMesh> >& bases,
    label baseSelectionStrategy,
    scalar enLimit,
    label numberOfModes
)
{
    bases.clear();

    label noRetainedModes = 0;

    PODEigenBase CMDecomp(snapshots);

    if (baseSelectionStrategy == 1)
    {
        noRetainedModes = numberOfModes;
    }
    else if (baseSelectionStrategy == 2)
    {
        const scalarField& cumEVs = CMDecomp.cumulativeEigenValues();

        forAll(cumEVs, baseInd)
        {
            noRetainedModes++;

            if (cumEVs[baseInd] > enLimit)
            {
                break;
            }
        }
    }
    else
    {
        Info << "Invalid base selection strategy! choose one of the following:"
             << endl << "1 - predescribed mode numbers"
             << endl << "2 - energy retention limit" << endl;
        throw std::exception();
    }

    rightSingVectors_ = FieldField<Field, scalar>(CMDecomp.eigenVectors());
    eigenValues_ = CMDecomp.eigenValues();

    for (label baseInd = 0; baseInd < noRetainedModes; baseInd++)
    {

//        rightSingVectors_.append(CMDecomp.eigenVectors()[baseInd]);

        tmp<GeometricField<T, fvPatchField, volMesh> > dummy
        (
            new GeometricField<T, fvPatchField, volMesh>
            (
                IOobject
                (
                    snapshots[0].name() + zone_ + "POD",
                    snapshots[0].time().timeName(),
                    snapshots[0].mesh(),
                    IOobject::NO_READ,
                    IOobject::NO_WRITE
                ),
                snapshots[0]
            )
        );

        dummy.ref() *= 0.0;
        forAll(dummy.ref().boundaryField(), patchI)
        {
            if(dummy.ref().boundaryField()[patchI].type() == "fixedValue" ||
               dummy.ref().boundaryField()[patchI].type() == "uniformFixedValue")
            {
                forAll(dummy.ref().boundaryFieldRef()[patchI], faceI)
                {
                    dummy.ref().boundaryFieldRef()[patchI][faceI] = pTraits<T>::zero;
                }
            }
        }

        forAll (rightSingVectors_[baseInd], eInd)
        {
            dummy.ref() += rightSingVectors_[baseInd][eInd]*snapshots[eInd];

            forAll(dummy.ref().boundaryField(), patchI)
            {
                if(dummy.ref().boundaryField()[patchI].type() == "fixedValue" ||
                   dummy.ref().boundaryField()[patchI].type() == "uniformFixedValue")
                {
                    forAll(dummy.ref().boundaryFieldRef()[patchI], faceI)
                    {
                        dummy.ref().boundaryFieldRef()[patchI][faceI] +=
                            rightSingVectors_[baseInd][eInd]
                            *snapshots[eInd].boundaryFieldRef()[patchI][faceI];
                    }
                }
            }
        }

        if (eigenValues_[baseInd] > SMALL)
        {
            dummy.ref() /= Foam::sqrt(eigenValues_[baseInd]);

            forAll(dummy.ref().boundaryField(), patchI)
            {
                if(dummy.ref().boundaryField()[patchI].type() == "fixedValue" ||
                   dummy.ref().boundaryField()[patchI].type() == "uniformFixedValue")
                {
                    forAll(dummy.ref().boundaryFieldRef()[patchI], faceI)
                    {
                        dummy.ref().boundaryFieldRef()[patchI][faceI] /=
                            Foam::sqrt(eigenValues_[baseInd]);
                    }
                }
            }
            dummy.ref().correctBoundaryConditions();
        }

        bases.append(dummy);
    }
}

template <typename T>
void Foam::DEIM<T>::createBasesGeneral
(
    PtrList<GeometricField<T, fvPatchField, volMesh> >& snapshots,
    PtrList<GeometricField<T, fvPatchField, volMesh> >& bases,
    FieldField<Field, scalar>& rightSingVectors,
    scalarField& eigenValues,
    label baseSelectionStrategy,
    scalar enLimit,
    label numberOfModes
)
{
    bases.clear();

    for (label baseInd = 0; baseInd < numberOfModes; baseInd++)
    {
        GeometricField<T, fvPatchField, volMesh> dummy
        (
            IOobject
            (
                snapshots[0].name() + zone_ + "POD",
                snapshots[0].time().timeName(),
                snapshots[0].mesh(),
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            snapshots[0]
        );

        dummy *= 0.0;
        forAll(dummy.boundaryField(), patchI)
        {
            if(dummy.boundaryField()[patchI].type() == "fixedValue" ||
               dummy.boundaryField()[patchI].type() == "uniformFixedValue")
            {
                forAll(dummy.boundaryFieldRef()[patchI], faceI)
                {
                    dummy.boundaryFieldRef()[patchI][faceI] = pTraits<T>::zero;
                }
            }
        }

        forAll (rightSingVectors_[baseInd], eInd)
        {
            dummy += rightSingVectors[baseInd][eInd]*snapshots[eInd];

            forAll(dummy.boundaryField(), patchI)
            {
                if(dummy.boundaryField()[patchI].type() == "fixedValue" ||
                   dummy.boundaryField()[patchI].type() == "uniformFixedValue")
                {
                    forAll(dummy.boundaryFieldRef()[patchI], faceI)
                    {
                        dummy.boundaryFieldRef()[patchI][faceI] +=
                            rightSingVectors[baseInd][eInd]
                            *snapshots[eInd].boundaryFieldRef()[patchI][faceI];
                    }
                }
            }
        }

        if (eigenValues_[baseInd] > SMALL)
        {
            dummy /= Foam::sqrt(eigenValues[baseInd]);

            forAll(dummy.boundaryField(), patchI)
            {
                if(dummy.boundaryField()[patchI].type() == "fixedValue" ||
                   dummy.boundaryField()[patchI].type() == "uniformFixedValue")
                {
                    forAll(dummy.boundaryFieldRef()[patchI], faceI)
                    {
                        dummy.boundaryFieldRef()[patchI][faceI] /=
                            Foam::sqrt(eigenValues[baseInd]);
                    }
                }
            }
            dummy.correctBoundaryConditions();
        }

        bases.append(dummy);
    }
}

template <typename T>
void Foam::DEIM<T>::createBases
(
    label baseSelectionStrategy,
    scalar enLimit,
    label numberOfModes
)
{
    createBasesGeneral
    (
        snapshots_,
        ownBase_,
        baseSelectionStrategy,
        enLimit,
        numberOfModes
    );
}

template <typename T>
void Foam::DEIM<T>::createBases
(
    FieldField<Field, scalar>& rightSingVectors,
    scalarField& eigenValues,
    label baseSelectionStrategy,
    scalar enLimit,
    label numberOfModes
)
{
    createBasesGeneral
    (
        snapshots_,
        ownBase_,
        rightSingVectors,
        eigenValues,
        baseSelectionStrategy,
        enLimit,
        numberOfModes
    );
}

template <typename T>
void Foam::DEIM<T>::saveBases()
{
    word path("ROMFiles/bases/");
    ROMUtilities::saveFields(ownBase_, path, runTime_);
}

template <typename T>
void Foam::DEIM<T>::saveEigenValues()
{
    word path("ROMFiles/bases/");

    word BDIDpath(path + "/eigenvalues/");

    mkDir(BDIDpath);

    OFstream os1(BDIDpath + zone_ + name_);
    os1 << eigenValues_;
}

template <typename T>
void Foam::DEIM<T>::readSnapshots(label noSnaps)
{
    word path("ROMFiles/snapshots/");

    ROMUtilities::readFields
    (
        snapshots_,
        baseMesh_,
        name_,
        noSnaps,
        path
    );

    if(zone_ != "")
    {
        label zoneID = baseMesh_.cellZones().findZoneID(zone_);

        tmp<volScalarField> zoneFilterTmp
        (
            new volScalarField
            (
                IOobject
                (
                    "zoneFilter",
                    "0",
                    baseMesh_,
                    IOobject::NO_READ,
                    IOobject::NO_WRITE
                ),
                baseMesh_,
                dimensionedScalar("", dimensionSet(0,0,0,0,0,0,0), 0.0),
                zeroGradientFvPatchScalarField::typeName
            )
        );
        volScalarField& zoneFilter = zoneFilterTmp.ref();

        forAll(baseMesh_.cellZones()[zoneID], cellIlocal)
        {
            label cellIglobal = baseMesh_.cellZones()[zoneID][cellIlocal];
            zoneFilter[cellIglobal] =  1.0;
        }

        forAll(snapshots_, snapIndex)
        {
            snapshots_[snapIndex] = snapshots_[snapIndex] * zoneFilter;
        }
    }
}

template <typename T>
void Foam::DEIM<T>::readBases(label noBases)
{
    word path("ROMFiles/bases/");

    ROMUtilities::readFields
    (
        ownBase_,
        baseMesh_,
        name_ + zone_ + "POD",
        noBases,
        path
    );
}

template <typename T>
void Foam::DEIM<T>::updateNonlinSolutions
(
    List<scalar>* redNonlinSol
)
{
    redNonlinSol_ = redNonlinSol;
}

template <typename T>
void Foam::DEIM<T>::updateNonlinBases
(
    PtrList<GeometricField<T, fvPatchField, volMesh> >& nonlinBase
)
{
    nonlinBase_ = &nonlinBase;
}

template <typename T>
void Foam::DEIM<T>::updateNativeProjectionBase
(
    PtrList<GeometricField<T, fvPatchField, volMesh> >& nativeProjectionBase
)
{
    nativeProjectionBase_ = &nativeProjectionBase;
}

// ************************************************************************* //
