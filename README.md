# GeN-ROM - An OpenFOAM&reg;-based intrusive Reduced-Order Modeling Framework for parametric multiphysics simulations 

<h2>Overview</h2> 

GeN-ROM is an [OpenFOAM&reg;](https://www.openfoam.com/)-based intrusive model order reduction tool that relies on a Proper Orthogonal Decomposition (POD) aided Reduced-Basis Technique (POD-RB) coupled with the method of snapshots. The developed framework extensively utilizes [GeN-Foam](https://gitlab.com/foam-for-nuclear/GeN-Foam), a similarly OpenFOAM&reg;-based multiphysics framework, for the training of the ROMs. GeN-ROM was originally created for the simulation of Molten Salt Reactors (MSRs) with nuclear fuel dissolved in the liquid salt.

<h2>Capabilities</h2> 

GeN-ROM is capable of the reduction of:
*  Parametric steady-state and transient Navier-Stokes equations for laminar and turbulent flows. The system supports clean, porous medium and hybrid flows.
*  Parametric steady-state and transient enthalpy equation with clean or porous medium fluid flows.
*  Parametric neutronics equations: multigroup neutron diffusion equations with moving precursors. Both k-eigenvalue and transient solves are supported.

The detailed description of the theory behind the developed solvers can be found in [[1]](#german2019reduced)-[[5]](#german2019datadriven).

<h2> Installation </h2>

To set up GeN-ROM, we need to install the following libraries/frameworks first:

1. **Install [OpenFOAM&reg;](https://www.openfoam.com/)**:

   GeN-ROM utilizes the high-level functions of OpenFOAM for field manipulation. 
   In the current state we only support version 2106 downloaded and installed from 
   [https://www.openfoam.com/](https://www.openfoam.com/). Follow the installation 
   instructions on the mentioned website. Also, one should also source the 
   proper bashrc file for OpenFOAM:
   ```
   source $HOME/openfoam/OpenFOAM-v2106/etc/bashrc
   ```
   given that OpenFOAM is installed in the *$HOME/openfoam* directory.
2. **Install [GeN-Foam](https://gitlab.com/foam-for-nuclear/GeN-Foam)**:

   GeN-ROM utilizes GeN-Foam for the training of the ROMs.
   So in this case we need to setup the repository as:
   ```
   git clone https://gitlab.com/foam-for-nuclear/GeN-Foam
   ```
   To enable certain functionalities in GeN-ROM GeN-Foam needs to be slightly enhanced:
   * To be able to use the flow resistances, we need to save snapshots of that as well.
     This can be done by modifying  the following file:
     ```
     GeN-Foam/GeN-Foam/classes/thermalHydraulics/solvers/onePhaseLegacy/include/equations/UEqn_1pl.H 
     ```
     First we need to copy the following file into the *equations* library in GeN-Foam:
     ```
     gen-rom/addons/gen-foam-extension/printFlowResistance.H
     ```
     and we need to include this at the very end of *UEqn_1pl.H* as:
     ```
     #include "printFlowResistance.H"
     ```
   * To be able to use the temperature feedback in the neutronics solvers we need to 
     print modified temperatures as well. This can be done by modifying the followng file:
     ```
      GeN-Foam/GeN-Foam/main/GeN-Foam.C
     ```
     First we need to copy the following file into the *main* firectory of GeN-Foam:
     ```
     gen-rom/addons/gen-foam-extension/printNeutronicsTemperatures.H
     ``` 
     and we need to include thios file in *GeN-Foam.C* right before the runtime prints:
     ```
     ...
     #include "printNeutronicsTemperatures.H"

     runTime.write();
     ...
     ``` 
   If these functionalities are set, or not needed, one can move one and compile GeN-Foam by navigating to 
   *GeN-Foam/GeN-Foam* directory and executing:
   ```
   ./Allwmake -j
   ```
3. **Install GeN-ROM**:
  
   First, clone the GeN-ROM repository:
   ```
   git clone https://gitlab.com/peter.german/gen-rom.git
   ```
   Then compile the framework:
   ```
   cd gen-rom
   wmake -j
   ```
4. **Install *python 3* with the following external libraries: numpy, scipy, pyDOE.**

<h2> Structure </h2>

The complex mutliphysics ROMs in this frameworks are built using elementary ROM classes for each governing equation. These ROMs are situated under the **reducedModels** subdirectory and are used in the solvers within the **solvers** folder. The snapshots for the training of the ROMs can be produced using a python script in **python/snapshotGeneration** folder. The script creates a **ROMFiles** subdirectory in the original case folder. The ROM operations are then controlled using an additional, **system/ROMDict** dictionary file. For more information on the details of running GeN-ROM, see the included tutorial. 

<h2>Tutorials</h2>

To visit simple tutorials on how to use the framework, click [here](tutorials/tutorials.md).
The tutorials only showcase the basic capabilities of the framework, one can 
explore the source code for more advanced functions.

## References
<a id="german2021genrom">[1]</a> German, P., Tano, M., Fiorina, C., & Ragusa, J. C. (2022). GeN-ROM—An OpenFOAM®-based multiphysics reduced-order modeling framework for the analysis of Molten Salt Reactors. _Progress in Nuclear Energy_, 146, 104148.

<a id="german2019datadriven">[2]</a> German, P., Tano, M. E., Fiorina, C., & Ragusa, J. C. (2021). Data-Driven Reduced-Order Modeling of Convective Heat Transfer in Porous Media. _Fluids_, _6_(8), 266.

<a id="german2019reduced">[3]</a> German, Peter and Ragusa, Jean C (2019), "Reduced-order modeling of parameterized multi-group diffusion k-eigenvalue problems", *Annals of Nuclear Energy*, 134, 144-157.

<a id="german2019application">[4]</a> German, Peter, Jean C. Ragusa, and Carlo Fiorina (2019). "Application of multiphysics model order reduction to doppler/neutronic feedback." *EPJ Nuclear Sciences & Technologies* 5. ARTICLE 17.

<a id="german2019comparison">[5]</a> German, P., Tano, M., Ragusa, J. C., and Fiorina, C. (2020). "Comparison of Reduced-Basis techniques for the model order reduction of parametric incompressible fluid flows. _Progress in Nuclear Energy_, _130_, 103551.

