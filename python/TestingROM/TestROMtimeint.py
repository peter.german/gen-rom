#!/usr/bin/python

import os
import shutil
import re
import sys
import numpy as np

sys.path.append(os.getcwd())
from field_parser import parse_internal_field
import testROMInput 

print("-------------------------------------------------------------------------------")
print(" Reading controls from testROMInput.")
print("-------------------------------------------------------------------------------")

noSamples = testROMInput.noSamples
startIndex = testROMInput.startIndex
fieldsToCompare = testROMInput.fieldsToCompare
weightName = testROMInput.weightingFunction
shift = testROMInput.shift
shiftIndex = testROMInput.shiftIndex
FOMDir = testROMInput.FOMDir
ROMDir = testROMInput.ROMDir
 
print("-------------------------------------------------------------------------------")
print(" Comparing the fields in the given directories.")
print("-------------------------------------------------------------------------------")

RelError = {}
avgs = {}
l2_errors = {}
for field in fieldsToCompare:
    RelError[field.split(".")[0]] = []
    avgs[field.split(".")[0]] = 0.0
    l2_errors[field.split(".")[0]] = 0.0

for sampleIndex in range(startIndex, noSamples):

    subDirFOM = FOMDir + "/" + str(sampleIndex)

    FOMFile = np.zeros(1)

    for rootFOM, dirNamesDOM, fileNamesFOM in os.walk(subDirFOM):

        for fileIndexFOM in range(0,len(fileNamesFOM)):

            if fileNamesFOM[fileIndexFOM] in fieldsToCompare:

                if weightName != "":
                    weigth = parse_internal_field(os.path.join(rootFOM, weightName))
                else: 
                    weigth = 1.0

                FOMFile = parse_internal_field(os.path.join(rootFOM, fileNamesFOM[fileIndexFOM]))

                if isinstance(FOMFile[0], np.ndarray):
                    FOMFile = np.linalg.norm(FOMFile, axis=1)
                else:
                    FOMFile = np.abs(FOMFile)

                sqrtvals = np.square(FOMFile)

                ref_norm = np.sqrt(np.dot(sqrtvals, weigth))

                avgs[fileNamesFOM[fileIndexFOM].split(".")[0]] += ref_norm

for sampleIndex in range(startIndex, noSamples):

    subDirFOM = FOMDir + "/" + str(sampleIndex)
    subDirROM = ROMDir + "/" + str(sampleIndex)

    FOMFile = np.zeros(1)
    ROMFile = np.zeros(1)

    for rootFOM, dirNamesDOM, fileNamesFOM in os.walk(subDirFOM):

        for fileIndexFOM in range(0,len(fileNamesFOM)):

            if fileNamesFOM[fileIndexFOM] in fieldsToCompare:

                fileNameROM = fileNamesFOM[fileIndexFOM].replace(
                                    fileNamesFOM[fileIndexFOM].split(".")[0], 
                                    fileNamesFOM[fileIndexFOM].split(".")[0]+"ROM")

                if weightName != "":
                    weigth = parse_internal_field(os.path.join(rootFOM, weightName))
                else: 
                    weigth = 1.0

                FOMFile = parse_internal_field(os.path.join(rootFOM, fileNamesFOM[fileIndexFOM]))

                for rootROM, dirNamesROM, fileNamesROM in os.walk(subDirROM):

                    for fileROM in fileNamesROM:

                        if fileNameROM == fileROM:

                            ROMFile = parse_internal_field(os.path.join(rootROM, fileNameROM))

                index = fieldsToCompare.index(fileNamesFOM[fileIndexFOM])
                if(shift[index]):
                    FOMFile = FOMFile - FOMFile[shiftIndex[index]]
                    ROMFile = ROMFile - ROMFile[shiftIndex[index]]

                error = FOMFile - ROMFile

                if isinstance(error[0], np.ndarray):
                    error = np.linalg.norm(error, axis=1)
                else:
                    error = np.abs(error)

                sqrError = np.square(error)

                relL2error = np.sqrt(np.dot(sqrError, weigth)) / avgs[fileNamesFOM[fileIndexFOM].split(".")[0]]

                l2_errors[fileNamesFOM[fileIndexFOM].split(".")[0]] += relL2error

print("-------------------------------------------------------------------------------")
print(" Printing statistics.")
print("-------------------------------------------------------------------------------")

for item in RelError:

    toPrint = []

    toPrint.append(item)

    for elem in RelError[item]:
        toPrint.append(elem)

    print(item + " time avg error: " + str(l2_errors[item]))

 
