#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 17:05:34 2019

@author: mtano
"""
import sys
import os
import fileinput
from field_parser import parse_internal_field
import numpy as np
import subprocess

os.chdir("/home/mtano/OpenFOAM/mtano-6/run/BFS-GF-model/")

#Config
base_dir = '140/fluidRegion/'
ROM_dir = '1/fluidRegion/'

U_file = base_dir + 'U.gz'
p_file = base_dir + 'p_rgh.gz'

U_ROM_file = ROM_dir + 'UROM.gz'
p_ROM_file = ROM_dir + 'p_rghROM.gz'

ROMDict = "system/ROMDict"
search_strings = ['numberOfMomentumBases', 'numberOfPressureBases', 'noSnapshots']
max_number_bases = 20

command = "GeN-ROM"

# Read base solutions
U_base = parse_internal_field(U_file)
 = parse_internal_field(p_file)

# Create holding dictionaries
U_ROM_sols = {}
p_ROM_sols = {}

for base in range(max_number_bases):
    
    # Changing ROM-Dict
    for line in fileinput.input(ROMDict, inplace=True):
        flag = False
        for s in search_strings:
            if s in line:
                new_sting = s + ' ' + str(base+1) + ';'
                print(new_sting)
                flag = True
                
        if not flag: sys.stdout.write(line)
            
    # Running simulaiton
    command_run = subprocess.call(command)
    if command_run == 1: 
        sys.exit()
        U_ROM_sols[base] = None
        p_ROM_sols[base] = None
    else:
        # Recovering data
        U_ROM_sols[base] = parse_internal_field(U_ROM_file)
        p_ROM_sols[base] = parse_internal_field(p_ROM_file)

# Get the volumes
generate_volumes_command = "postProcess -func writeCellVolumes"
subprocess.call(generate_volumes_command, shell=True)
vol_file = '1/V.gz'
vols = parse_internal_field(vol_file)

    
# Compute error-norms
U_error = np.zeros(max_number_bases); p_error = np.zeros(max_number_bases)

for base in range(max_number_bases):
    p_error[base] = np.mean(np.sqrt((p_base - p_ROM_sols[base])**2)*vols)/np.mean(vols)
    U_error[base] = np.mean(np.sqrt((np.linalg.norm(U_base,axis=1) - np.linalg.norm(U_ROM_sols[base],axis=1))**2)*vols)/np.mean(vols)

p_error = p_error/np.mean(p_base)
U_error = U_error/np.mean(np.linalg.norm(U_base,axis=1)) 
    