#!/usr/bin/python
import os
import numpy as np
import math

# The number of samples to create 
noSamples = 2

# Defining the fields it analyze
fieldsToCompare = ["U.gz", "p_rgh.gz"]

# Weighting function name (field that it reads). It should be in the
# directory as the FOM. 
weightingFunction = "V.gz"

# Defining the directories of the files of FOM
FOMDir = "/home/german/projects/msfr-test-cases/ip-op-ss-test/ROMFiles/test"

# Defining the directories of the files of ROM
ROMDir = "/home/german/projects/msfr-test-cases/ip-op-ss-test/ROMFiles/testROM"