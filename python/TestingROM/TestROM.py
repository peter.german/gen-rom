#!/usr/bin/python

import os
import shutil
import re
import sys
import numpy as np

sys.path.append(os.getcwd())
from field_parser import parse_internal_field
import testROMInput 

print("-------------------------------------------------------------------------------")
print(" Reading controls from testROMInput.")
print("-------------------------------------------------------------------------------")

noSamples = testROMInput.noSamples
startIndex = testROMInput.startIndex
fieldsToCompare = testROMInput.fieldsToCompare
weightName = testROMInput.weightingFunction
shift = testROMInput.shift
shiftIndex = testROMInput.shiftIndex
FOMDir = testROMInput.FOMDir
ROMDir = testROMInput.ROMDir
 
print("-------------------------------------------------------------------------------")
print(" Comparing the fields in the given directories.")
print("-------------------------------------------------------------------------------")

RelError = {}
for field in fieldsToCompare:
    RelError[field.split(".")[0]] = []

for sampleIndex in range(startIndex, noSamples):

    subDirFOM = FOMDir + "/" + str(sampleIndex)
    subDirROM = ROMDir + "/" + str(sampleIndex)

    FOMFile = np.zeros(1)
    ROMFile = np.zeros(1)

    for rootFOM, dirNamesDOM, fileNamesFOM in os.walk(subDirFOM):

        for fileIndexFOM in range(0,len(fileNamesFOM)):

            if fileNamesFOM[fileIndexFOM] in fieldsToCompare:

                fileNameROM = fileNamesFOM[fileIndexFOM].replace(
                                    fileNamesFOM[fileIndexFOM].split(".")[0], 
                                    fileNamesFOM[fileIndexFOM].split(".")[0]+"ROM")

                if weightName != "":
                    weigth = parse_internal_field(os.path.join(rootFOM, weightName))
                else: 
                    weigth = 1.0

                FOMFile = parse_internal_field(os.path.join(rootFOM, fileNamesFOM[fileIndexFOM]))

                for rootROM, dirNamesROM, fileNamesROM in os.walk(subDirROM):

                    for fileROM in fileNamesROM:

                        if fileNameROM == fileROM:

                            ROMFile = parse_internal_field(os.path.join(rootROM, fileNameROM))

                size = 0
                everything_constant=False
                if(isinstance(weigth,(list,np.ndarray))):
                    size = len(weigth)
                elif(isinstance(FOMFile,(list,np.ndarray)) and len(FOMFile)!=3):
                    size = len(FOMFile)
                elif(isinstance(ROMFile,(list,np.ndarray)) and len(ROMFile)!=3):
                    size = len(ROMFile)
                else:
                    everything_constant=True

                if(not isinstance(weigth,(list,np.ndarray))):
                    w_value = weigth
                    weigth = np.asarray([w_value for i in range(size)])
                if(isinstance(FOMFile,(float))):
                    fom_value = FOMFile
                    FOMFile = np.asarray([fom_value for i in range(size)])
                else:
                    if(len(FOMFile)==3):
                        fom_value = FOMFile
                        FOMFile = np.asarray([fom_value for i in range(size)])

                if(isinstance(ROMFile,(float))):
                        rom_value = ROMFile
                        ROMFile = np.asarray([rom_value for i in range(size)])
                else:
                    if(len(ROMFile)==3):
                        rom_value = ROMFile
                        ROMFile = np.asarray([rom_value for i in range(size)])


                if(everything_constant):
                    reference = 1.0
                    error = FOMFile - ROMFile
                    if isinstance(error, np.ndarray):
                        error = np.linalg.norm(error, axis=1)
                        reference = np.linalg.norm(FOMFile, axis=1)
                    else:
                        error = np.abs(error)
                        reference = np.abs(FOMFile)

                    sqrError = np.square(error)
                    sqrReference = np.square(reference)
                    relL2error = 0.0
                    if(sqrReference != 0.0):
                        relL2error = np.sqrt(np.sum(sqrError * weigth)) / np.sqrt(np.sum(sqrReference * weigth))
                    else:
                        relL2error = np.sqrt(np.sum(sqrError * weigth))

                    RelError[fileNamesFOM[fileIndexFOM].split(".")[0]].append(relL2error)

                else:
                    index = fieldsToCompare.index(fileNamesFOM[fileIndexFOM])
                    if(shift[index]):
                        FOMFile = FOMFile - FOMFile[shiftIndex[index]]
                        ROMFile = ROMFile - ROMFile[shiftIndex[index]]

                    error = FOMFile - ROMFile

                    if isinstance(error[0], np.ndarray):
                        error = np.linalg.norm(error, axis=1)
                        reference = np.linalg.norm(FOMFile, axis=1)
                    else:
                        error = np.abs(error)
                        reference = np.abs(FOMFile)

                    sqrError = np.square(error)
                    sqrReference = np.square(reference)
                    relL2error = 0.0
                    if(abs(np.sum(sqrReference * weigth)) != 0.0):
                        relL2error = np.sqrt(np.sum(sqrError * weigth)) / np.sqrt(np.sum(sqrReference * weigth))
                    else:
                        relL2error = np.sqrt(np.sum(sqrError * weigth))
                    RelError[fileNamesFOM[fileIndexFOM].split(".")[0]].append(relL2error)

print("-------------------------------------------------------------------------------")
print(" Printing statistics.")
print("-------------------------------------------------------------------------------")

for item in RelError:

    toPrint = []

    toPrint.append(item)

    for elem in RelError[item]:
        toPrint.append(elem)

    print(item + " mean error: " + str(np.mean(RelError[item])) 
               + ", max error: " + str(np.max(RelError[item])))

    outfile = open(toPrint[0] + "Error.txt", "w")
    outfile.write("\n".join(str(i) for i in toPrint))
    outfile.close()

 
