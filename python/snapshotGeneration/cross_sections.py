import os
import numpy as np

class CrossSections:

    def __init__(self, path, pathfueltemp="", pathrhocool=""):

        self.path = path

        self.pathfueltemp = pathfueltemp

        self.pathrhocool = pathrhocool

        self.no_energy_groups = 0

        self.no_precursor_groups = 0

        self.T_ref = None
        self.T_pert = None

        self.rho_ref = None
        self.rho_pert = None

        self.fast_neutrons = False

        self.zone_names = []

        self.no_zones = 0

        self.iv = {}

        self.D = {}
        self.D_fueltemp = {}
        self.D_rhocool = {}
        self.alpha_D_fueltemp = {}
        self.alpha_D_rhocool = {}

        self.nu_sigma_f = {}
        self.nu_sigma_f_fueltemp = {}
        self.nu_sigma_f_rhocool = {}
        self.alpha_nu_sigma_f_fueltemp = {}
        self.alpha_nu_sigma_f_rhocool = {}

        self.sigma_pow = {}
        self.sigma_pow_fueltemp = {}
        self.sigma_pow_rhocool = {}
        self.alpha_sigma_pow_fueltemp = {}
        self.alpha_sigma_pow_rhocool = {}

        self.scattering_matrix = {}
        self.scattering_matrix_fueltemp = {}
        self.scattering_matrix_rhocool = {}
        self.alpha_scattering_matrix_fueltemp = {}
        self.alpha_scattering_matrix_rhocool = {}

        self.sigma_r = {}
        self.sigma_r_fueltemp = {}
        self.sigma_r_rhocool = {}
        self.alpha_sigma_r_fueltemp = {}
        self.alpha_sigma_r_rhocool = {}

        self.chi_p = {}

        self.chi_d = {}

        self.lamb = {}

        self.beta = {}

        self.beta_tot = {}

    def read_cross_sections(self):

        print("Reading default cross sections.")

        xsfile = open(self.path)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split()
        for li in range(len(split_text)):
            if "energyGroups" in split_text[li]:
                self.no_energy_groups = int(split_text[li+1])
            elif "precGroups" in split_text[li]:
                self.no_precursor_groups = int(split_text[li+1])
            elif "fastNeutrons" in split_text[li]:
                if split_text[li+1].startswith("true"):
                    self.fast_neutrons = True
                else:
                    self.fast_neutrons = False
            elif "zones" in split_text[li]:
                zones_start = li
            elif ");" in split_text[li]:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):
            if "{" in zones_raw[li]:

                name = zones_raw[li-1]
                self.zone_names.append(name)

                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break

                zone = zones_raw[zone_begin:zone_end]

                for subli in range(len(zone)):
                    if "IV" == zone[subli]:
                        self.iv[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "D" == zone[subli]:
                        self.D[name] = [float(i.strip("(").strip(";").strip(")")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "nuSigmaEff" == zone[subli]:
                        self.nu_sigma_f[name] = [float(i.strip("(").strip(";").strip(")")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "sigmaPow" == zone[subli]:
                        self.sigma_pow[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "scatteringMatrixP0" == zone[subli]:
                        tmp = []
                        for i in range(self.no_energy_groups):
                            if(i==0):
                                tmp.append([float(i) for i in zone[subli+5:subli+self.no_energy_groups+5]])
                                subli = subli+5+self.no_energy_groups
                            else:
                                tmp.append([float(i) for i in zone[subli+2:subli+self.no_energy_groups+2]])
                                subli = subli+2+self.no_energy_groups
                        self.scattering_matrix[name] = tmp
                    elif "sigmaDisapp" == zone[subli]:
                        self.sigma_r[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                    elif "chiPrompt" == zone[subli]:
                        self.chi_p[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                    elif "chiDelayed" == zone[subli]:
                        self.chi_d[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                    elif "lambda" == zone[subli]:
                        self.lamb[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_precursor_groups+4]]
                    elif "Beta" == zone[subli]:
                        self.beta[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_precursor_groups+4]]
                        self.beta_tot[name] = sum(self.beta[name])

        self.no_zones = len(self.zone_names)

    def read_cross_sections_fueltemp(self):

        print("Reading fuel temperature cross sections.")

        if self.pathfueltemp == "":
            raise ValueError("A path must be given to the fuel temperature cross sections!")

        xsfile = open(self.pathfueltemp)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split()
        for li in range(len(split_text)):
            if "TfuelRef" in split_text[li]:
                self.T_ref = float(split_text[li+1])
            elif "TfuelPerturbed" in split_text[li]:
                self.T_pert = float(split_text[li+1])
            elif "zones" in split_text[li]:
                zones_start = li
            elif ");" in split_text[li]:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):
            if "{" in zones_raw[li]:

                name = zones_raw[li-1]
                if name not in self.zone_names:
                    raise ValueError("Unknown zone defined in fuel temperatur cross sections!")

                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break

                zone = zones_raw[zone_begin:zone_end]

                for subli in range(len(zone)):
                    if "D" == zone[subli]:
                        self.D_fueltemp[name] = [float(i.strip("(").strip(";").strip(")")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "nuSigmaEff" == zone[subli]:
                        self.nu_sigma_f_fueltemp[name] = [float(i.strip("(").strip(";").strip(")")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "sigmaPow" == zone[subli]:
                        self.sigma_pow_fueltemp[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "scatteringMatrixP0" == zone[subli]:
                        tmp = []
                        for i in range(self.no_energy_groups):
                            if(i==0):
                                tmp.append([float(i) for i in zone[subli+5:subli+self.no_energy_groups+5]])
                                subli = subli+5+self.no_energy_groups
                            else:
                                tmp.append([float(i) for i in zone[subli+2:subli+self.no_energy_groups+2]])
                                subli = subli+2+self.no_energy_groups
                        self.scattering_matrix_fueltemp[name] = tmp
                    elif "sigmaDisapp" == zone[subli]:
                        self.sigma_r_fueltemp[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]

    def read_cross_sections_rhocool(self):

        print("Reading coolant density cross sections.")

        if self.pathrhocool == "":
            raise ValueError("A path must be given to the coolant density cross sections!")

        xsfile = open(self.pathrhocool)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split()
        for li in range(len(split_text)):
            if "rhoCoolRef" in split_text[li]:
                self.rho_ref = float(split_text[li+1])
            elif "rhoCoolPerturbed" in split_text[li]:
                self.rho_pert = float(split_text[li+1])
            elif "zones" in split_text[li]:
                zones_start = li
            elif ");" in split_text[li]:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):
            if "{" in zones_raw[li]:

                name = zones_raw[li-1]
                if name not in self.zone_names:
                    raise ValueError("Unknown zone defined in coolant density cross sections!")

                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break

                zone = zones_raw[zone_begin:zone_end]

                for subli in range(len(zone)):
                    if "D" == zone[subli]:
                        self.D_rhocool[name] = [float(i.strip("(").strip(";").strip(")")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "nuSigmaEff" == zone[subli]:
                        self.nu_sigma_f_rhocool[name] = [float(i.strip("(").strip(";").strip(")")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "sigmaPow" == zone[subli]:
                        self.sigma_pow_rhocool[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]
                        subli = subli+4+self.no_energy_groups
                    elif "scatteringMatrixP0" == zone[subli]:
                        tmp = []
                        for i in range(self.no_energy_groups):
                            if(i==0):
                                tmp.append([float(i) for i in zone[subli+5:subli+self.no_energy_groups+5]])
                                subli = subli+5+self.no_energy_groups
                            else:
                                tmp.append([float(i) for i in zone[subli+2:subli+self.no_energy_groups+2]])
                                subli = subli+2+self.no_energy_groups
                        self.scattering_matrix_rhocool[name] = tmp
                    elif "sigmaDisapp" == zone[subli]:
                        self.sigma_r_rhocool[name] = [float(i.strip("(")) for i in zone[subli+4:subli+self.no_energy_groups+4]]

    def create_fueltemp_feedback_coefficients(self):

        print("Assumed fast neutron spectrum: ", self.fast_neutrons)

        dT=0.0
        if self.fast_neutrons:
            dT = np.log(self.T_pert/self.T_ref)
        else:
            dT = np.sqrt(self.T_pert) - np.sqrt(self.T_ref)

        for name in self.D:
            self.alpha_D_fueltemp[name] = (np.asarray(self.D_fueltemp[name]) - np.asarray(self.D[name])) / dT

        for name in self.nu_sigma_f:
            self.alpha_nu_sigma_f_fueltemp[name] = (np.asarray(self.nu_sigma_f_fueltemp[name]) -
                                                    np.asarray(self.nu_sigma_f[name])) / dT
        for name in self.sigma_r:
            self.alpha_sigma_r_fueltemp[name] = (np.asarray(self.sigma_r_fueltemp[name]) -
                                                    np.asarray(self.sigma_r[name])) / dT
        for name in self.sigma_pow:
            self.alpha_sigma_pow_fueltemp[name] = (np.asarray(self.sigma_pow[name]) -
                                                   np.asarray(self.sigma_pow[name])) / dT
        for name in self.scattering_matrix:
            self.alpha_scattering_matrix_fueltemp[name] = (np.asarray(self.scattering_matrix_fueltemp[name]) -
                                                           np.asarray(self.scattering_matrix[name])) / dT

    def create_rhocool_feedback_coefficients(self):

        drho = self.rho_pert - self.rho_ref

        for name in self.D:
            self.alpha_D_rhocool[name] = (np.asarray(self.D_rhocool[name]) - np.asarray(self.D[name])) / drho

        for name in self.nu_sigma_f:
            self.alpha_nu_sigma_f_rhocool[name] = (np.asarray(self.nu_sigma_f_rhocool[name]) -
                                                    np.asarray(self.nu_sigma_f[name])) / drho
        for name in self.sigma_r:
            self.alpha_sigma_r_rhocool[name] = (np.asarray(self.sigma_r_rhocool[name]) -
                                                    np.asarray(self.sigma_r[name])) / drho
        for name in self.sigma_pow:
            self.alpha_sigma_pow_rhocool[name] = (np.asarray(self.sigma_pow_rhocool[name]) -
                                                   np.asarray(self.sigma_pow[name])) / drho
        for name in self.scattering_matrix:
            self.alpha_scattering_matrix_rhocool[name] = (np.asarray(self.scattering_matrix_rhocool[name]) -
                                                           np.asarray(self.scattering_matrix[name])) / drho
