import sys
import copy

from sampling import Sampling

class Perturb:

    def __init__(self, idata, iintervals):

        self.data = idata

        self.intervals = iintervals

        self.noPars = {}

        self.paramSpaceDim = 0

        self.pertSamples = []

        self.paramValue = []

    def getDimensions(self):

        self.paramSpaceDim = 0

        globalCounter = 0
        for fileID in self.data:
            counter = 0
            for zone in self.data[fileID]:
                for prop in self.data[fileID][zone]:

                    corrInstance = self.data[fileID][zone][prop]
                    corrInterval = self.intervals[fileID][zone][prop]

                    if isinstance(corrInstance, list) and len(corrInstance) > 1:
                        for item in corrInterval:

                            if abs(item[0]-item[1])>1e-12:
                                counter += 1
                                globalCounter += 1

                    elif isinstance(corrInstance, list) and len(corrInstance) == 1:
                        counter += 1
                        globalCounter += 1
                    else:
                        counter += 1
                        globalCounter += 1
                    
            self.noPars[fileID] = counter

        self.paramSpaceDim = globalCounter

    def createSamples(self, iNoSamples, sampling):

        self.getDimensions()

        samp = Sampling()

        if sampling == "IHS":

            samp.computeIHS(self.paramSpaceDim, 
                        iNoSamples, 
                        1, 
                        11)
            sampleMx = samp.getIHS()

        elif sampling == "tensor":
            samp.computeTensor(self.paramSpaceDim, 
                                iNoSamples)
            sampleMx = samp.getTensor()

        elif sampling == "LHS":
            samp.computeLHS(self.paramSpaceDim, 
                            iNoSamples, 
                            11)
            sampleMx = samp.getLHS()

        elif sampling == "uniform":
            samp.computeUniform(self.paramSpaceDim, iNoSamples, 11)
            sampleMx = samp.getUniform()

        elif sampling == "uniformSobol":
            samp.computeUniformSobol(self.paramSpaceDim, iNoSamples)
            sampleMx = samp.getUniform()

        for sampleIndex in range(0,iNoSamples):

            tempSample = self.perturbData(sampleMx, sampleIndex)

            self.pertSamples.append(tempSample)


    def perturbData(self, iSampleMx, isampleIndex):

        paramIndex = 0

        perturbed = copy.deepcopy(self.data)

        tempParaValues = []

        for fileID in perturbed:

            for zone in perturbed[fileID]:
                for prop in perturbed[fileID][zone]:

                    corrInstance = perturbed[fileID][zone][prop]
                    corrInterval = self.intervals[fileID][zone][prop]

                    if isinstance(corrInstance, list) and len(corrInstance) > 1:
                        for itemIndex in range(0,len(corrInterval)):

                            minval = float(corrInterval[itemIndex][0])

                            maxval = float(corrInterval[itemIndex][1])

                            if abs(minval-maxval)>1e-12:

                                refval = float(corrInstance[itemIndex])

                                dist = iSampleMx[paramIndex][isampleIndex]

                                perturbed[fileID][zone][prop][itemIndex] = \
                                    minval+dist*(maxval-minval)

                                paramIndex += 1

                                tempParaValues.append(str(perturbed[fileID][zone][prop][itemIndex]))

                    elif isinstance(corrInstance, list) and len(corrInstance) == 1:

                        minval = float(corrInterval[0][0])
                        maxval = float(corrInterval[0][1])

                        refval = float(corrInstance[0])
                        dist = iSampleMx[paramIndex][isampleIndex]
                        perturbed[fileID][zone][prop][0] = minval+dist*(maxval-minval)

                        paramIndex += 1
                        tempParaValues.append(str(perturbed[fileID][zone][prop][0]))

                    else:

                        minval = float(self.intervals[fileID][zone][prop][0])
                        maxval = float(self.intervals[fileID][zone][prop][1])

                        refval = float(perturbed[fileID][zone][prop])
                        dist = iSampleMx[paramIndex][isampleIndex]
    
                        perturbed[fileID][zone][prop] = \
                            minval+dist*(maxval-minval)
    
                        paramIndex += 1

                        tempParaValues.append(str(perturbed[fileID][zone][prop])) 

        self.paramValue.append(tempParaValues)

        return perturbed