import os
import shutil
import re
import copy
import gzip

class StreamData:

    data = {}

    def __init__(self, ifileNames, ireadDicts):

        self.fileNames = ifileNames

        self.readDicts = ireadDicts

        self.paramTable = []

        self.toPrint = []

    def writeDataTable(self, paramName, pertIntervals, taggedParams, paramValues, resDir):

        counter = 0
        for group in pertIntervals:
            
            for zone in pertIntervals[group]:

                for prop in  pertIntervals[group][zone]:

                    print(prop, zone, group)
                    if (group in taggedParams.keys()):
                        if (zone in taggedParams[group].keys()):
                            if (prop in taggedParams[group][zone].keys()):
                                if(isinstance(pertIntervals[group][zone][prop][0],list)):
                                    for entry in pertIntervals[group][zone][prop]:
                                        if abs(entry[1]-entry[0])>0.0:
                                            self.toPrint.append(counter)
                                            counter += 1
                                else:
                                    self.toPrint.append(counter)
                                    counter += 1
                            else:
                                if(isinstance(pertIntervals[group][zone][prop][0],list)):
                                    for entry in pertIntervals[group][zone][prop]:
                                        if abs(entry[1]-entry[0])>0.0:
                                            counter += 1
                                else:
                                    counter += 1
                        else:
                            if(isinstance(pertIntervals[group][zone][prop][0],list)):
                                for entry in pertIntervals[group][zone][prop]:
                                    if abs(entry[1]-entry[0])>0.0:
                                        counter += 1
                            else:
                                counter += 1

                    else:
                        if(isinstance(pertIntervals[group][zone][prop][0],list)):
                            for entry in pertIntervals[group][zone][prop]:
                                if abs(entry[1]-entry[0])>0.0:
                                    counter += 1
                        else:
                            counter += 1

        if (len(resDir) > 1):

            self.toPrint.append(counter)

            for paramCombination in paramValues:
            
                for timeInstance in resDir:
    
                    tempList = paramCombination.copy()
    
                    tempList.append(timeInstance)
    
                    self.paramTable.append(tempList)

        else:

            self.paramTable =  paramValues.copy()

        tempDir = os.path.join(os.getcwd(), 'ROMFiles/', paramName) 
        outFile = open(tempDir, 'w')
        outFile.write(str(len(self.paramTable)) +" "+str(len(self.toPrint))+"\n")
        outFile.write("(\n")
        for line in self.paramTable:
            outFile.write("("+" ".join([line[i] for i in self.toPrint])+")\n")
        outFile.write(")\n") 
        outFile.close()

    def readData(self, ioriginalDirs, isourceDirs):
       
        for fileID in ioriginalDirs:

            if not os.path.exists(ioriginalDirs[fileID]):
                os.makedirs(ioriginalDirs[fileID])

            shutil.copyfile(
                os.path.join(isourceDirs[fileID], self.fileNames[fileID]), 
                os.path.join(ioriginalDirs[fileID], self.fileNames[fileID]))

            if self.fileNames[fileID].endswith(".gz"):
                file = gzip.open(os.path.join(ioriginalDirs[fileID], 
                                         self.fileNames[fileID]), "rb")
            else:
                file = open(os.path.join(ioriginalDirs[fileID], 
                                         self.fileNames[fileID]), "r")

            rawText = file.read()
            file.close()

            self.data[fileID] = self.processText(rawText, fileID)

    def processText(self, irawText, ifileID):

        lblText = irawText.split("\n")

        zoneProps = {}

        for zone in self.readDicts[ifileID]:

            prop = {}

            found = False

            lIndex = 0
            while lIndex < len(lblText):

                if len(lblText[lIndex].split())!=0:
                    if zone=="notInBrackets":
                        found=True
                    if zone==lblText[lIndex].split()[0]:
                        found = True
                    if "}" in lblText[lIndex].split():
                        found = False

                if found and (len(lblText[lIndex].split())!=0):

                    for pKeyWord in self.readDicts[ifileID][zone].keys():
                    
                        if (lblText[lIndex].lstrip().startswith(pKeyWord)):

                            temp = self.processLine(lblText[lIndex].strip().strip(pKeyWord))

                            prop[pKeyWord] = temp

                lIndex = lIndex + 1

            zoneProps[zone] = prop

        return zoneProps

    def processLine(self, irawLine):

        print(irawLine)
        if irawLine.split()[0].startswith("("):
            rawvec = irawLine[irawLine.find("(")+1:irawLine.find(")")]
            vec = rawvec.split()
            return vec
        else:
            return irawLine.split()[0].strip(';')

    def writeSamples(self, isampleStruct, ioriginalDirs, iperturbedDirs):

        for fileName in iperturbedDirs:
            if not os.path.exists(iperturbedDirs[fileName]):
                os.makedirs(iperturbedDirs[fileName])

            if self.fileNames[fileName].endswith(".gz"):
                file = gzip.open(os.path.join(ioriginalDirs[fileName], 
                                         self.fileNames[fileName]), "rb")
            else:
                file = open(os.path.join(ioriginalDirs[fileName], 
                                         self.fileNames[fileName]), "r")

            rawText = file.read()
            file.close()

            splitText = rawText.split("\n")

            for sampleIndex in range(0,len(isampleStruct)):

                sample = isampleStruct[sampleIndex][fileName]

                lblText = list(splitText)
            
                found = False

                lIndex = 0
                while lIndex < len(lblText):

                    if len(lblText[lIndex].split()) != 0:

                        if "notInBrackets" in sample:

                            selectedZone = sample["notInBrackets"]

                            found=True

                        if lblText[lIndex].split()[0] in sample:

                            selectedZone = sample[lblText[lIndex].split()[0]]

                            found = True

                        elif lblText[lIndex].split()[0] == "}":

                            found = False

                    if found and len(lblText[lIndex].split()) != 0:

                        rawLine = lblText[lIndex]

                        splitLine = rawLine.split()

                        for pKeyWord in selectedZone.keys():

                            if (rawLine.lstrip().startswith(pKeyWord)):

                                newLine = rawLine[0:rawLine.find(pKeyWord)]

                                newLine = newLine + pKeyWord

                                if isinstance(selectedZone[pKeyWord], list):
                                    newLine = newLine +" ("+ \
                                              " ".join(
                                                [str(x) for x in selectedZone[pKeyWord]])+");"
                                else:
                                    newLine = newLine + " " + \
                                              str(selectedZone[pKeyWord]) + ";"

                                lblText[lIndex] = newLine

                    lIndex = lIndex + 1

                file = open(os.path.join(iperturbedDirs[fileName], self.fileNames[fileName]+"_"+str(sampleIndex)), "w")
                rawText = file.write("\n".join(lblText))
                file.close()

    def printProp(self):

        for fileID in self.data:
            print(fileID)
            for zone in self.data[fileID]:
                print(self.data[fileID])





