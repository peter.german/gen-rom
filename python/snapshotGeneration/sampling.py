
import numpy as np
import sys
import random
import math
import itertools
from SALib.sample import saltelli
from pyDOE import *

class Sampling:

    def __init__(self):

        self.ihsMx = []

        self.lhsMx = []

        self.tensorMx = []

        self.uniformMx =  []

    def computeIHS (self, dim_num, point_num, duplication, seed):

        np.random.seed(seed)

        opt = point_num/pow(point_num,(1.0/dim_num))

        x = np.zeros((dim_num, point_num))
        x[:,point_num-1] = np.random.randint(point_num, size=dim_num)

        avail = np.zeros((dim_num, point_num))

        for j in range(0,point_num):
            avail[:,j] = j

        for i in range(0,dim_num):
            avail[i,int(x[i,point_num-1])] = point_num-1

        for count in range(point_num-2, 0, -1):

            cd = (count+1)*duplication

            coll_list = np.zeros(cd)
            point = np.zeros((dim_num,cd))

            for i in range(0, dim_num):
                for k in range(0, duplication):
                    coll_list[k*(count+1):(k+1)*(count+1)-1] = avail[i,0:count];

                pks=np.random.uniform(0,1, size=cd)

                for k in range(cd-1, 0, -1):
                    point_index = int(math.floor(k*pks[k]))
                    point[i,k] = coll_list[point_index]
                    coll_list[point_index] = coll_list[k]

            min_all = sys.float_info.max
            best = 0

            for k in range(0,cd):

                min_can = sys.float_info.max

                for j in range(count+1,point_num):

                    dist = 0.0

                    for i in range(0,dim_num):

                        dist = dist + (point[i,k]-x[i,j])**2

                    dist = math.sqrt(dist)
                    min_can = min(min_can, dist)


                if abs(min_can-opt) < min_all:
                    min_all = abs (min_can-opt)
                    best = k

            x[:,count] = point[:,best]

            for i in range(0,dim_num):
                for j in range(0,point_num):
                    if avail[i,j] == x[i,count]:
                        avail[i,j] = avail[i,count]

        x[:,0] = avail[:,0]

        x = (x+0.5)/point_num

        self.ihsMx = x

    def computeLHS (self, dim_num, point_num, seed):

        np.random.seed(seed)

        self.lhsMx = lhs(dim_num, samples=point_num, criterion="c")
        self.lhsMx = np.transpose(self.lhsMx)

    def computeTensor (self, dim_num, point_num):

        print(pow(point_num, 1/dim_num))
        print(round(pow(point_num, 1/dim_num)))

        if math.isclose(pow(point_num, 1/dim_num), round(pow(point_num, 1/dim_num))):

            pointperdim = int(round(pow(point_num, 1/dim_num)))

            singlevector = np.linspace(0, 1, pointperdim)

            x = np.zeros((dim_num, point_num))

            perm = list(itertools.product(singlevector, repeat = dim_num))

            print(perm)

            for permElem in range(len(perm)):
                x[:,permElem] = perm[permElem]

            print(x)

            self.tensorMx = x

        else:
            print("Check your input, pointnum is not an exponent of dimnum")

    def computeUniform (self, dim_num, point_num, seed):

        np.random.seed(seed)

        self.uniformMx = np.random.uniform(low=0.0, high=1.0, size=(dim_num, point_num))

    def computeUniformSobol (self, dim_num, point_num):

        problem = {
                'num_vars': dim_num,
                'names': ['x'+str(i) for i in range(dim_num)],
                'bounds': [[0.0, 1.0] for i in range(dim_num)]
        }

        if not (point_num % (dim_num+2) == 0):
            raise ValueError('Number not divisible by the dimensions + 2')

        self.uniformMx = np.transpose(saltelli.sample(problem, int(point_num/(dim_num+2))))

        print(self.uniformMx)


    def getIHS (self):

        return(self.ihsMx)

    def getTensor (self):

        return(self.tensorMx)

    def getUniform (self):

        return(self.uniformMx)

    def getLHS(self):

        return(self.lhsMx)



