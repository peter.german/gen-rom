import sys
import os
import subprocess
import copy
import shutil
from operator import itemgetter

from sampling import Sampling

class Production:

    def __init__(self,
                 inoSamples,
                 iresDir,
                 itargetDir,
                 iDatFileName,
                 icommand,
                 iloFields,
                 iloFieldsMod):

        self.noSamples = inoSamples

        self.resDir = iresDir

        self.targetDir = itargetDir

        self.datFileName = iDatFileName

        self.command = icommand

        self.loFields = iloFields

        self.loFieldsMod = iloFieldsMod

        self.fileIndex = 0

    def createSnapshots(self,
                        paramTable,
                        taggedParams,
                        ipertDirs,
                        iorgDirs,
                        isrcDirs):

        sortedLists = []
        temp = []

        pertDirs = ipertDirs
        srcDirs = isrcDirs
        orgDirs = iorgDirs

        self.fileIndex = 0

        sortedLists = {}

        transient = False
        if len(self.resDir) > 1:
            transient = True

        tmpParamTable = []
        for elem in paramTable:
            if transient:
                if (elem[0:-1] not in tmpParamTable):
                    tmpParamTable.append(elem[0:-1])
            else:
                tmpParamTable.append(elem)

        for subDir in pertDirs:

            print(pertDirs[subDir])
            print(os.listdir(pertDirs[subDir]))

            temp = [fileName.split("_") for fileName in os.listdir(pertDirs[subDir])]

            print(temp)
 
            sortedLists[subDir] = ["_".join(entry) for entry in 
                              sorted(temp, key=lambda x: (float(x[1])))]


        for sampleIndex in range(0, self.noSamples):

            correspondingFiles = {}

            for fileType in sortedLists:
                correspondingFiles[fileType] = sortedLists[fileType][sampleIndex]
            print( correspondingFiles)
            print("#############################################################")
            print(" Running sample: " + str(sampleIndex))
            print("#############################################################")

            self.copyInputFiles(pertDirs, srcDirs, correspondingFiles)

            self.writeCurrentParameterList(tmpParamTable, taggedParams, sampleIndex)
            
            self.runCalculation()

            self.copyResults(sampleIndex)

            self.restoreOriginal(orgDirs, srcDirs) 


    def writeCurrentParameterList(self, paramTable, taggedParams, sIndex):

        tagged_params_tmp = copy.copy(taggedParams)
        if (len(self.resDir) >1):
            tagged_params_tmp = taggedParams[0:-1]
        print(tagged_params_tmp)

        tempDir = os.path.join(os.getcwd(), 'ROMFiles/currentParameters') 
        outFile = open(tempDir, 'w')
        
        outFile.write(str(len(tagged_params_tmp))+" ")
        outFile.write("("+" ".join([paramTable[sIndex][i] for i in tagged_params_tmp])+")\n")
        outFile.close()

        print("("+" ".join(paramTable[sIndex])+")\n")
        print("("+" ".join([paramTable[sIndex][i] for i in tagged_params_tmp])+")")

    def copyResults(self, sampleIndex):

        for timeDir in self.resDir:

            tempDir = os.path.join(os.getcwd(), timeDir)  

            for root, dirs, files in os.walk(tempDir):
                for name in files:

                    if os.path.splitext(name)[0] in self.loFields:

                        ind = self.loFields.index(os.path.splitext(name)[0])

                        relDir = os.path.relpath(root, tempDir)

                        dirName = os.path.join(os.getcwd(), 
                                            self.targetDir,
                                            str(self.fileIndex),
                                            relDir)
                        if not os.path.exists(dirName):
                            os.makedirs(dirName)

                        new_name = name.replace(os.path.splitext(name)[0], self.loFieldsMod[ind])

                        shutil.copyfile(os.path.join(root, name),
                                    os.path.join(dirName, new_name))

            self.fileIndex = self.fileIndex + 1


        datDirName = os.path.join(os.getcwd(), "ROMFiles", "datFiles", str(sampleIndex))
        print(datDirName)
        if not os.path.exists(datDirName):
            os.makedirs(datDirName)
        shutil.copyfile(os.path.join(os.getcwd(),self.datFileName), os.path.join(datDirName, self.datFileName.split("/")[-1]))                    

    def runCalculation(self):

        command_run = subprocess.call(self.command)

        if command_run == 1:
            sys.exit()


    def copyInputFiles(self, ipertDirs, isrcDirs, ifiles):

        for dirIndex in ipertDirs:

            cpfrom = os.path.join(ipertDirs[dirIndex], ifiles[dirIndex])

            cpto = os.path.join(isrcDirs[dirIndex], 
                                ifiles[dirIndex].split("_")[0])

            shutil.copyfile(cpfrom, cpto)

    def restoreOriginal(self, iorgDirs, isrcDirs):

        for dirIndex in iorgDirs:

            files = os.listdir(iorgDirs[dirIndex])

            for fileID in files:

                cpfrom = os.path.join(iorgDirs[dirIndex], fileID)

                cpto = os.path.join(isrcDirs[dirIndex], fileID)

                shutil.copyfile(cpfrom, cpto)




                    
            










