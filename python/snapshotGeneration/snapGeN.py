#!/usr/bin/python

import os
import shutil
import re
import sys

from streamdata import StreamData
from perturbing import Perturb
from production import Production

sys.path.append(os.getcwd())
import snapGenInput 

print("-------------------------------------------------------------------------------")
print(" Reading snapshot controls from snapGenInput.")
print("-------------------------------------------------------------------------------")

noSamples = snapGenInput.noSamples
sampling = snapGenInput.sampling
resultDir = snapGenInput.resultDir
targetDir = snapGenInput.targetDir
pertIntervals = snapGenInput.pertIntervals
resultsToPrint = snapGenInput.resultsToPrint
resultsToPrintMod = resultsToPrint
datName = snapGenInput.datName
try:
	resultsToPrintMod = snapGenInput.resultsToPrintMod
	if (len(snapGenInput.resultsToPrintMod) != len(resultsToPrint)):
		raise Exception("The length of resultsToPrintMod should be the same as the length of resultsToPrint")
except:
	pass
fileNames = snapGenInput.fileNames
srcDirs = snapGenInput.srcDirs
origDirs = snapGenInput.origDirs
pertDirs = snapGenInput.pertDirs
command = snapGenInput.command
paramName = snapGenInput.paramTableName
taggedParameters = snapGenInput.pertIntervals
try:
	taggedParameters = snapGenInput.taggedParameters
except:
	pass
 
print("-------------------------------------------------------------------------------")
print(" Reading data for perturbation.")
print("-------------------------------------------------------------------------------")

fileStream = StreamData(fileNames, pertIntervals)
fileStream.readData(origDirs, srcDirs)

fileStream.printProp()
 
print("-------------------------------------------------------------------------------")
print(" Perturbing data.")
print("-------------------------------------------------------------------------------")

pert = Perturb(fileStream.data, pertIntervals)

pert.createSamples(noSamples, sampling)

print("-------------------------------------------------------------------------------")
print(" Writing sample files.")
print("-------------------------------------------------------------------------------")

fileStream.writeSamples(pert.pertSamples, origDirs, pertDirs)
fileStream.writeDataTable(paramName, pertIntervals, taggedParameters, pert.paramValue, resultDir)

print("-------------------------------------------------------------------------------")
print(" Running calculations with different inputs.")
print("-------------------------------------------------------------------------------")
 
prod = Production(noSamples, resultDir, targetDir, datName, command, resultsToPrint, resultsToPrintMod)
prod.createSnapshots(fileStream.paramTable, fileStream.toPrint, pertDirs, origDirs, srcDirs)