import openfoamparser as ofp
import numpy as np
import os

from utilities import read_list, read_matrix

class NeutroDEIM:

    def __init__(self, name, region_name, control_dict, rom_dict, mesh):

        self.name = name
        self.region_name = region_name

        self.mesh = mesh

        self.control_dict = control_dict
        self.rom_dict = rom_dict

        self.function = None
        if(name == "TCool"):
            self.function = lambda T : T 
        elif(name == "logTNeutro"):
            self.function = lambda T : np.log(T)
        elif(name == "sqrtTNeutro"):
            self.function = lambda T : np.sqrt(T)
        else:
            raise ValueError("The name of the DEIM in not valid!", 
                             "Should be one of TCool/sqrtTNeutro/logTNeutro")

        self.basis_functions = []
        self.reduced_basis_functions = []

        self.interpolation_matrix = None

        self.solution = None     

    def load_reduced_basis_vectors(self, path, no_vectors):

        print("Loading reduced nonlinear base for "+self.name+".")

        self.reduced_basis_functions = []
        for i in range(no_vectors):
            fname = os.path.join(path, "redNonlinBase_"+self.name+str(i))
            self.reduced_basis_functions.append(read_list(fname, self.control_dict.binary))

    def read_interpol_matrix(self, path):

        print("Loading reduced interpolation matrix for "+self.name+".")

        fname = os.path.join(path, "redInterpolMx"+self.name)
        self.interpolation_matrix = read_matrix(fname, self.control_dict.binary)

    def compute_coefficients(self, temp_solution):

        reconstructed = self.reduced_basis_functions[0]*0.0
        for i in range(len(self.reduced_basis_functions)):
            reconstructed += temp_solution[i]*self.reduced_basis_functions[i]

        transformed = self.function(reconstructed)

        self.solution = self.interpolation_matrix.dot(transformed) 




            






        








