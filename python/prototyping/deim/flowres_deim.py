import numpy as np
import openfoamparser as ofp
import os
from utilities import read_list, read_matrix

class FlowresDEIM:

    def __init__(self, name, rom_dict, phase_dict, thermo_dict, bd_dict):

        self.name = name
        self.rom_dict = rom_dict
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict
        self.solution = np.zeros(rom_dict.no_flowres_bases)
        self.red_basis_functions = []
        self.red_nonlinear_base = []
        self.red_own_base = []
        self.chosen_directions = []
        self.chosen_cells = []
        self.cell_gamma = []
        self.cell_A = []
        self.cell_B = []
        self.cell_Dh = []
        self.cell_mu = []
        self.cell_rho = []
        self.interpolation_matrix = []
        self.red_flowres_vectors = []
        self.flowres_vector = []

    def load_self_operators(self, path, binary):

        print("Loading self matrices for "+self.name+".")

        no_bases = self.rom_dict.no_flowres_bases
        no_velocity_bases = self.rom_dict.no_velocity_bases + self.rom_dict.no_supremizer_bases

        fname = os.path.join(path, "redInterpolMx"+self.name)
        self.interpolation_matrix = read_matrix(fname, binary)

        fname = os.path.join(path, "cells"+self.name)
        self.chosen_cells = read_list(fname, binary, data_type='i')

        fname = os.path.join(path, "directions"+self.name)
        self.chosen_directions = read_list(fname, binary, data_type='i')

        fname = os.path.join(path, "rho_"+self.name)
        self.cell_rho = read_list(fname, binary)

        fname = os.path.join(path, "mu_"+self.name)
        self.cell_mu = read_list(fname, binary)

        fname = os.path.join(path, "D_"+self.name)
        self.cell_Dh = read_list(fname, binary)

        fname = os.path.join(path, "gamma_"+self.name)
        self.cell_gamma = read_list(fname, binary)

        fname = os.path.join(path, "darcyConst_"+self.name)
        self.cell_A = read_list(fname, binary)

        fname = os.path.join(path, "darcyExp_"+self.name)
        self.cell_B = read_list(fname, binary)

        for basei in range(no_bases):

            fname = os.path.join(path, "redFlowResSet_"+self.name+str(basei))
            self.red_flowres_vectors.append(read_list(fname, binary))

            fname = os.path.join(path, "redOwnBase_"+self.name+str(basei))
            self.red_own_base.append(read_list(fname, binary, elems_per_entry=3))

        for basei in range(no_velocity_bases):

            fname = os.path.join(path, "redNonlinBase_"+self.name+str(basei))
            self.red_nonlinear_base.append(read_list(fname, binary, elems_per_entry=3))

    def sum_operators(self, velocity_solution):

        Dh = self.cell_Dh 
        gamma = self.cell_gamma
        rho = self.cell_rho
        mu = self.cell_mu
        A = self.cell_A
        B = self.cell_B

        no_own_bases = self.rom_dict.no_flowres_bases
        no_velocity_bases = self.rom_dict.no_velocity_bases + self.rom_dict.no_supremizer_bases

        velocity = np.zeros((no_own_bases,3))

        for veli in range(no_velocity_bases):
            velocity += velocity_solution[veli] * self.red_nonlinear_base[veli]

        sum_coeffs = np.zeros(len(self.red_own_base))

        for celli in range(no_own_bases):
            real_u = velocity[celli]/gamma[celli]
            Re = max(rho[celli] * Dh[celli] * np.linalg.norm(real_u) / mu[celli], 10.0)
            frictFact = A[celli]*pow(Re, B[celli])
            dragCoeff = 0.5 * rho[celli] * np.linalg.norm(real_u) * frictFact / Dh[celli]
            sum_coeffs[celli] = - gamma[celli] * dragCoeff * real_u[self.chosen_directions[celli]]

        sum_coeffs = self.interpolation_matrix.dot(sum_coeffs) 

        self.flowres_vector = np.zeros((no_velocity_bases,1))
        for basei in range(no_own_bases):

            self.flowres_vector += sum_coeffs[basei]*self.red_flowres_vectors[basei]

