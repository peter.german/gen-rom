from parameterparser import CrossSections
from parameterparser import ROMDictionary
from parameterparser import ThermoDictionary
from parameterparser import PhaseDictionary
from parameterparser import BoundaryDictionary
from parameterparser import ControlDictionary
from parameterparser import ReactorStateDictionary

from solvers import FluidThermoNeutroSolver

from openfoamparser import mesh_parser
import os

control_dict = ControlDictionary("./system/controlDict")
control_dict.read_dictionary()

neutro_mesh = mesh_parser.FoamMesh("constant/neutroRegion/polyMesh/")
neutro_mesh.read_cell_centres(control_dict.start_time+'/neutroRegion/C')
neutro_mesh.read_cell_volumes(control_dict.start_time+'/neutroRegion/V')

fluid_mesh = mesh_parser.FoamMesh("constant/fluidRegion/polyMesh/")
fluid_mesh.read_cell_centres(control_dict.start_time+'/fluidRegion/C')
fluid_mesh.read_cell_volumes(control_dict.start_time+'/fluidRegion/V')

xs = CrossSections("./constant/neutroRegion/nuclearData",
	               "./constant/neutroRegion/nuclearDataFuelTemp",
	               "./constant/neutroRegion/nuclearDataRhoCool")
xs.read_cross_sections(verbose=True)
xs.read_cross_sections_fueltemp(verbose=True)
xs.read_cross_sections_rhocool(verbose=True)

xs.create_fueltemp_feedback_coefficients(verbose=True)
xs.create_rhocool_feedback_coefficients(verbose=True)

reactor_state = ReactorStateDictionary("./constant/neutroRegion/reactorState")
reactor_state.read_dictionary()

rom_dict = ROMDictionary("./system/ROMDict")
rom_dict.read_dictionary()

thermo_dict = ThermoDictionary("./constant/fluidRegion/thermophysicalProperties")
thermo_dict.read_dictionary()

phase_dict = PhaseDictionary("./constant/fluidRegion/phaseProperties")
phase_dict.read_dictionary()

bd_dict_fluid = BoundaryDictionary("./constant/fluidRegion/polyMesh/boundary")
bd_dict_fluid.read_dictionary()

bd_dict_neutro = BoundaryDictionary("./constant/neutroRegion/polyMesh/boundary")
bd_dict_neutro.read_dictionary()

nts = FluidThermoNeutroSolver(control_dict, xs, reactor_state, rom_dict, phase_dict, thermo_dict, 
	                          bd_dict_fluid, bd_dict_neutro, fluid_mesh, neutro_mesh)
nts.create_rom_objects()
nts.load_rom_bases()
nts.load_rom_operators()

nts.solve()













