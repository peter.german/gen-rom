import os
import struct
import numpy as np
import gzip
import string
import re

def read_matrix(fn, binary):

    if not os.path.exists(fn):
        print("Can not open file " + fn)
        return None

    if not binary:
        f = None
        if fn.endswith(".gz"):
            f = gzip.open(fn, "r")
        else:
            f = open(fn, "r")
        
        content = f.read()
        
        split_content = content.split()

        if "{" in content:
            nrows = int(split_content[0])
            ncols = int(split_content[1].split("{")[0])
            matrix = np.zeros((nrows,ncols))
            value = re.findall(r"\{([^}]+)\}", content)
            matrix.fill(float(value[0]))
        else:
            nrows = int(split_content[0])
            ncols = int(split_content[1].split("(")[0])
            matrix = np.zeros((nrows,ncols))

            counter = int(0)
            for elem in split_content[1:]:
                values = elem.replace(str(ncols)+"((",'').replace("))",'').split(")(")
                for value in values:
                    matrix[counter // nrows, counter % ncols] = float(value)
                    counter += 1
        return matrix
    else:

        f = open(fn, "br")   
        content = f.readlines()

        header = content[0].split(b'(')[0]

        n_rows = int(header.split()[0])
        n_cols = int(header.split()[1])
    
        bcontent = b''.join(content)[len(header):]
    
        tmx = np.array(struct.unpack('{}d'.format(n_rows*n_cols), 
                        bcontent[struct.calcsize('c'):n_rows*n_cols*struct.calcsize('d')+struct.calcsize('c')]))
    
        matrix = tmx.reshape((n_rows, n_cols))  

        return matrix

    return None

def read_list(fn, binary, elems_per_entry=1, data_type='d'):

    if not os.path.exists(fn):
        print("Can not open file " + fn)
        return None

    if not binary:
        f = None
        if fn.endswith(".gz"):
            f = gzip.open(fn, "r")
        else:
            f = open(fn, "r")
        
        content = f.read()
        
        split_content = content.split()

        if "{" in content:
            n_elems = int(split_content[0].split("{")[0])
            flist = np.zeros((n_elems,elems_per_entry))
            value = re.findall(r"\{([^}]+)\}", content)
            flist.fill(float(value[0]))

            print(flist)
        else:
            n_elems = int(split_content[0].split("(")[0])
            flist = np.zeros((n_elems,elems_per_entry))
            counter = int(0)

            for elem in split_content[1:]:
                values = elem.replace(str(n_elems)+"(",'').replace("))",'').split(")(")
                for value in values:
                    flist[counter // elems_per_entry, counter % elems_per_entry] = float(value.replace("(","").replace(")",""))
                    counter += 1
        return flist.flatten()
    else:

        f = open(fn, "br")   
        content = f.readlines()

        n_elems = int(content[1].strip(b'\n'))

        bcontent = b''.join(content[2:])

        tlist = np.array(struct.unpack(('{}'+data_type).format(n_elems*elems_per_entry), 
                       bcontent[struct.calcsize('c'):n_elems*elems_per_entry*struct.calcsize(data_type)+struct.calcsize('c')]))
        flist = tlist.reshape((n_elems,elems_per_entry))

        return flist

    return None


