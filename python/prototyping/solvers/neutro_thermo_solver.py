import numpy as np 
import os

from .diffusion_k_solver import DiffusionKSolver
from .thermo_solver import ThermoSolver

class NeutroThermoSolver:

    def __init__(self, control_dict, xs, rom_dict, phase_dict, thermo_dict, bd_dict_fluid, bd_dict_neutro, fluid_mesh, neutro_mesh):

        self.control_dict = control_dict
        self.xs = xs
        self.rom_dict = rom_dict
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict_fluid = bd_dict_fluid
        self.bd_dict_neutro = bd_dict_neutro
        self.neutro_mesh = neutro_mesh
        self.fluid_mesh = fluid_mesh

        self.sol_field_names = []

        self.neutro_solver = DiffusionKSolver(control_dict, xs, rom_dict, phase_dict, thermo_dict, bd_dict_neutro, neutro_mesh, True, True)
        self.thermo_solver = ThermoSolver(control_dict, rom_dict, xs, phase_dict, thermo_dict, bd_dict_fluid, fluid_mesh)

    def create_rom_objects(self):

        self.thermo_solver.create_rom_objects()
        self.sol_field_names = self.sol_field_names + self.thermo_solver.sol_field_names
        self.neutro_solver.create_rom_objects()
        self.sol_field_names = self.sol_field_names + self.neutro_solver.sol_field_names

    def load_rom_bases(self):

        self.thermo_solver.load_rom_bases()
        self.neutro_solver.load_rom_bases()

    def load_rom_operators(self):

        self.thermo_solver.load_rom_operators()
        self.neutro_solver.load_rom_operators()

    def solve(self):

        self.thermo_solver.solve()

        self.neutro_solver.compute_sytem_matrices(temp_solution=self.thermo_solver.energy_rom.solution)

        self.neutro_solver.solve()






        