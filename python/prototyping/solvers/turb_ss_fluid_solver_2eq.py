from roms import VelocityROM
from roms import PressureROM
from roms import NutROM
from roms import AlphatROM

import copy 
import numpy as np
import scipy as sp
from scipy.optimize import fsolve

class TurbSteadyFluidSolverTwoEq:
    
    def __init__(self, control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh):

        self.control_dict = control_dict
        self.rom_dict = rom_dict
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict
        self.mesh = mesh

        self.velocity_rom = None
        self.pressure_rom = None

        self.nut_rom = None
        self.alphat_rom = None

        self.sol_field_names = []

        self.system_mx = None 
        self.rhs = None

        self.combined_solution = []

        self.vel_size = 0
        self.pres_size = 0
        self.sys_size = 0

    def has_field(self, name):

        d = False
        if self.velocity_rom.name == name:
            d=True
        if self.pressure_rom.name == name:
            d=True
        return d

    def create_rom_objects(self):

        rom_dict = self.rom_dict
        phase_dict = self.phase_dict
        control_dict = self.control_dict
        thermo_dict = self.thermo_dict
        bd_dict = self.bd_dict
        mesh = self.mesh

        self.velocity_rom = VelocityROM("UDarcy", "fluidRegion", control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh)
        self.sol_field_names.append("UDarcy")

        self.pressure_rom = PressureROM("p_rgh", "fluidRegion", control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh)
        self.sol_field_names.append("p_rgh")

        self.nut_rom = NutROM("nut", "fluidRegion", control_dict, rom_dict, phase_dict, mesh)
        self.sol_field_names.append("nut")

        self.alphat_rom = AlphatROM("alphat", "fluidRegion", control_dict, rom_dict, phase_dict, mesh)
        self.sol_field_names.append("alphat")

    #def reset_phase_dict(self):
    #
    #    self.phase_dict = copy.copy(self.xs_orig)
    #
    #    self.velocity_rom.phase_dict = self.phase_dict
    #    self.pressure_rom.phase_dict = self.phase_dict
    #
    #def change_pump_power(self, new_value):
    #
    #    self.phase_dict.momentum_source['pump'][2] = new_value
    #
    #    self.velocity_rom.phase_dict = self.phase_dict
    #    self.pressure_rom.phase_dict = self.phase_dict

    def load_rom_bases(self):

        self.velocity_rom.load_basis_vectors("./ROMFiles/bases/", 
                                             self.rom_dict.no_velocity_bases)
        self.velocity_rom.load_sup_bases("./ROMFiles/supremizerBases/", 
                                                 self.rom_dict.no_supremizer_bases)
        self.velocity_rom.combine_supremizer_bases()

        self.velocity_rom.initialize_solution()

        self.pressure_rom.load_basis_vectors("./ROMFiles/bases/",
                                             self.rom_dict.no_pressure_bases)
        self.pressure_rom.initialize_solution()

        self.vel_size = len(self.velocity_rom.solution)
        self.pres_size = len(self.pressure_rom.solution)
        self.sys_size = self.vel_size + self.pres_size

        self.nut_rom.load_basis_vectors("./ROMFiles/bases/",
                                        self.rom_dict.no_nut_bases)
        self.nut_rom.initialize_solution()

        self.alphat_rom.load_basis_vectors("./ROMFiles/bases/",
                                           self.rom_dict.no_alphat_bases)
        self.alphat_rom.initialize_solution()

    def load_rom_operators(self):

        binary = self.control_dict.binary

        self.velocity_rom.load_self_operators("./ROMFiles/redOperators/MomentumROM", binary)
        self.pressure_rom.load_self_operators("./ROMFiles/redOperators/PressureROM", binary)
        self.nut_rom.load_self_operators("./ROMFiles/redOperators/nutROM", binary)
        self.alphat_rom.load_self_operators("./ROMFiles/redOperators/alphatROM", binary)

    def compute_sytem_matrix(self, solution):

        gamma = self.phase_dict.gamma
        mu = self.thermo_dict.mu
        rho = self.thermo_dict.rho

        self.system_mx = np.zeros((self.sys_size,self.sys_size))

        self.velocity_rom.sum_advection_terms(solution)

        adv_mx = self.velocity_rom.advection_mx
        ce_mx = self.velocity_rom.cont_err_mx
        diff_mx = self.velocity_rom.lam_diffusion_matrix
        dev_mx = self.velocity_rom.lam_deviatoric_matrix
        pg_mx = self.velocity_rom.press_grad_matrix
        div_mx = self.pressure_rom.div_mx

        turb_diff_mx = self.velocity_rom.turb_diff_matrix
        turb_dev_mx = self.velocity_rom.turb_dev_matrix

        self.system_mx[0:self.vel_size,0:self.vel_size] = \
            rho*adv_mx \
            - rho*ce_mx \
            - mu*dev_mx \
            - mu*diff_mx \
            - rho*turb_diff_mx \
            - rho*turb_dev_mx

        self.system_mx[0:self.vel_size,self.vel_size:self.sys_size] = pg_mx
        self.system_mx[self.vel_size:self.sys_size,0:self.vel_size] = rho*div_mx

    def compute_rhs(self, solution):

        rho = self.thermo_dict.rho
        mag_g = self.thermo_dict.mag_g
        beta_th = self.thermo_dict.beta_th

        self.rhs = np.zeros((self.sys_size,1))

        mom_src = self.phase_dict.momentum_source
        pump_src = self.velocity_rom.momentum_sources

        bq_src = self.velocity_rom.boussinesq_vector

        self.rhs[0:self.vel_size] = self.rhs[0:self.vel_size] + np.reshape(rho*mag_g*beta_th*bq_src, (self.vel_size,1))

        for zonei in range(len(self.phase_dict.zone_names)):
            zone = self.phase_dict.zone_names[zonei]
            if zone in mom_src:
                magnitude = np.linalg.norm(mom_src[zone])
                self.rhs[0:self.vel_size] = self.rhs[0:self.vel_size] + magnitude * pump_src[zonei]

        flowres = np.zeros((self.vel_size,1))
        for module in self.velocity_rom.flowres_deims:
            module.sum_operators(solution)
            self.rhs[0:self.vel_size] = self.rhs[0:self.vel_size] + module.flowres_vector
            flowres = flowres + module.flowres_vector

    def compute_residual(self, solution):

        self.compute_sytem_matrix(solution)

        self.compute_rhs(solution)

        res = self.system_mx.dot(solution) - np.concatenate(self.rhs)

        return res

    def solve(self, current_params, temp_solution=[]):

        self.nut_rom.solve(current_params)
        self.alphat_rom.solve(current_params)

        self.velocity_rom.sum_turb_diff_terms(self.nut_rom.solution)
        self.velocity_rom.sum_temperature_terms(temp_solution)

        guess = np.concatenate((self.velocity_rom.solution, self.pressure_rom.solution), axis=0)

        self.compute_sytem_matrix(guess)

        self.compute_rhs(guess)

        self.combined_solution = fsolve(self.compute_residual, guess)

        self.distribute_solution()

    def distribute_solution(self):

        self.velocity_rom.solution = self.combined_solution[0:self.vel_size]
        self.pressure_rom.solution = self.combined_solution[self.vel_size:self.sys_size]


        



