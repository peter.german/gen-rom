from roms import EnergyROM
from deim import NeutroDEIM

import copy 
import numpy as np
import scipy as sp
from scipy.optimize import fsolve

class ThermoSolver:
    
    def __init__(self, control_dict, rom_dict, xs, phase_dict, thermo_dict, bd_dict, mesh, neutro_mesh,
                 fueltemp_feedback=False, rhocool_feedback=False):

        self.control_dict = control_dict
        self.rom_dict = rom_dict
        self.xs = xs
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict
        self.mesh = mesh
        self.neutro_mesh = neutro_mesh

        self.energy_rom = None
        self.fueltemp_deim = None
        self.rhocool_deim = None

        self.sol_field_names = []

        self.system_mx = None 
        self.rhs = None

        self.fueltemp_feedback = fueltemp_feedback
        self.rhocool_feedback = rhocool_feedback

    def has_field(self, name):

        d = False
        if self.energy_rom.name == name:
            d=True

        return d

    def create_rom_objects(self):

        control_dict = self.control_dict
        rom_dict = self.rom_dict
        xs = self.xs
        phase_dict = self.phase_dict
        thermo_dict = self.thermo_dict
        bd_dict = self.bd_dict
        mesh = self.mesh
        neutro_mesh = self.neutro_mesh

        self.energy_rom = EnergyROM("T", "fluidRegion", xs, control_dict, rom_dict, 
                                    phase_dict, thermo_dict, bd_dict, mesh)
        self.sol_field_names.append("T")

        if (self.fueltemp_feedback):
            
            deim_name = None 
            if xs.fast_neutrons:
                deim_name = "logTNeutro"
            else: 
                deim_name = "sqrtTNeutro"

            self.fueltemp_deim = NeutroDEIM(deim_name, "neutroRegion", control_dict, rom_dict, neutro_mesh)

        if (self.rhocool_feedback):    
            self.rhocool_deim = NeutroDEIM("TCool", "neutroRegion", control_dict, rom_dict, neutro_mesh)

    def load_rom_bases(self):

        rom_dict = self.rom_dict

        self.energy_rom.load_basis_vectors("ROMFiles/bases/", rom_dict.no_temp_bases)
        self.energy_rom.initialize_solution()

        if (self.fueltemp_deim != None):
            self.fueltemp_deim.load_reduced_basis_vectors("./ROMFiles/redOperators/NeutroDEIM", rom_dict.no_temp_bases)

        if (self.rhocool_deim != None):
            self.rhocool_deim.load_reduced_basis_vectors("./ROMFiles/redOperators/NeutroDEIM", rom_dict.no_temp_bases)

    def load_rom_operators(self):

        binary = self.control_dict.binary
        self.energy_rom.load_self_operators("./ROMFiles/redOperators/EnergyROM", binary)
        self.energy_rom.load_deim_operators("./ROMFiles/redOperators/EnergyDEIM", binary)

        if (self.fueltemp_deim != None):
            self.fueltemp_deim.read_interpol_matrix("./ROMFiles/redOperators/NeutroDEIM")

        if (self.rhocool_deim != None):
            self.rhocool_deim.read_interpol_matrix("./ROMFiles/redOperators/NeutroDEIM")

    def compute_system(self, velocity_solution=[], alphat_solution=[], flux_solutions=[]):

        if (self.fueltemp_deim != None):
            self.fueltemp_deim.compute_coefficients(self.energy_rom.solution)
            self.energy_rom.sum_fueltemp_terms(self.fueltemp_deim.solution)
 
        if (self.rhocool_deim != None):
            self.rhocool_deim.compute_coefficients(self.energy_rom.solution)
            self.energy_rom.sum_rhocool_terms(self.rhocool_deim.solution)

        self.energy_rom.sum_fluid_operators(velocity_solution, alphat_solution)

        self.energy_rom.sum_neutronics_operators(flux_solutions, self.fueltemp_feedback, self.rhocool_feedback)

        no_zones = self.phase_dict.no_zones
        zone_names = self.phase_dict.zone_names

        cp = self.thermo_dict.cp
        mu = self.thermo_dict.mu
        Pr = self.thermo_dict.Pr
        rho = self.thermo_dict.rho
        Av = self.phase_dict.vol_surface
        alpha = self.phase_dict.heat_exchange_coeff
        Text = self.phase_dict.external_T
        beta_th = self.thermo_dict.beta_th

        diff_mx = self.energy_rom.diff_matrix
        vol_heat_src = self.energy_rom.vol_heat_src
        vol_neutro_src = self.energy_rom.vol_neutro_src
        vol_neutro_src = vol_neutro_src.reshape((len(vol_neutro_src),1))
        
        heat_transfer_tensor = self.energy_rom.heat_transfer_tensor
        heat_transfer_sources = self.energy_rom.heat_transfer_sources 

        adv_mx = self.energy_rom.advection_matrix
        conterr_mx = self.energy_rom.conterr_matrix
        turb_diff_mx = self.energy_rom.turb_diff_matrix

        no_own_bases = len(self.energy_rom.basis_functions)

        self.system_mx = np.zeros((no_own_bases,no_own_bases))
        self.rhs = np.zeros((no_own_bases,1))

        self.system_mx += rho*adv_mx - rho*conterr_mx - mu/Pr * diff_mx - turb_diff_mx
        self.rhs += 1.0/cp*vol_heat_src + 1.0/cp*vol_neutro_src

        for zonei in range(no_zones):

            if not zone_names[zonei] in Av.keys():
                continue

            zname = zone_names[zonei]

            self.system_mx += 1.0/cp*alpha[zname]*Av[zname]*heat_transfer_tensor[zonei]
            self.rhs += 1.0/cp*alpha[zname]*Av[zname]*Text[zname]*heat_transfer_sources[zonei]

    def solve(self, velocity_sol=[], alphat_sol=[], flux_sols=[]):

        self.compute_system(velocity_sol, alphat_sol, flux_sols)

        self.energy_rom.solution = np.linalg.solve(self.system_mx, self.rhs)



        



