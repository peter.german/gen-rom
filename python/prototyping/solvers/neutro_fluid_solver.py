import numpy as np 
import os

from diffusion_k_solver import DiffusionKSolver
from lam_ss_fluid_solver_1eq import LamSteadyFluidSolverOneEq

class NeutroFluidSolver:

    def __init__(self, control_dict, xs, rom_dict, phase_dict, thermo_dict, bd_dict, mesh):

        self.control_dict = control_dict
        self.xs = xs
        self.rom_dict = rom_dict
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict
        self.mesh = mesh

        self.sol_field_names = []

        self.neutro_solver = DiffusionKSolver(control_dict, xs, rom_dict, phase_dict, thermo_dict, bd_dict, mesh)
        self.fluid_solver = LamSteadyFluidSolverOneEq(control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh)

    def get_reconstructed_solution(self, name):
        
        for flux_rom in self.neutro_solver.flux_roms:
            if flux_rom.name == name:
                solution = flux_rom.get_reconstructed_solution()
                return(solution)
        for prec_rom in self.neutro_solver.prec_roms:
            if prec_rom.name == name:
                solution = prec_rom.get_reconstructed_solution()
                return(solution)
        if self.fluid_solver.velocity_rom.name == name:
            solution = self.fluid_solver.velocity_rom.get_reconstructed_solution()
            return(solution)
        if self.fluid_solver.pressure_rom.name == name:
            solution = self.fluid_solver.pressure_rom.get_reconstructed_solution()
            return(solution)
        
        return None


    def create_rom_objects(self):

        self.fluid_solver.create_rom_objects()
        self.sol_field_names = self.sol_field_names + self.fluid_solver.sol_field_names
        self.neutro_solver.create_rom_objects()
        self.sol_field_names = self.sol_field_names + self.neutro_solver.sol_field_names

    def load_rom_bases(self):

        self.fluid_solver.load_rom_bases()
        self.neutro_solver.load_rom_bases()

    def load_rom_operators(self):

        self.fluid_solver.load_rom_operators()
        self.neutro_solver.load_rom_operators()

    def solve(self):

        self.fluid_solver.solve()

        self.neutro_solver.sum_advection_tensors(self.fluid_solver.velocity_rom.solution)

        self.neutro_solver.compute_sytem_matrices()

        self.neutro_solver.solve()






        