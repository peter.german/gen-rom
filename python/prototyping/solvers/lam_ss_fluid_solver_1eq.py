from velocity_rom import VelocityROM
from pressure_rom import PressureROM

import copy 
import numpy as np
import scipy as sp
from scipy.optimize import fsolve

class LamSteadyFluidSolverOneEq:
    
    def __init__(self, control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh):

        self.control_dict = control_dict
        self.rom_dict = rom_dict
        self.phase_dict = phase_dict
        self.phase_dict_orig = copy.copy(phase_dict)
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict
        self.mesh = mesh
        self.velocity_rom = None
        self.pressure_rom = None

        self.sol_field_names = []

        self.system_mx = None 
        self.rhs = None

        self.combined_solution = []

    def has_field(self, name):

        d = False
        if self.velocity_rom.name == name:
            d=True
        if self.pressure_rom.name == name:
            d=True
        return d

    def get_figure(self):

        print("nothing yet")
        return(None)

    def create_rom_objects(self):

        rom_dict = self.rom_dict
        phase_dict = self.phase_dict
        thermo_dict = self.thermo_dict
        bd_dict = self.bd_dict
        mesh = self.mesh

        self.velocity_rom = VelocityROM("UDarcy", rom_dict, phase_dict, thermo_dict, bd_dict, mesh)
        self.sol_field_names.append("UDarcy")

        self.pressure_rom = PressureROM("p_rgh", rom_dict, phase_dict, thermo_dict, bd_dict, mesh)
        self.sol_field_names.append("p_rgh")

    def reset_phase_dict(self):

        self.phase_dict = copy.copy(self.xs_orig)

        self.velocity_rom.phase_dict = self.phase_dict
        self.pressure_rom.phase_dict = self.phase_dict

    def change_pump_power(self, new_value):

        self.phase_dict.momentum_source['pump'][2] = new_value

        self.velocity_rom.phase_dict = self.phase_dict
        self.pressure_rom.phase_dict = self.phase_dict

    def load_rom_bases(self):

        self.velocity_rom.load_basis_vectors("./ROMFiles/bases/")
        self.pressure_rom.load_basis_vectors("./ROMFiles/bases/")

    def load_rom_operators(self):

        binary = self.control_dict.binary

        self.velocity_rom.load_self_operators("./ROMFiles/redOperators/MomentumROM", binary)

    def compute_sytem_matrix(self, solution):

        gamma = self.phase_dict.gamma
        mu = self.thermo_dict.mu
        rho = self.thermo_dict.rho

        sys_size = len(self.velocity_rom.solution)
        self.system_mx = np.zeros((sys_size,sys_size))

        self.velocity_rom.sum_advection_terms(solution)

        adv_mx = self.velocity_rom.advection_mx
        ce_mx = self.velocity_rom.cont_err_mx
        diff_mx = self.velocity_rom.lam_diffusion_matrix
        dev_mx = self.velocity_rom.lam_deviatoric_matrix
        pg_mx = self.velocity_rom.press_grad_matrix

        self.system_mx = \
            rho*adv_mx \
            - rho*ce_mx \
            - mu*dev_mx \
            - mu*diff_mx \
            + pg_mx

    def compute_rhs(self, solution):

        sys_size = len(self.velocity_rom.solution)
        self.rhs = np.zeros((sys_size,1))

        mom_src = self.phase_dict.momentum_source
        pump_src = self.velocity_rom.momentum_sources

        for zonei in range(len(self.phase_dict.zone_names)):
            zone = self.phase_dict.zone_names[zonei]
            if zone in mom_src:
                magnitude = np.linalg.norm(mom_src[zone])
                self.rhs = self.rhs + magnitude * pump_src[zonei]

        flowres = np.zeros((sys_size,1))
        for module in self.velocity_rom.flowres_deims:
            module.sum_operators(solution)
            self.rhs = self.rhs + module.flowres_vector
            flowres = flowres + module.flowres_vector

    def compute_residual(self, solution):

        self.compute_sytem_matrix(solution)

        self.compute_rhs(solution)

        res = self.system_mx.dot(solution) - np.concatenate(self.rhs)

        return res

    def solve(self):

        self.combined_solution = fsolve(self.compute_residual, self.velocity_rom.solution)

        self.distribute_solution()

    def distribute_solution(self):

        self.velocity_rom.solution = self.combined_solution
        self.pressure_rom.solution = self.combined_solution


        



