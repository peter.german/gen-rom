from roms import FluxROM
from roms import PrecROM
from deim import NeutroDEIM
import copy 

import numpy as np
from scipy.sparse.linalg import eigs
import scipy 


class DiffusionKSolver:
    
    def __init__(self, control_dict, cross_sections, reactor_state, rom_dict, phase_dict, thermo_dict, 
                 bd_dict, mesh, fueltemp_feedback=False, rhocool_feedback=False):

        self.control_dict = control_dict
        self.xs = cross_sections
        self.reactor_state = reactor_state
        self.rom_dict = rom_dict
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict
        self.mesh = mesh

        self.flux_roms = []
        self.prec_roms = []
        self.fueltemp_deim = None
        self.rhocool_deim = None

        self.sol_field_names = []

        self.starts = []
        self.ends = []

        self.A_fiss = None 
        self.A = None

        self.keff = 0.0
        self.eig_vect = None

        self.fueltemp_feedback = fueltemp_feedback
        self.rhocool_feedback = rhocool_feedback

    def has_field(self, name):

        d = False
        for rom in self.flux_roms:
            if rom.name == name:
                d=True
        
        for rom in self.prec_roms:
            if rom.name == name:
                d=True
        return d

    def create_rom_objects(self):

        self.flux_roms = []
        self.prec_roms = []

        control_dict = self.control_dict
        xs = self.xs
        rom_dict = self.rom_dict
        phase_dict = self.phase_dict
        thermo_dict = self.thermo_dict
        bd_dict = self.bd_dict
        mesh = self.mesh

        if (self.fueltemp_feedback):
            
            deim_name = None 
            if xs.fast_neutrons:
                deim_name = "logTNeutro"
            else: 
                deim_name = "sqrtTNeutro"

            self.fueltemp_deim = NeutroDEIM(deim_name, "neutroRegion", control_dict, rom_dict, mesh)

        if (self.rhocool_feedback):    
            self.rhocool_deim = NeutroDEIM("TCool", "neutroRegion", control_dict, rom_dict, mesh)

        counter = 0
        for g in range(xs.no_energy_groups):
            name = 'fluxStar'+str(g)
            self.flux_roms.append(FluxROM(g, name, "neutroRegion", control_dict, xs, thermo_dict, rom_dict, phase_dict, bd_dict, mesh))
            self.starts.append(counter)
            counter += self.rom_dict.no_flux_bases[g]
            self.ends.append(counter)
            self.sol_field_names.append(name)
        
        for i in range(xs.no_precursor_groups):
            name = 'precStar'+str(i)
            self.prec_roms.append(PrecROM(i, name, "neutroRegion", control_dict, xs, rom_dict, phase_dict, bd_dict, mesh))
            self.starts.append(counter)
            counter += self.rom_dict.no_prec_bases[i]
            self.ends.append(counter)
            self.sol_field_names.append(name)

        self.eig_vect = np.ones(counter)

#    def reset_cross_sections(self):
#
#        self.xs = copy.copy(self.xs_orig)
#
#        for g in range(self.xs.no_energy_groups):
#            self.flux_roms[g].cross_sections = self.xs
#        
#        for i in range(self.xs.no_precursor_groups):
#            self.prec_roms[g].cross_sections = self.xs
#
#    def change_cross_sections(self, new_values):
#
#        for g in range(self.xs.no_energy_groups):
#            self.xs.D['main_fd'][g] = new_values[g]
#            self.xs.nu_sigma_f['main_fd'][g] = new_values[g+self.xs.no_energy_groups]
#
#        for g in range(self.xs.no_energy_groups):
#            self.flux_roms[g].cross_sections = self.xs
#        
#        for i in range(self.xs.no_precursor_groups):
#            self.prec_roms[g].cross_sections = self.xs

    def load_rom_bases(self):

        xs = self.xs
        rom_dict = self.rom_dict

        for g in range(xs.no_energy_groups):
            self.flux_roms[g].load_basis_vectors("./ROMFiles/bases/", rom_dict.no_flux_bases[g])
            self.flux_roms[g].initialize_solution()

        for i in range(xs.no_precursor_groups):
            self.prec_roms[i].load_basis_vectors("./ROMFiles/bases/", rom_dict.no_prec_bases[i])

        if (self.fueltemp_deim != None):
            self.fueltemp_deim.load_reduced_basis_vectors("./ROMFiles/redOperators/NeutroDEIM", rom_dict.no_temp_bases)

        if (self.rhocool_deim != None):
            self.rhocool_deim.load_reduced_basis_vectors("./ROMFiles/redOperators/NeutroDEIM", rom_dict.no_temp_bases)

    def load_rom_operators(self):

        xs = self.xs
        binary = self.control_dict.binary

        for g in range(xs.no_energy_groups):
            self.flux_roms[g].load_self_operators("./ROMFiles/redOperators/NeutROM", binary)
            self.flux_roms[g].load_fueltemp_operators("./ROMFiles/redOperators/NeutroDEIM", binary)
            self.flux_roms[g].load_rhocool_operators("./ROMFiles/redOperators/NeutroDEIM", binary)

        for i in range(xs.no_precursor_groups):
            self.prec_roms[i].load_self_operators("./ROMFiles/redOperators/PrecROM", binary)
            self.prec_roms[i].load_fueltemp_operators("./ROMFiles/redOperators/PrecDEIM", binary)
            self.prec_roms[i].load_rhocool_operators("./ROMFiles/redOperators/PrecDEIM", binary)
            self.prec_roms[i].load_fluid_operators("./ROMFiles/redOperators/PrecROM", binary)

        if (self.fueltemp_deim != None):
            self.fueltemp_deim.read_interpol_matrix("./ROMFiles/redOperators/NeutroDEIM")

        if (self.rhocool_deim != None):
            self.rhocool_deim.read_interpol_matrix("./ROMFiles/redOperators/NeutroDEIM")

    def compute_sytem_matrices(self, temp_solution=[], velocity_solution=[], alphat_solution=[]):

        if (self.fueltemp_deim != None):
            self.fueltemp_deim.compute_coefficients(temp_solution)
            for g in range(self.xs.no_energy_groups):
                self.flux_roms[g].sum_fueltemp_terms(self.fueltemp_deim.solution)
            for i in range(self.xs.no_precursor_groups):
                self.prec_roms[i].sum_fueltemp_terms(self.fueltemp_deim.solution)
 
        if (self.rhocool_deim != None):
            self.rhocool_deim.compute_coefficients(temp_solution)
            for g in range(self.xs.no_energy_groups):
                self.flux_roms[g].sum_rhocool_terms(self.rhocool_deim.solution)
            for i in range(self.xs.no_precursor_groups):
                self.prec_roms[i].sum_rhocool_terms(self.rhocool_deim.solution)

        for i in range(self.xs.no_precursor_groups):
            self.prec_roms[i].sum_fluid_operators(velocity_solution, alphat_solution)

        flux_roms = self.flux_roms
        prec_roms = self.prec_roms

        beta = self.xs.beta 
        beta_tot = self.xs.beta_tot
        chi_p = self.xs.chi_p
        chi_d = self.xs.chi_d
        nusf = self.xs.nu_sigma_f
        D = self.xs.D 
        sr = self.xs.sigma_r
        ss = self.xs.scattering_matrix
        l = self.xs.lamb
        ScNo = 1.0
        gamma = self.phase_dict.gamma
        mu = self.thermo_dict.mu
        rho = self.thermo_dict.rho
        beta_th = self.thermo_dict.beta_th
        T_ref_th = self.thermo_dict.T_ref

        alpha_nusf_fueltemp = self.xs.alpha_nu_sigma_f_fueltemp
        alpha_D_fueltemp = self.xs.alpha_D_fueltemp 
        alpha_sr_fueltemp = self.xs.alpha_sigma_r_fueltemp
        alpha_ss_fueltemp = self.xs.alpha_scattering_matrix_fueltemp

        alpha_nusf_rhocool = self.xs.alpha_nu_sigma_f_rhocool
        alpha_D_rhocool = self.xs.alpha_D_rhocool 
        alpha_sr_rhocool = self.xs.alpha_sigma_r_rhocool
        alpha_ss_rhocool = self.xs.alpha_scattering_matrix_rhocool

        sys_size = self.ends[-1]
        A_fiss = np.zeros((sys_size,sys_size))
        A = np.zeros((sys_size,sys_size))

        for fluxi in range(len(flux_roms)):

            mass_mxs = self.flux_roms[fluxi].mass_matrices
            lapl_mxs = self.flux_roms[fluxi].stiffness_matrices
            cross_mass_mxs = self.flux_roms[fluxi].cross_mass_matrices
            cross_prec_mxs = self.flux_roms[fluxi].cross_prec_matrices
            bd_mxs = self.flux_roms[fluxi].boundary_matrices

            mass_mxs_fueltemp = self.flux_roms[fluxi].mass_matrices_fueltemp
            lapl_mxs_fueltemp = self.flux_roms[fluxi].stiffness_matrices_fueltemp
            cross_mass_mxs_fueltemp = self.flux_roms[fluxi].cross_mass_matrices_fueltemp

            mass_mxs_rhocool = self.flux_roms[fluxi].mass_matrices_rhocool
            lapl_mxs_rhocool = self.flux_roms[fluxi].stiffness_matrices_rhocool
            cross_mass_mxs_rhocool = self.flux_roms[fluxi].cross_mass_matrices_rhocool

            starti = self.starts[fluxi]
            endi = self.ends[fluxi]

            for zonei in range(len(self.xs.zone_names)):

                zone = self.xs.zone_names[zonei]
            
                A[starti:endi,starti:endi] = A[starti:endi,starti:endi] \
                                             - D[zone][fluxi] * lapl_mxs[zonei] \
                                             + sr[zone][fluxi] * mass_mxs[zonei]
                A_fiss[starti:endi,starti:endi] = A_fiss[starti:endi,starti:endi] \
                                                  + nusf[zone][fluxi]*chi_p[zone][fluxi]*(1.0- beta_tot[zone])*mass_mxs[zonei]

                if (self.fueltemp_deim != None):
                    T_aux_ref =  self.xs.T_ref 
                    if self.xs.fast_neutrons:
                        T_aux_ref = np.log(self.xs.T_ref)
                    else:
                        T_aux_ref = np.sqrt(self.xs.T_ref)
                
                    A[starti:endi,starti:endi] = A[starti:endi,starti:endi] \
                                             - alpha_D_fueltemp[zone][fluxi] * lapl_mxs_fueltemp[zonei] \
                                             + alpha_D_fueltemp[zone][fluxi] * T_aux_ref * lapl_mxs[zonei] \
                                             + alpha_sr_fueltemp[zone][fluxi] * mass_mxs_fueltemp[zonei] \
                                             - alpha_sr_fueltemp[zone][fluxi] * T_aux_ref * mass_mxs[zonei]
                    A_fiss[starti:endi,starti:endi] = A_fiss[starti:endi,starti:endi] \
                                                      + alpha_nusf_fueltemp[zone][fluxi]*chi_p[zone][fluxi]*(1.0-beta_tot[zone])*mass_mxs_fueltemp[zonei] \
                                                      - alpha_nusf_fueltemp[zone][fluxi]*chi_p[zone][fluxi]*(1.0-beta_tot[zone])* T_aux_ref * mass_mxs[zonei]

                if (self.rhocool_deim != None):
                
                    T_aux_ref = T_ref_th 
                
                    A[starti:endi,starti:endi] = A[starti:endi,starti:endi] \
                                             + alpha_D_rhocool[zone][fluxi] * beta_th * rho * (lapl_mxs_rhocool[zonei] - T_aux_ref * lapl_mxs[zonei]) \
                                             - alpha_sr_rhocool[zone][fluxi] * beta_th * rho * (mass_mxs_rhocool[zonei] - T_aux_ref *  mass_mxs[zonei])
                    A_fiss[starti:endi,starti:endi] = A_fiss[starti:endi,starti:endi] \
                                                      - alpha_nusf_rhocool[zone][fluxi]*chi_p[zone][fluxi]*(1.0-beta_tot[zone])* beta_th \
                                                      * rho * (mass_mxs_rhocool[zonei] - T_aux_ref * mass_mxs[zonei])

            for bdi in range(self.bd_dict.no_bds):
                A[starti:endi,starti:endi] = A[starti:endi,starti:endi] - bd_mxs[bdi]

            # print("flux mx ", A[starti:endi,starti:endi])

            counter = 0
            for fluxj in range(len(flux_roms)):

                if fluxi == fluxj:
                    continue

                startj = self.starts[fluxj]
                endj = self.ends[fluxj]

                for zonei in range(len(self.xs.zone_names)):

                    zone = self.xs.zone_names[zonei]
                
                    A[starti:endi,startj:endj] = A[starti:endi,startj:endj] \
                                                 - ss[zone][fluxj][fluxi] * cross_mass_mxs[zonei][counter]
                    A_fiss[starti:endi,startj:endj] = A_fiss[starti:endi,startj:endj] \
                                        + nusf[zone][fluxj]*chi_p[zone][fluxi]*(1.0- beta_tot[zone])*cross_mass_mxs[zonei][counter]

                    if (self.fueltemp_deim != None):
                    
                        T_aux_ref =  self.xs.T_ref 
                        if self.xs.fast_neutrons:
                            T_aux_ref = np.log(self.xs.T_ref)
                        else:
                            T_aux_ref = np.sqrt(self.xs.T_ref)
                    
                        A[starti:endi,startj:endj] = A[starti:endi,startj:endj] \
                                                 - alpha_ss_fueltemp[zone][fluxj][fluxi] * cross_mass_mxs_fueltemp[zonei][counter] \
                                                 + alpha_ss_fueltemp[zone][fluxj][fluxi] * T_aux_ref * cross_mass_mxs[zonei][counter]
                        A_fiss[starti:endi,startj:endj] = A_fiss[starti:endi,startj:endj] \
                                        + alpha_nusf_fueltemp[zone][fluxj]*chi_p[zone][fluxi]*(1.0- beta_tot[zone])* cross_mass_mxs_fueltemp[zonei][counter] \
                                        - alpha_nusf_fueltemp[zone][fluxj]*chi_p[zone][fluxi]*(1.0- beta_tot[zone])* T_aux_ref * cross_mass_mxs[zonei][counter]

                    if (self.rhocool_deim != None):
                    
                        T_aux_ref =  T_ref_th 
                    
                        A[starti:endi,startj:endj] = A[starti:endi,startj:endj] \
                                                 + alpha_ss_rhocool[zone][fluxj][fluxi] * beta_th * rho * \
                                                  (cross_mass_mxs_rhocool[zonei][counter] - T_aux_ref *  cross_mass_mxs[zonei][counter])
                        A_fiss[starti:endi,startj:endj] = A_fiss[starti:endi,startj:endj] \
                                                 - alpha_nusf_rhocool[zone][fluxj]*chi_p[zone][fluxi]*(1.0- beta_tot[zone])* beta_th * rho * \
                                                  (cross_mass_mxs_rhocool[zonei][counter] - T_aux_ref *  cross_mass_mxs[zonei][counter])

                counter += 1

            for preci in range(len(prec_roms)):

                startj = self.starts[preci+len(flux_roms)]
                endj = self.ends[preci+len(flux_roms)]

                for zonei in range(len(self.xs.zone_names)):
                    zone = self.xs.zone_names[zonei]
                
                    A[starti:endi,startj:endj] = A[starti:endi,startj:endj] \
                                                 - chi_d[zone][fluxi] * gamma[zone] * l[zone][preci] * cross_prec_mxs[zonei][preci]


        for preci in range(len(prec_roms)):

            mass_mxs = self.prec_roms[preci].mass_matrices
            lam_diff_mxs = self.prec_roms[preci].lam_diffusion_matrices
            turb_diff_mx = self.prec_roms[preci].turb_diffusion_matrix
            adv_mx = self.prec_roms[preci].advection_matrix
            adv_mx_bd = self.prec_roms[preci].advection_bc_matrix
            flux_mxs = self.prec_roms[preci].cross_flux_matrices
            flux_mxs_rhocool = self.prec_roms[preci].cross_flux_matrices_rhocool
            flux_mxs_fueltemp = self.prec_roms[preci].cross_flux_matrices_fueltemp
            starti = self.starts[preci + len(flux_roms)]
            endi = self.ends[preci + len(flux_roms)] 

            for zonei in range(len(self.xs.zone_names)): 

                zone = self.xs.zone_names[zonei]
            
                A[starti:endi,starti:endi] = A[starti:endi,starti:endi] \
                                             + gamma[zone] * l[zone][preci] * mass_mxs[zonei] \
                                             - mu/rho/ScNo *  lam_diff_mxs[zonei] 

            A[starti:endi,starti:endi] = A[starti:endi,starti:endi] + adv_mx + adv_mx_bd -turb_diff_mx/rho 

            # print("prec mx ", A[starti:endi,starti:endi])

            for fluxj in range(len(flux_roms)): 

                startj = self.starts[fluxj]
                endj = self.ends[fluxj] 

                for zonei in range(len(self.xs.zone_names)): 
                    zone = self.xs.zone_names[zonei]
                
                    A_fiss[starti:endi,startj:endj] = A_fiss[starti:endi,startj:endj] \
                                        + nusf[zone][fluxj]*beta[zone][preci]*flux_mxs[zonei][fluxj]

                    if (self.fueltemp_deim != None):
                    
                        T_aux_ref =  self.xs.T_ref 
                        if self.xs.fast_neutrons:
                            T_aux_ref = np.log(self.xs.T_ref)
                        else:
                            T_aux_ref = np.sqrt(self.xs.T_ref)
                    
                        A_fiss[starti:endi,startj:endj] = A_fiss[starti:endi,startj:endj] \
                                        + alpha_nusf_fueltemp[zone][fluxj]*beta[zone][preci]* flux_mxs_fueltemp[zonei][fluxj] \
                                        - alpha_nusf_fueltemp[zone][fluxj]*beta[zone][preci]* T_aux_ref * flux_mxs[zonei][fluxj]

                    if (self.rhocool_deim != None):
                    
                        T_aux_ref =  T_ref_th 
                    
                        A_fiss[starti:endi,startj:endj] = A_fiss[starti:endi,startj:endj] \
                                                 - alpha_nusf_rhocool[zone][fluxj]*beta[zone][preci]* beta_th * rho * \
                                                  (flux_mxs_rhocool[zonei][fluxj] - T_aux_ref *  flux_mxs[zonei][fluxj])

        self.A = A
        self.A_fiss = A_fiss

        starti = self.starts[len(flux_roms)]
        endi = self.ends[len(flux_roms)]        

    def sum_advection_tensors(self, solution):

        for prec_rom in self.prec_roms:
            prec_rom.sum_coupling_operators(solution)

    def distribute_solution(self):

        xs = self.xs

        for g in range(xs.no_energy_groups):
            self.flux_roms[g].solution = self.eig_vect[self.starts[g]:self.ends[g]]
        
        nfrom = len(self.flux_roms)
        for i in range(xs.no_precursor_groups):
            self.prec_roms[i].solution = self.eig_vect[self.starts[i+nfrom]:self.ends[i+nfrom]]

    def solve(self):

        vals, vecs = scipy.linalg.eig(self.A_fiss, b=self.A)

        self.keff = max(vals).real

        self.eig_vect = vecs.T[0].real

        power = 0.0
        for g in range(self.xs.no_energy_groups):
            power += self.flux_roms[g].compute_power(self.eig_vect[self.starts[g]:self.ends[g]], 
                                                     self.fueltemp_feedback, self.rhocool_feedback)

        self.eig_vect = self.eig_vect*self.reactor_state.p_target/power

        self.distribute_solution()


        









