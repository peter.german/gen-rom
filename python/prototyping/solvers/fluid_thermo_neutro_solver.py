import numpy as np 
import os

from utilities import read_list, read_matrix

from .diffusion_k_solver import DiffusionKSolver
from .thermo_solver import ThermoSolver
from .turb_ss_fluid_solver_2eq import TurbSteadyFluidSolverTwoEq

class FluidThermoNeutroSolver:

    def __init__(self, control_dict, xs, reactor_state, rom_dict, phase_dict, thermo_dict, 
                 bd_dict_fluid, bd_dict_neutro, fluid_mesh, neutro_mesh):

        self.control_dict = control_dict
        self.xs = xs
        self.reactor_state = reactor_state
        self.rom_dict = rom_dict
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict_fluid = bd_dict_fluid
        self.bd_dict_neutro = bd_dict_neutro
        self.neutro_mesh = neutro_mesh
        self.fluid_mesh = fluid_mesh

        self.tolerance = 1e-8

        self.sol_field_names = []

        self.fluid_solver = TurbSteadyFluidSolverTwoEq(control_dict, rom_dict, phase_dict, thermo_dict, bd_dict_fluid, fluid_mesh)
        self.neutro_solver = DiffusionKSolver(control_dict, xs, reactor_state, rom_dict, phase_dict, thermo_dict, bd_dict_neutro, neutro_mesh, True, True)
        self.thermo_solver = ThermoSolver(control_dict, rom_dict, xs, phase_dict, thermo_dict, bd_dict_fluid, fluid_mesh, neutro_mesh, True, True)

    def get_reconstructed_solution(self, name):
        
        for flux_rom in self.neutro_solver.flux_roms:
            if flux_rom.name == name:
                solution = flux_rom.get_reconstructed_solution()
                return(solution)
        for prec_rom in self.neutro_solver.prec_roms:
            if prec_rom.name == name:
                solution = prec_rom.get_reconstructed_solution()
                return(solution)
        if self.fluid_solver.velocity_rom.name == name:
            solution = self.fluid_solver.velocity_rom.get_reconstructed_solution()
            return(solution)
        if self.fluid_solver.pressure_rom.name == name:
            solution = self.fluid_solver.pressure_rom.get_reconstructed_solution()
            return(solution)
        if self.fluid_solver.nut_rom.name == name:
            solution = self.fluid_solver.nut_rom.get_reconstructed_solution()
            return(solution)
        if self.fluid_solver.alphat_rom.name == name:
            solution = self.fluid_solver.alphat_rom.get_reconstructed_solution()
            return(solution)
        if self.thermo_solver.energy_rom.name == name:
            solution = self.thermo_solver.energy_rom.get_reconstructed_solution()
            return(solution)
        
        return None

    def update_parameters(self, sdict):

        for key in sdict:
            if key.startswith("D_"):
                self.xs.set_parameter('D','main_fd',int(key.replace("D_",''))-1,sdict[key])
            if key.startswith("nuSigma_f_"):
                self.xs.set_parameter('nuSigmaEff','main_fd',int(key.replace("nuSigma_f_",''))-1,sdict[key])
            if key.startswith("Sigma_r_"):
                self.xs.set_parameter('sigmaDisapp','main_fd',int(key.replace("Sigma_r_",''))-1,sdict[key])
            if key.startswith("Fp"):
                self.phase_dict.set_parameter('momentumSource', 'pump', 2, sdict[key])
            if key.startswith("T_HX"):
                self.phase_dict.set_parameter('T', 'hx', 0, sdict[key])
            if key.startswith("alpha_HX"):
                self.phase_dict.set_parameter('value', 'hx', 0, sdict[key])
            if key.startswith("beta_th"):
                self.thermo_dict.set_parameter('beta', '', 0, sdict[key])
            if key.startswith("Pr"):
                self.thermo_dict.set_parameter('Pr', '', 0, sdict[key])

    def create_rom_objects(self):

        self.fluid_solver.create_rom_objects()
        self.sol_field_names = self.sol_field_names + self.fluid_solver.sol_field_names
        self.thermo_solver.create_rom_objects()
        self.sol_field_names = self.sol_field_names + self.thermo_solver.sol_field_names
        self.neutro_solver.create_rom_objects()
        self.sol_field_names = self.sol_field_names + self.neutro_solver.sol_field_names

    def load_rom_bases(self):

        self.fluid_solver.load_rom_bases()
        self.thermo_solver.load_rom_bases()
        self.neutro_solver.load_rom_bases()

    def load_rom_operators(self):

        self.fluid_solver.load_rom_operators()
        self.thermo_solver.load_rom_operators()
        self.neutro_solver.load_rom_operators()

    def solve(self):

        res = 1e16
        while res > self.tolerance:

            initial_solution = np.concatenate((self.fluid_solver.velocity_rom.solution,
                                               self.thermo_solver.energy_rom.solution.flatten(),
                                               self.neutro_solver.eig_vect), axis=0)

            self.fluid_solver.solve([self.phase_dict.get_parameter('momentumSource','pump',2)], 
                                    temp_solution=self.thermo_solver.energy_rom.solution)

            flux_sols = []
            for rom in self.neutro_solver.flux_roms:
                flux_sols.append(rom.solution)

            self.thermo_solver.solve(velocity_sol=self.fluid_solver.velocity_rom.solution,
                                     alphat_sol=self.fluid_solver.alphat_rom.solution,
                                     flux_sols=flux_sols)

            self.neutro_solver.compute_sytem_matrices(temp_solution=self.thermo_solver.energy_rom.solution,
                                                      velocity_solution=self.fluid_solver.velocity_rom.solution, 
                                                      alphat_solution=self.fluid_solver.alphat_rom.solution)

            self.neutro_solver.solve()

            new_solution = np.concatenate((self.fluid_solver.velocity_rom.solution,
                                           self.thermo_solver.energy_rom.solution.flatten(),
                                           self.neutro_solver.eig_vect), axis=0)

            diff_sol = np.abs(initial_solution - new_solution) / np.abs(new_solution)
            res = np.max(diff_sol)
            
        print("keff = ", self.neutro_solver.keff)








        