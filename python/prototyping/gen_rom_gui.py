from parameterparser import CrossSections
from parameterparser import ROMDictionary
from parameterparser import ThermoDictionary
from parameterparser import PhaseDictionary
from parameterparser import BoundaryDictionary
from parameterparser import ControlDictionary
from parameterparser import ReactorStateDictionary
from solvers import FluidThermoNeutroSolver
from openfoamparser import mesh_parser
from gui import plot_solution

import openfoamparser
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk

import PySimpleGUI as sg
import math


sg.theme('Dark Grey 11')

menu_def = [['File', ['Load ROM', 'Exit'  ]],     
                ['Help', 'About...'], ]

unc_parameters = ["D_"+str(i+1) for i in range(6)] + \
                 ["nuSigma_f_"+str(i+1) for i in range(6)] + \
                 ["Sigma_r_"+str(i+1) for i in range(6)] +\
                 ["Fp"] +\
                 ["T_HX", "alpha_HX"] +\
                 ["beta_th", "Pr"]
limits = [[0.0213481, 0.02609211],
          [0.01421,0.01736779],
          [0.00902115,0.01102585],
          [0.0108757,0.0132926],
          [0.01038024,0.01268696],
          [0.00993447,0.01214213]] + \
         [[0.51799320, 0.6331028],
          [0.3379086,0.4129994],
          [0.36975420,0.4519218],
          [0.5606712,0.6852648],
          [1.320093,1.613447],
          [4.282902,5.234658]] + \
         [[6.20973E+00, 7.589670E+00],
          [3.55995E+00, 4.351050E+00],
          [1.59669E+00, 1.951510E+00],
          [1.78506E+00, 2.181740E+00],
          [1.47393E+00, 1.801470E+00],
          [3.20967E+00, 3.922930E+00]] + \
         [[-100000.0,-60000.0]] + \
         [[850.0, 950.0],
          [0.8e5,1.2e5]] + \
         [[1.8e-4,2.2e-4],
          [7.2, 8.8]]

texts = [[sg.Text(param)] for param in unc_parameters]
max_length_col1 = len(max(unc_parameters[0:math.floor(len(limits)/2)+1], key=len))
sliders_col1 = [
            [
                sg.Text(unc_parameters[i], size=(max_length_col1, 1), justification='right'),
                sg.Slider(range=(limits[i][0],limits[i][1]), 
                          default_value=(limits[i][0]+limits[i][1])/2, 
                          size=(20,15), 
                          resolution=.000001, 
                          orientation='h', 
                          font=('Helvetica', 10),
                          key="slider-"+str(i))
            ] for i in range(math.floor(len(limits)/2)+1)
          ]

max_length_col2 = len(max(unc_parameters[math.floor(len(limits)/2)+1:], key=len))
sliders_col2 = [
            [
                sg.Text(unc_parameters[i], size=(max_length_col2, 1), justification='right'),
                sg.Slider(range=(limits[i][0],limits[i][1]), 
                          default_value=(limits[i][0]+limits[i][1])/2, 
                          size=(20,15), 
                          resolution=.000001, 
                          orientation='h', 
                          font=('Helvetica', 10),
                          key="slider-"+str(i))
            ] for i in range(math.floor(len(limits)/2)+1,len(limits))
          ]

 
control_column = [
    [sg.Output(size=(50,20), background_color='black', text_color='white')],
    [sg.Button("Load")],
    [sg.Button("Solve", disabled=True)],
    [sg.Button("Reset", disabled=True)],
]

layout = [
            [sg.Menu(menu_def, )], 
            [
                sg.Column(control_column),
                sg.VSeparator(), 
                sg.Column(sliders_col1),
                sg.VSeparator(),
                sg.Column(sliders_col2),
                sg.VSeparator(), 
                sg.Column(
                    layout=[
                        [sg.Text('Effective multiplication factor (keff): '), sg.Text(size=(15,1), key='keff-output')],
                        [sg.Text('Field to plot: '), sg.Drop(values=(), key='sol-list'), sg.Button("Plot", disabled=True)],
                        [sg.Canvas(key='controls_cv')],
                        [sg.Canvas(key='fig-cv', size=(400, 400))]
                            ],
                    pad=(0, 0)
                )
            ]
]

# Create the window
window = sg.Window("GeN-ROM - Model-Order Reduction tool for OpenFOAM", layout)

control_dict = None
xs = None
rom_dict = None
thermo_dict = None
phase_dict = None
bd_dict = None
mesh = None

nts = None
fig_canvas_agg = None
toolbar = None


# Create an event loop
while True:
    event, values = window.read()
    
    if event == 'Load ROM' or event =='Load':

        print("***********************************************")
        print("Loading GeN-Foam control dictionaries.")
        print("***********************************************")
        control_dict = ControlDictionary("./system/controlDict")
        control_dict.read_dictionary()
        
        neutro_mesh = mesh_parser.FoamMesh("constant/neutroRegion/polyMesh/")
        neutro_mesh.read_cell_centres(control_dict.start_time+'/neutroRegion/C')
        neutro_mesh.read_cell_volumes(control_dict.start_time+'/neutroRegion/V')
        
        fluid_mesh = mesh_parser.FoamMesh("constant/fluidRegion/polyMesh/")
        fluid_mesh.read_cell_centres(control_dict.start_time+'/fluidRegion/C')
        fluid_mesh.read_cell_volumes(control_dict.start_time+'/fluidRegion/V')
        
        xs = CrossSections("./constant/neutroRegion/nuclearData",
                           "./constant/neutroRegion/nuclearDataFuelTemp",
                           "./constant/neutroRegion/nuclearDataRhoCool")
        xs.read_cross_sections(verbose=True)
        xs.read_cross_sections_fueltemp(verbose=True)
        xs.read_cross_sections_rhocool(verbose=True)
        
        xs.create_fueltemp_feedback_coefficients(verbose=True)
        xs.create_rhocool_feedback_coefficients(verbose=True)
        
        reactor_state = ReactorStateDictionary("./constant/neutroRegion/reactorState")
        reactor_state.read_dictionary()
        
        rom_dict = ROMDictionary("./system/ROMDict")
        rom_dict.read_dictionary()
        
        thermo_dict = ThermoDictionary("./constant/fluidRegion/thermophysicalProperties")
        thermo_dict.read_dictionary()
        
        phase_dict = PhaseDictionary("./constant/fluidRegion/phaseProperties")
        phase_dict.read_dictionary()
        
        bd_dict_fluid = BoundaryDictionary("./constant/fluidRegion/polyMesh/boundary")
        bd_dict_fluid.read_dictionary()
        
        bd_dict_neutro = BoundaryDictionary("./constant/neutroRegion/polyMesh/boundary")
        bd_dict_neutro.read_dictionary()

        print("***********************************************")
        print("Creating reduced-order solver.")
        print("***********************************************")

        nts = FluidThermoNeutroSolver(control_dict, xs, reactor_state, rom_dict, phase_dict, thermo_dict, 
                              bd_dict_fluid, bd_dict_neutro, fluid_mesh, neutro_mesh)
        nts.create_rom_objects()

        print("***********************************************")
        print("Reading basis functions.")
        print("***********************************************")

        nts.load_rom_bases()

        print("***********************************************")
        print("Reading precomputed operators.")
        print("***********************************************")

        nts.load_rom_operators()

        print("***********************************************")
        print('ROM Loaded!')
        print("***********************************************")

        window.FindElement('Solve').Update(disabled=False)
        window.FindElement('Reset').Update(disabled=False)
        window.FindElement('Load').Update(disabled=False)
        window.FindElement('Plot').Update(disabled=False)
        

    if event == 'Solve':

        sdict = dict(zip(unc_parameters,
                         [values["slider-"+str(i)] for i in range(len(limits))]))

        nts.update_parameters(sdict)
        
        nts.solve()
        window['keff-output'].update('{:.6f}'.format(nts.neutro_solver.keff))
        window['sol-list'].update(values=nts.sol_field_names)
        window['sol-list'].expand(expand_x=True)

    if event == "Reset":
        for i in range(len(limits)):
            window.Element("slider-"+str(i)).Update((limits[i][0]+limits[i][1])/2)

    if event == "Plot":
        if values["sol-list"] == "":
            continue
        if (fig_canvas_agg != None):
            fig_canvas_agg.get_tk_widget().forget()
            toolbar.forget()
        solution = nts.get_reconstructed_solution(values["sol-list"])
        fig = plot_solution(solution, fluid_mesh, values["sol-list"])
        window['fig-cv'].TKCanvas.delete("all")
        fig_canvas_agg = FigureCanvasTkAgg(fig, window['fig-cv'].TKCanvas)
        fig_canvas_agg.draw()
        toolbar = NavigationToolbar2Tk(fig_canvas_agg, window['controls_cv'].TKCanvas)
        toolbar.update()
    
        fig_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)


    if event == "Exit" or event == sg.WIN_CLOSED:
        break

window.close()