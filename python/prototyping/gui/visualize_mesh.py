import openfoamparser
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.patches import PathPatch
from matplotlib.path import Path
import matplotlib as mpl
from scipy.interpolate import griddata
import numpy as np

mesh = openfoamparser.FoamMesh('.')
mesh.read_cell_centres('0/fluidRegion/C')
cc_BC = openfoamparser.parse_boundary_field('0/fluidRegion/C')
flux = openfoamparser.parse_internal_field('20/neutroRegion/precStar3')

reflector = np.asarray(cc_BC[b'reflector'][b'value'])
hx = np.asarray(cc_BC[b'hx'][b'value'])

filtered_hx = []
for vector in hx:
	if (vector[1] < 2.0 and vector[2] > -1.18 and vector[2] < 1.18):
		filtered_hx.append(vector)
filtered_hx= np.asarray(filtered_hx)

organized = []
for i in np.sort(reflector[:,2], axis=0):
   index = np.where(i == reflector[:,2])
   organized.append(reflector[index[0][0],:])

unused_indices = [i for i in range(len(filtered_hx))]
for i in range(len(filtered_hx)):

	index = 0
	distance = 100

	for j in unused_indices:
		distance_j = np.linalg.norm(filtered_hx[j]-organized[-1])
		if distance_j < distance:
			index = j
			distance = distance_j

	unused_indices.remove(index)
	organized.append(filtered_hx[index])

organized = np.asarray(organized)

verts = [(organized[i,1],organized[i,2]) for i in range(len(organized))]
codes = [Path.MOVETO]+[Path.LINETO for i in range(len(organized)-2)]+[Path.CLOSEPOLY]
path = Path(verts, codes)
patch = PathPatch(path, facecolor='w')



ngridy = 300
ngridz = 300

# Interpolation grid dimensions
yinterpmin = -0.0
yinterpmax = 2.5
zinterpmin = -1.5
zinterpmax = 1.5

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)
# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)

# Interpolation of scalra fields and vector field components
flux_i = griddata((mesh.cell_centres[:,1], mesh.cell_centres[:,2]), flux, (yinterp, zinterp), method='linear')

fig, ax = plt.subplots()
fig.set_figheight(10)
fig.set_figwidth(10)

cmap = mpl.cm.viridis
norm = mpl.colors.Normalize(vmin=np.min(flux_i), vmax=np.max(flux_i))

fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap),
             ax=ax, orientation='vertical', label='Some Units')

ax.contourf(yi, zi, flux_i, levels=10)

ax.add_patch(patch)

fig.savefig("example.pdf")



