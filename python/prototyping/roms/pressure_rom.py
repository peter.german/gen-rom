import numpy as np
import openfoamparser as ofp
import os
from .rom_base import ROMBase
from utilities import read_list, read_matrix

class PressureROM(ROMBase):

    def __init__(self, name, region_name, control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh):

        ROMBase.__init__(self, name, region_name, control_dict, rom_dict, mesh)

        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict

        self.div_mx = []

    def load_self_operators(self, path, binary):

        print("Loading self matrices for "+self.name+".")
        
        fname = os.path.join(path, "redDivMatrix")
        self.div_mx = read_matrix(fname, binary)