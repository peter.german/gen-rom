import numpy as np
import openfoamparser as ofp
import os
from .rom_base import ROMBase
from utilities import read_list, read_matrix

class AlphatROM(ROMBase):

    def __init__(self, name, region_name, control_dict, rom_dict, phase_dict, mesh):

        ROMBase.__init__(self, name, region_name, control_dict, rom_dict, mesh)

        self.phase_dict = phase_dict

        self.interpol_weights = []
        self.min_params = []
        self.max_params = []
        self.norm_table = []
        self.num_divisions =[]

    def load_self_operators(self, path, binary):

        print("Loading self operators for "+self.name+".")

        self.interpol_weights = []

        fname = os.path.join(path, "minParameters")
        self.min_params = read_list(fname, binary)

        fname = os.path.join(path, "maxParameters")
        self.max_params = read_list(fname, binary)

        fname = os.path.join(path, "numberOfDivisions")
        self.num_divisions = read_list(fname, binary, data_type='i')

        fname = os.path.join(path, "normParamTable")
        self.norm_table = read_matrix(fname, binary)

        for i in range(self.rom_dict.no_nut_bases):

            fname = os.path.join(path, "InterpolWeights_"+str(i))
            self.interpol_weights.append(read_list(fname, binary))

    def solve(self, new_params):

        distances = []
        
        norm_new_params = []

        for i in range(len(new_params)):
            if abs(self.max_params[i]-self.min_params[i]) > 1e-16:
                norm_new_params.append(
                    (new_params[i]-self.min_params[i])*(self.num_divisions[i]-1)
                    /(self.max_params[i]-self.min_params[i]))
            else:
                norm_new_params.append(0.0)

        norm_new_params = np.asarray(norm_new_params)

        for i in range(len(self.norm_table)):
            distances.append(np.linalg.norm(self.norm_table[i]-norm_new_params))

        self.solution *= 0.0

        for i in range(len(self.solution)):

            for j in range(len(self.norm_table)):

                self.solution[i] += self.interpol_weights[i][j] * np.exp(-(0.4*distances[j])**2) 

