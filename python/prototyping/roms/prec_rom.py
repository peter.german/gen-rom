import numpy as np
import openfoamparser as ofp
import os
from .rom_base import ROMBase
from utilities import read_list, read_matrix

class PrecROM(ROMBase):

    def __init__(self, group_number, name, region_name, control_dict, 
                 xs, rom_dict, phase_dict, bd_dict, mesh):

        ROMBase.__init__(self, name, region_name, control_dict, rom_dict, mesh)

        self.group_number = group_number

        self.xs = xs
        self.phase_dict = phase_dict
        self.bd_dict = bd_dict

        self.mass_matrices = []
        self.cross_flux_matrices = []
        self.lam_diffusion_matrices = []

        self.turb_diffusion_matrices = []
        self.advection_matrices = []
        self.advection_bc_terms = []
        self.advection_bc_matrix = []  

        self.advection_matrix = None
        self.turb_diffusion_matrix = None

        self.cross_flux_tensors_fueltemp = []
        self.cross_flux_tensors_rhocool = []

        self.cross_flux_matrices_fueltemp = []
        self.cross_flux_matrices_rhocool = []

    def load_self_operators(self, path, binary):

        print("Loading self matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.cross_flux_matrices = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            fname = os.path.join(path, "precMassTensor_"+str(zonei)+"_"+self.name)
            self.mass_matrices.append(read_matrix(fname, binary))

            fname = os.path.join(path, "lamDiffTensor_"+str(zonei)+"_"+self.name)
            self.lam_diffusion_matrices.append(read_matrix(fname, binary))

            for fromi in range(no_energy_groups):

                fname = os.path.join(path, "crossFluxTensor_"+str(zonei)+"_"+str(fromi)+"_"+self.name)
                self.cross_flux_matrices[zonei].append(read_matrix(fname, binary))

    def load_fueltemp_operators(self, path, binary):

        print("Loading fuel temperature matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.cross_flux_tensors_fueltemp = [[] for i in range(no_zones)]

        name = ""
        if self.xs.fast_neutrons == True:
            name = "logTNeutro"
        else:
            name = "sqrtTNeutro"

        for zonei in range(no_zones):

            self.cross_flux_tensors_fueltemp[zonei] = [[] for i in range(no_energy_groups)]

            for bdi in range(self.rom_dict.no_fueltemp_bases):
    
                for fromi in range(no_energy_groups):
    
                    fname = os.path.join(path, "crossMassTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(fromi)+"_"+str(bdi)+"_"+name)
                    self.cross_flux_tensors_fueltemp[zonei][fromi].append(read_matrix(fname, binary))


    def sum_fueltemp_terms(self, deim_solution=[]):

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.cross_flux_matrices_fueltemp = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            cross_flux_matrices = [0.0*matrix for matrix in self.cross_flux_matrices[zonei]]

            for bdi in range(self.rom_dict.no_fueltemp_bases):
    
                for counter in range(len(cross_flux_matrices)):
    
                    cross_flux_matrices[counter] += deim_solution[bdi] * self.cross_flux_tensors_fueltemp[zonei][counter][bdi]

            self.cross_flux_matrices_fueltemp[zonei] = cross_flux_matrices


    def load_rhocool_operators(self, path, binary):

        print("Loading coolant density matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.cross_flux_tensors_rhocool = [[] for i in range(no_zones)]

        name = "TCool"

        for zonei in range(no_zones):

            self.cross_flux_tensors_rhocool[zonei] = [[] for i in range(no_energy_groups)]

            for bdi in range(self.rom_dict.no_rhocool_bases):
    
                for fromi in range(no_energy_groups):
    
                    fname = os.path.join(path, "crossMassTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(fromi)+"_"+str(bdi)+"_"+name)
                    self.cross_flux_tensors_rhocool[zonei][fromi].append(read_matrix(fname, binary))


    def sum_rhocool_terms(self, deim_solution=[]):

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.cross_flux_matrices_rhocool = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            cross_flux_matrices = [0.0*matrix for matrix in self.cross_flux_matrices[zonei]]

            for bdi in range(self.rom_dict.no_rhocool_bases):
    
                for counter in range(len(cross_flux_matrices)):
    
                    cross_flux_matrices[counter] += deim_solution[bdi] * self.cross_flux_tensors_rhocool[zonei][counter][bdi]

            self.cross_flux_matrices_rhocool[zonei] = cross_flux_matrices

    def load_fluid_operators(self, path, binary):

        print("Loading fluid flow terms for "+self.name+".")

        no_bds = self.bd_dict.no_bds
        no_velocity_bases = self.rom_dict.no_velocity_bases + self.rom_dict.no_supremizer_bases
        no_alphat_bases = self.rom_dict.no_alphat_bases

        self.advection_matrices = []
        self.turb_diffusion_matrices = []
        self.advection_bc_terms = [[] for i in range(no_velocity_bases)]

        for veli in range(no_velocity_bases):

            fname = os.path.join(path, "advTensor_"+str(veli)+"_"+self.name)
            self.advection_matrices.append(read_matrix(fname, binary))

            for bdi in range(no_bds):

                fname = os.path.join(path, "advBDInternalTensor_"+str(veli)+"_"+str(bdi)+"_"+self.name)
                self.advection_bc_terms[veli].append(read_matrix(fname, binary))

        for ai in range(no_alphat_bases):

            fname = os.path.join(path, "turbDiffTensor_"+str(ai)+"_"+self.name)
            self.turb_diffusion_matrices.append(read_matrix(fname, binary))

    def sum_fluid_operators(self, velocity_solution=[], alphat_solution=[]):

        no_own_bases = len(self.basis_functions)
        no_velocity_bases = len(velocity_solution)
        no_bds = self.bd_dict.no_bds
        
        tmp_adv_matrix = np.zeros((no_own_bases, no_own_bases))
        tmp_turbdiff_matrix = np.zeros((no_own_bases, no_own_bases))
        tmp_bd_matrix = np.zeros((no_own_bases, no_own_bases))

        for veli in range(len(velocity_solution)):

            tmp_adv_matrix += velocity_solution[veli]*self.advection_matrices[veli]

            for bdi in range(no_bds):

                tmp_bd_matrix += velocity_solution[veli]*self.advection_bc_terms[veli][bdi]

        for ai in range(len(alphat_solution)):

            tmp_turbdiff_matrix += alphat_solution[ai]*self.turb_diffusion_matrices[ai]

        self.advection_matrix = tmp_adv_matrix
        self.advection_bc_matrix = tmp_bd_matrix
        self.turb_diffusion_matrix = tmp_turbdiff_matrix


