import numpy as np
import openfoamparser as ofp
import os
from .rom_base import ROMBase
from utilities import read_list, read_matrix

class FluxROM(ROMBase):

    def __init__(self, group_number, name, region_name, control_dict, 
                 xs, thermo_dict, rom_dict, phase_dict, bd_dict, mesh):

        ROMBase.__init__(self, name, region_name, control_dict, rom_dict, mesh)

        self.group_number = group_number

        self.xs = xs
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict

        self.mass_matrices = []
        self.cross_mass_matrices = []
        self.cross_prec_matrices = []
        self.stiffness_matrices = []
        self.boundary_matrices = []
        self.red_integrals = []

        self.mass_tensors_fueltemp = []
        self.cross_mass_tensors_fueltemp = []
        self.stiffness_tensors_fueltemp = []
        self.red_integral_tensor_fueltemp = []

        self.mass_tensors_rhocool = []
        self.cross_mass_tensors_rhocool = []
        self.stiffness_tensors_rhocool = []
        self.red_integral_tensor_rhocool = []

        self.mass_matrices_fueltemp = []
        self.cross_mass_matrices_fueltemp = []
        self.stiffness_matrices_fueltemp = []
        self.red_integrals_fueltemp = []

        self.mass_matrices_rhocool = []
        self.cross_mass_matrices_rhocool = []
        self.stiffness_matrices_rhocool = []
        self.red_integrals_rhocool = []

    def compute_power(self, solution, fueltemp_feedback=False, rhocool_feedback=False):

        no_zones = self.xs.no_zones
        beta_th = self.thermo_dict.beta_th
        rho = self.thermo_dict.rho

        alpha_sigma_pow_fueltemp = self.xs.alpha_sigma_pow_fueltemp
        alpha_sigma_pow_rhocool = self.xs.alpha_sigma_pow_rhocool

        power = 0.0
        for zonei in range(no_zones):

            zone = self.xs.zone_names[zonei]

            for soli in range(len(self.solution)):
                power += solution[soli]*self.xs.sigma_pow[zone][self.group_number]*self.red_integrals[zonei][soli]

                if fueltemp_feedback:

                    T_aux_ref =  self.xs.T_ref 
                    if self.xs.fast_neutrons:
                        T_aux_ref = np.log(self.xs.T_ref)
                    else:
                        T_aux_ref = np.sqrt(self.xs.T_ref)

                    power += solution[soli]*alpha_sigma_pow_fueltemp[zone][self.group_number]*(self.red_integrals_fueltemp[zonei][soli] - T_aux_ref*self.red_integrals[zonei][soli]) 

                if rhocool_feedback:

                    T_aux_ref = self.thermo_dict.T_ref

                    power -= solution[soli]*alpha_sigma_pow_rhocool[zone][self.group_number]*beta_th*rho*(self.red_integrals_rhocool[zonei][soli] - T_aux_ref*self.red_integrals[zonei][soli])
                

        return(power)

    def initialize_solution(self):

        print("Initializing "+self.name+": ")

        fpath = os.path.join(self.control_dict.start_time, self.region_name, self.name)            
        initial_field = ofp.parse_internal_field(fpath)
        if initial_field == None:
            print("Falling back to defaultFlux field.")
            fpath = os.path.join(self.control_dict.start_time, self.region_name, "defaultFlux")
            initial_field = ofp.parse_internal_field(fpath)
        V = self.mesh.cell_volumes

        for i in range(len(self.solution)):
            self.solution[i] = np.dot(np.multiply(initial_field, V), self.basis_functions[i]) 

    def load_self_operators(self, path, binary):

        print("Loading self matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups
        no_bds = self.bd_dict.no_bds

        self.cross_mass_matrices = [[] for i in range(no_zones)]
        self.cross_prec_matrices = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            # Reading self mass matrix
            fname = os.path.join(path, "massTensor_"+str(zonei)+"_"+self.name)
            self.mass_matrices.append(read_matrix(fname, binary))

            # Reading self stiffness matrix
            fname = os.path.join(path, "laplTensor_"+str(zonei)+"_"+self.name)
            self.stiffness_matrices.append(read_matrix(fname, binary))

             # Reading power intergrals
            fname = os.path.join(path, "redIntegrals_"+str(zonei)+"_"+self.name)
            self.red_integrals.append(read_list(fname, binary))

            for fromi in range(no_energy_groups):
                
                if fromi == self.group_number:
                    continue

                fname = os.path.join(path, "crossMassTensor_"+str(zonei)+"_"+str(fromi)+"_"+self.name)
                self.cross_mass_matrices[zonei].append(read_matrix(fname, binary))

            for fromi in range(no_precursor_groups):

                fname = os.path.join(path, "crossPrecTensor_"+str(zonei)+"_"+str(fromi)+"_"+self.name)
                self.cross_prec_matrices[zonei].append(read_matrix(fname, binary))

        for bdi in range(no_bds):

            # Reading boundary condition term
            fname = os.path.join(path, "bdTensor_"+str(bdi)+"_"+self.name)
            self.boundary_matrices.append(read_matrix(fname,binary))

    def load_fueltemp_operators(self, path, binary):

        print("Loading fuel temperature matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.mass_tensors_fueltemp = [[] for i in range(no_zones)]
        self.stiffness_tensors_fueltemp = [[] for i in range(no_zones)]
        self.red_integral_tensors_fueltemp = [[] for i in range(no_zones)]
        self.cross_mass_tensors_fueltemp = [[] for i in range(no_zones)]

        name = ""
        if self.xs.fast_neutrons == True:
            name = "logTNeutro"
        else:
            name = "sqrtTNeutro"

        for zonei in range(no_zones):

            self.cross_mass_tensors_fueltemp[zonei] = [[] for i in range(no_energy_groups-1)]

            for bdi in range(self.rom_dict.no_fueltemp_bases):

                # Reading self mass matrix
                fname = os.path.join(path, "massTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(bdi)+"_"+name)
                self.mass_tensors_fueltemp[zonei].append(read_matrix(fname, binary))
    
                # Reading self stiffness matrix
                fname = os.path.join(path, "laplTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(bdi)+"_"+name)
                self.stiffness_tensors_fueltemp[zonei].append(read_matrix(fname, binary))
    
                 # Reading power intergrals
                fname = os.path.join(path, "redIntegralsT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(bdi)+"_"+name)
                self.red_integral_tensors_fueltemp[zonei].append(read_list(fname, binary))
    
                counter = 0
                for fromi in range(no_energy_groups):
                    
                    if fromi == self.group_number:
                        continue
    
                    fname = os.path.join(path, "crossMassTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(fromi)+"_"+str(bdi)+"_"+name)
                    self.cross_mass_tensors_fueltemp[zonei][counter].append(read_matrix(fname, binary))

                    counter += 1

    def sum_fueltemp_terms(self, deim_solution):

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.mass_matrices_fueltemp = []
        self.stiffness_matrices_fueltemp = []
        self.red_integrals_fueltemp = []
        self.cross_mass_matrices_fueltemp = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            mass_matrix = self.mass_matrices[zonei]*0.0
            stiffness_matrix = self.stiffness_matrices[zonei]*0.0
            red_integral = self.red_integrals[zonei]*0.0

            cross_mass_matrices = [0.0*matrix for matrix in self.cross_mass_matrices[zonei]]

            for bdi in range(self.rom_dict.no_fueltemp_bases):

                mass_matrix += deim_solution[bdi] * self.mass_tensors_fueltemp[zonei][bdi]
                stiffness_matrix += deim_solution[bdi] * self.stiffness_tensors_fueltemp[zonei][bdi]
                red_integral += deim_solution[bdi] * self.red_integral_tensors_fueltemp[zonei][bdi]
    
                for counter in range(len(cross_mass_matrices)):
    
                    cross_mass_matrices[counter] += deim_solution[bdi] * self.cross_mass_tensors_fueltemp[zonei][counter][bdi]

            self.mass_matrices_fueltemp.append(mass_matrix) 
            self.stiffness_matrices_fueltemp.append(stiffness_matrix) 
            self.red_integrals_fueltemp.append(red_integral)
            self.cross_mass_matrices_fueltemp[zonei] = cross_mass_matrices


    def load_rhocool_operators(self, path, binary):

        print("Loading coolant density matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.mass_tensors_rhocool = [[] for i in range(no_zones)]
        self.stiffness_tensors_rhocool = [[] for i in range(no_zones)]
        self.red_integral_tensors_rhocool = [[] for i in range(no_zones)]
        self.cross_mass_tensors_rhocool = [[] for i in range(no_zones)]

        name = "TCool"

        for zonei in range(no_zones):

            self.cross_mass_tensors_rhocool[zonei] = [[] for i in range(no_energy_groups-1)]

            for bdi in range(self.rom_dict.no_rhocool_bases):

                # Reading self mass matrix
                fname = os.path.join(path, "massTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(bdi)+"_"+name)
                self.mass_tensors_rhocool[zonei].append(read_matrix(fname, binary))
    
                # Reading self stiffness matrix
                fname = os.path.join(path, "laplTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(bdi)+"_"+name)
                self.stiffness_tensors_rhocool[zonei].append(read_matrix(fname, binary))
    
                 # Reading power intergrals
                fname = os.path.join(path, "redIntegralsT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(bdi)+"_"+name)
                self.red_integral_tensors_rhocool[zonei].append(read_list(fname, binary))
    
                counter = 0
                for fromi in range(no_energy_groups):
                    
                    if fromi == self.group_number:
                        continue
    
                    fname = os.path.join(path, "crossMassTensorT_"+str(self.group_number)+"_"+str(zonei)+"_"+str(fromi)+"_"+str(bdi)+"_"+name)
                    self.cross_mass_tensors_rhocool[zonei][counter].append(read_matrix(fname, binary))

                    counter += 1


    def sum_rhocool_terms(self, deim_solution):

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.mass_matrices_rhocool = []
        self.stiffness_matrices_rhocool = []
        self.red_integrals_rhocool = []
        self.cross_mass_matrices_rhocool = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            mass_matrix = self.mass_matrices[zonei]*0.0
            stiffness_matrix = self.stiffness_matrices[zonei]*0.0
            red_integral = self.red_integrals[zonei]*0.0

            cross_mass_matrices = [0.0*matrix for matrix in self.cross_mass_matrices[zonei]]

            for bdi in range(self.rom_dict.no_rhocool_bases):

                mass_matrix += deim_solution[bdi] * self.mass_tensors_rhocool[zonei][bdi]
                stiffness_matrix += deim_solution[bdi] * self.stiffness_tensors_rhocool[zonei][bdi]
                red_integral += deim_solution[bdi] * self.red_integral_tensors_rhocool[zonei][bdi]
    
                for counter in range(len(cross_mass_matrices)):
    
                    cross_mass_matrices[counter] += deim_solution[bdi] * self.cross_mass_tensors_rhocool[zonei][counter][bdi] 

            self.mass_matrices_rhocool.append(mass_matrix) 
            self.stiffness_matrices_rhocool.append(stiffness_matrix) 
            self.red_integrals_rhocool.append(red_integral)
            self.cross_mass_matrices_rhocool[zonei] = cross_mass_matrices 


    



            






        








