import copy
import numpy as np
import openfoamparser as ofp
import os
from utilities import read_list, read_matrix
from .rom_base import ROMBase
from deim import FlowresDEIM

class VelocityROM(ROMBase):

    def __init__(self, name, region_name, control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh):

        ROMBase.__init__(self, name, region_name, control_dict, rom_dict, mesh)

        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict

        self.sup_basis_functions = []

        self.advecion_matrices = []
        self.advection_mx = []
        self.cont_error_matrices = []
        self.cont_err_mx = []
        self.lam_diffusion_matrix = []
        self.lam_deviatoric_matrix = []
        self.press_grad_matrix = []
        self.momentum_sources = []

        self.turb_diff_tensor = []
        self.turb_dev_tensor = []

        self.turb_diff_matrix = []
        self.turb_dev_matrix = []

        self.boussinesq_vector = []
        self.boussinesq_matrix = []

        self.flowres_deims = []
        for zone in self.phase_dict.zone_names:
            if zone in self.phase_dict.flowres_A:
                if self.phase_dict.flowres_A != 0.0:
                    self.flowres_deims.append(FlowresDEIM("implicitMomentumSource"+zone, 
                                            rom_dict, phase_dict, thermo_dict, bd_dict))

    def load_sup_bases(self, path, no_vectors):

        print("Loading UDarcysup basis vectors.")

        self.sup_basis_functions = list()
        for basei in range(no_vectors):
            fpath = os.path.join(path, str(basei), self.region_name, "UDarcysupPOD")            
            self.sup_basis_functions.append(ofp.parse_internal_field(fpath))

    def combine_supremizer_bases(self):

        print("Combining supremizer bases.")
        for sup_base in self.sup_basis_functions:
            self.basis_functions.append(sup_base)

        self.solution = np.zeros(len(self.basis_functions))

    def initialize_solution(self):

        fpath = os.path.join(self.control_dict.start_time, self.region_name, self.name)            
        initial_field = ofp.parse_internal_field(fpath)
        V = self.mesh.cell_volumes

        sys_size = len(self.basis_functions)
        sys_mx = np.zeros((sys_size, sys_size))
        sys_rhs = np.zeros(sys_size)

        weighted_base = copy.copy(self.basis_functions[0])
        for i in range(len(weighted_base)):
            weighted_base[i] = weighted_base[i]*V[i]

        for i in range(sys_size):
            weighted_basis = copy.copy(self.basis_functions[i])
            for k in range(len(weighted_base)):
                weighted_basis[k] = weighted_basis[k]*V[k]

            sys_rhs[i] = np.tensordot(initial_field, weighted_basis)
            
            for j in range(sys_size):
                sys_mx[i,j] = np.tensordot(weighted_basis, self.basis_functions[j])

        self.solution = np.linalg.solve(sys_mx, sys_rhs)

    def load_self_operators(self, path, binary):

        print("Loading self matrices for "+self.name+".")

        no_zones = self.phase_dict.no_zones
        no_bds = self.bd_dict.no_bds
        no_nut_bases = self.rom_dict.no_nut_bases
        no_temp_bases = self.rom_dict.no_temp_bases

        fname = os.path.join(path, "redDiffMx")
        self.lam_diffusion_matrix = read_matrix(fname, binary)

        fname = os.path.join(path, "redDevMx")
        self.lam_deviatoric_matrix = read_matrix(fname, binary)

        fname = os.path.join(path, "redPressGradMx")
        self.press_grad_matrix = read_matrix(fname, binary)

        fname = os.path.join(path, "redBoussinesqMx")
        self.boussinesq_matrix = read_matrix(fname, binary)

        for zonei in range(no_zones):

            fname = os.path.join(path, "redPumpSrc_"+str(zonei))
            self.momentum_sources.append(read_list(fname, binary))

        for basei in range(len(self.solution)):

            fname = os.path.join(path, "redConvMx_"+str(basei))
            self.advecion_matrices.append(read_matrix(fname, binary))

            fname = os.path.join(path, "redContErrMx_"+str(basei))
            self.cont_error_matrices.append(read_matrix(fname, binary))

        for module in self.flowres_deims:
            module.load_self_operators(path, binary)

        for ni in range(no_nut_bases):

            fname = os.path.join(path, "redTurbDiffTensor_"+str(ni))
            self.turb_diff_tensor.append(read_matrix(fname, binary))

            fname = os.path.join(path, "redTurbDevTensor_"+str(ni))
            self.turb_dev_tensor.append(read_matrix(fname, binary))

    def sum_advection_terms(self, solution):

        base_size = len(self.solution)
        self.advection_mx = np.zeros((base_size, base_size))
        self.cont_err_mx = np.zeros((base_size, base_size))

        for i in range(base_size):

            self.advection_mx += solution[i] * self.advecion_matrices[i]
            self.cont_err_mx += solution[i] * self.cont_error_matrices[i]

    def sum_turb_diff_terms(self, nut_solution = []):

        base_size = len(self.solution)

        self.turb_diff_matrix = np.zeros((base_size,base_size))
        self.turb_dev_matrix = np.zeros((base_size,base_size))

        for i in range(len(nut_solution)):
            self.turb_diff_matrix += nut_solution[i]*self.turb_diff_tensor[i]
            self.turb_dev_matrix += nut_solution[i]*self.turb_dev_tensor[i]

    def sum_temperature_terms(self, temp_solution = []):

        base_size = len(self.solution)

        self.boussinesq_vector = np.zeros(base_size)

        for i in range(len(temp_solution)):
            self.boussinesq_vector += temp_solution[i] * self.boussinesq_matrix[:,i]

    def sum_flowres_terms(self):

        for module in self.flowres_deims:
            module.sum_operators(self.solution)



