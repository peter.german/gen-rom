from scipy.interpolate import griddata
import numpy as np
import openfoamparser as ofp
import os
from utilities import read_list, read_matrix
from .rom_base import ROMBase

class EnergyROM(ROMBase):

    def __init__(self, name, region_name, cross_sections, control_dict, rom_dict, phase_dict, thermo_dict, bd_dict, mesh):

        ROMBase.__init__(self, name, region_name, control_dict, rom_dict, mesh)

        self.xs = cross_sections
        self.phase_dict = phase_dict
        self.thermo_dict = thermo_dict
        self.bd_dict = bd_dict

        self.diff_matrix = []
        self.vol_heat_src = []
        self.vol_neutro_src = []
        self.turb_diff_tensor = []
        self.advection_tensor = []
        self.conterr_tensor = []
        self.adv_bd_tensor = []
        self.heat_transfer_tensor = []
        self.heat_transfer_sources = [] 
        self.flux_tensor = []
        self.flux_tensor_fueltemp = []
        self.flux_tensor_rhocool = []   

        self.advection_matrix = None
        self.conterr_matrix = None
        self.turb_diff_matrix = None

        self.flux_matrices_fueltemp = []
        self.flux_matrices_rhocool = []    

    def load_self_operators(self, path, binary):

        print("Loading self matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_velocity_bases = self.rom_dict.no_velocity_bases + self.rom_dict.no_supremizer_bases
        no_alphat_bases = self.rom_dict.no_alphat_bases

        self.heat_transfer_tensor = []
        self.heat_transfer_sources = []

        self.flux_tensor = [[] for i in range(no_zones)]

        fname = os.path.join(path, "diffMatrix")
        self.diff_matrix = read_matrix(fname, binary)

        fname = os.path.join(path, "redVolHeatSrc")
        self.vol_heat_src = read_list(fname, binary)

        for zonei in range(no_zones):

            fname = os.path.join(path, "heatTransferVectors_"+str(zonei))
            self.heat_transfer_sources.append(read_list(fname, binary))

            fname = os.path.join(path, "heatTransferMxs_"+str(zonei))
            self.heat_transfer_tensor.append(read_matrix(fname, binary))

            for fromi in range(no_energy_groups):

                fname = os.path.join(path, "crossFluxTensor_"+str(zonei)+"_"+str(fromi)+"_"+self.name)
                self.flux_tensor[zonei].append(read_matrix(fname, binary))

        for veli in range(no_velocity_bases):

            fname = os.path.join(path, "advTensor_"+str(veli))
            self.advection_tensor.append(read_matrix(fname, binary))

            fname = os.path.join(path, "contErrTensor_"+str(veli))
            self.conterr_tensor.append(read_matrix(fname, binary)) 

        for ai in range(no_alphat_bases):

            fname = os.path.join(path, "turbDiffTensor_"+str(ai))
            self.turb_diff_tensor.append(read_matrix(fname, binary))

    def sum_fluid_operators(self, velocity_solution=[], alphat_solution=[]):

        no_own_bases = len(self.basis_functions)
        no_velocity_bases = len(velocity_solution)
        
        tmp_adv_matrix = np.zeros((no_own_bases, no_own_bases))
        tmp_turbdiff_matrix = np.zeros((no_own_bases, no_own_bases))
        tmp_conterr_matrix = np.zeros((no_own_bases, no_own_bases))

        for veli in range(len(velocity_solution)):

            tmp_adv_matrix += velocity_solution[veli]*self.advection_tensor[veli]
            tmp_conterr_matrix += velocity_solution[veli]*self.conterr_tensor[veli]
            

        for ai in range(len(alphat_solution)):

            tmp_turbdiff_matrix += alphat_solution[ai]*self.turb_diff_tensor[ai]

        self.advection_matrix = tmp_adv_matrix
        self.conterr_matrix = tmp_conterr_matrix
        self.turb_diff_matrix = tmp_turbdiff_matrix

    def load_deim_operators(self, path, binary):

        print("Loading neutronics matrices for "+self.name+".")

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups

        self.flux_tensor_fueltemp = [[] for i in range(no_zones)]
        self.flux_tensor_rhocool = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            self.flux_tensor_fueltemp[zonei] = [[] for i in range(no_energy_groups)]
            self.flux_tensor_rhocool[zonei] = [[] for i in range(no_energy_groups)]

            name = ""
            if self.xs.fast_neutrons == True:
                name = "logTNeutro"
            else:
                name = "sqrtTNeutro"

            for bdi in range(self.rom_dict.no_fueltemp_bases):
    
                for fromi in range(no_energy_groups):
    
                    fname = os.path.join(path, "crossMassTensorT_"+str(zonei)+"_"+str(fromi)+"_"+str(bdi)+"_"+name)
                    self.flux_tensor_fueltemp[zonei][fromi].append(read_matrix(fname, binary))

            name = "TCool"

            for bdi in range(self.rom_dict.no_rhocool_bases):
    
                for fromi in range(no_energy_groups):
    
                    fname = os.path.join(path, "crossMassTensorT_"+str(zonei)+"_"+str(fromi)+"_"+str(bdi)+"_"+name)
                    self.flux_tensor_rhocool[zonei][fromi].append(read_matrix(fname, binary))

    def sum_fueltemp_terms(self, deim_solution=[]):

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.flux_matrices_fueltemp = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            cross_flux_matrices = [0.0*matrix for matrix in self.flux_tensor[zonei]]

            for bdi in range(self.rom_dict.no_fueltemp_bases):
    
                for counter in range(len(cross_flux_matrices)):
    
                    cross_flux_matrices[counter] += deim_solution[bdi] * self.flux_tensor_fueltemp[zonei][counter][bdi]

            self.flux_matrices_fueltemp[zonei] = cross_flux_matrices

    def sum_rhocool_terms(self, deim_solution=[]):

        no_zones = self.xs.no_zones
        no_energy_groups = self.xs.no_energy_groups
        no_precursor_groups = self.xs.no_precursor_groups

        self.flux_matrices_rhocool = [[] for i in range(no_zones)]

        for zonei in range(no_zones):

            cross_flux_matrices = [0.0*matrix for matrix in self.flux_tensor[zonei]]

            for bdi in range(self.rom_dict.no_rhocool_bases):
    
                for counter in range(len(cross_flux_matrices)):
    
                    cross_flux_matrices[counter] += deim_solution[bdi] * self.flux_tensor_rhocool[zonei][counter][bdi]

            self.flux_matrices_rhocool[zonei] = cross_flux_matrices

    def sum_neutronics_operators(self, flux_solutions=[], fueltemp_feedback=False, rhocool_feedback=False):

        no_groups = self.xs.no_energy_groups
        no_energy_groups = self.xs.no_energy_groups

        no_zones = self.xs.no_zones

        self.vol_neutro_src = np.zeros(len(self.solution))

        sigma_pow = self.xs.sigma_pow
        alpha_sigma_pow_fueltemp = self.xs.alpha_sigma_pow_fueltemp
        alpha_sigma_pow_rhocool = self.xs.alpha_sigma_pow_rhocool

        flux_tensor = self.flux_tensor
        flux_matrices_fueltemp = self.flux_matrices_fueltemp
        flux_matrices_rhocool = self.flux_matrices_rhocool

        beta_th = self.thermo_dict.beta_th
        rho = self.thermo_dict.rho

        for zonei in range(no_zones):

            zone = self.xs.zone_names[zonei]

            for gi in range(no_energy_groups):

                for basei in range(self.rom_dict.no_flux_bases[gi]):

                    self.vol_neutro_src += flux_solutions[gi][basei] * sigma_pow[zone][gi] * flux_tensor[zonei][gi][:,basei]

                    if fueltemp_feedback:

                        T_aux_ref =  self.xs.T_ref 
                        if self.xs.fast_neutrons:
                            T_aux_ref = np.log(self.xs.T_ref)
                        else:
                            T_aux_ref = np.sqrt(self.xs.T_ref)

                        self.vol_neutro_src += flux_solutions[gi][basei] * alpha_sigma_pow_fueltemp[zone][gi] * (flux_matrices_fueltemp[zonei][gi][:,basei]
                                               -T_aux_ref*flux_tensor[zonei][gi][:,basei])

                    if rhocool_feedback:

                        T_aux_ref = self.xs.T_ref

                        self.vol_neutro_src -= flux_solutions[gi][basei] * alpha_sigma_pow_rhocool[zone][gi] * beta_th * rho * (flux_matrices_rhocool[zonei][gi][:,basei]
                                               -T_aux_ref*flux_tensor[zonei][gi][:,basei])






            






        








