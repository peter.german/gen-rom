import openfoamparser as ofp
import numpy as np
import os

class ROMBase:

    def __init__(self, name, region_name, control_dict, rom_dict, mesh):

        self.name = name
        self.region_name = region_name
        self.mesh = mesh

        self.control_dict = control_dict
        self.rom_dict = rom_dict

        self.basis_functions = []
        self.solution = None     

    def load_basis_vectors(self, path, no_vectors):

        print("Loading "+self.name+" basis vectors.")

        self.basis_functions = list()
        for basei in range(no_vectors):
            fpath = os.path.join(path, str(basei), self.region_name, self.name+"POD")            
            self.basis_functions.append(ofp.parse_internal_field(fpath))

        self.solution = np.zeros(len(self.basis_functions))

    def get_reconstructed_solution(self):

        reconst = self.basis_functions[0]*0.0

        for soli in range(len(self.solution)):
            reconst += self.solution[soli]*self.basis_functions[soli]

        return reconst

    def initialize_solution(self):

        fpath = os.path.join(self.control_dict.start_time, self.region_name, self.name)            
        initial_field = ofp.parse_internal_field(fpath)
        V = self.mesh.cell_volumes

        for i in range(len(self.solution)):
            self.solution[i] = np.dot(np.multiply(initial_field, V), self.basis_functions[i])





            






        








