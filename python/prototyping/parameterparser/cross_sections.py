import os
import numpy as np

class CrossSections:

    def __init__(self, path, pathfueltemp="", pathrhocool=""):

        self.path = path

        self.pathfueltemp = pathfueltemp

        self.pathrhocool = pathrhocool

        self.no_energy_groups = 0

        self.no_precursor_groups = 0

        self.T_ref = None
        self.T_pert = None

        self.rho_ref = None
        self.rho_pert = None

        self.fast_neutrons = False

        self.zone_names = []

        self.no_zones = 0

        self.iv = {}

        self.D = {}
        self.D_fueltemp = {}
        self.D_rhocool = {}
        self.alpha_D_fueltemp = {}
        self.alpha_D_rhocool = {}

        self.nu_sigma_f = {}
        self.nu_sigma_f_fueltemp = {}
        self.nu_sigma_f_rhocool = {}
        self.alpha_nu_sigma_f_fueltemp = {}
        self.alpha_nu_sigma_f_rhocool = {}

        self.sigma_pow = {}
        self.sigma_pow_fueltemp = {}
        self.sigma_pow_rhocool = {}
        self.alpha_sigma_pow_fueltemp = {}
        self.alpha_sigma_pow_rhocool = {}

        self.scattering_matrix = {}
        self.scattering_matrix_fueltemp = {}
        self.scattering_matrix_rhocool = {}
        self.alpha_scattering_matrix_fueltemp = {}
        self.alpha_scattering_matrix_rhocool = {}

        self.sigma_r = {}
        self.sigma_r_fueltemp = {}
        self.sigma_r_rhocool = {}
        self.alpha_sigma_r_fueltemp = {}
        self.alpha_sigma_r_rhocool = {}

        self.chi_p = {}

        self.chi_d = {}

        self.lamb = {}

        self.beta = {}

        self.beta_tot = {}

    def write_file(self, path, tag='', verbose=False):

        self.write_cross_sections(path, tag=tag, verbose=verbose)

        self.write_cross_sections_fueltemp(path, tag=tag, verbose=verbose)

        self.write_cross_sections_rhocool(path, tag=tag, verbose=verbose)

    def read_cross_sections(self, verbose=False):

        if verbose:
            print("Reading default cross sections.")

        xsfile = open(self.path)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split('\n')

        for li in range(len(split_text)):
            split_line = split_text[li].split()
            if "energyGroups" in split_line:
                self.no_energy_groups = int(split_line[split_line.index("energyGroups")+1])
            elif "precGroups" in split_line:
                self.no_precursor_groups = int(split_line[split_line.index("precGroups")+1])
            elif "fastNeutrons" in split_line:
                if split_line[split_line.index("fastNeutrons")+1].startswith('true'):
                    self.fast_neutrons = True
                else:
                    self.fast_neutrons = False 
            elif "zones" in split_line:
                zones_start = li
            elif ");" in split_line:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):
            if "{" in zones_raw[li]:

                name = zones_raw[li-1].replace(' ','')
                self.zone_names.append(name)

                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break

                zone = zones_raw[zone_begin:zone_end]

                for subli in range(len(zone)):
                    split_line = zone[subli].split()
                    if "IV" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.iv[name] = [float(i) for i in numbers_raw.split()]
                    elif "D" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.D[name] = [float(i) for i in numbers_raw.split()]
                    elif "nuSigmaEff" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.nu_sigma_f[name] = [float(i) for i in numbers_raw.split()]
                    elif "sigmaPow" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.sigma_pow[name] = [float(i) for i in numbers_raw.split()]
                    elif "scatteringMatrixP0" in split_line:
                        tmp = []
                        for i in range(self.no_energy_groups):
                            numbers_raw = zone[subli+i+1][zone[subli+i+1].find('(')+1:zone[subli+i+1].find(')')]
                            tmp.append([float(i) for i in numbers_raw.split()])
                                
                        self.scattering_matrix[name] = tmp
                    elif "sigmaDisapp" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.sigma_r[name] = [float(i) for i in numbers_raw.split()]
                    elif "chiPrompt" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.chi_p[name] = [float(i) for i in numbers_raw.split()]
                    elif "chiDelayed" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.chi_d[name] = [float(i) for i in numbers_raw.split()]
                    elif "lambda" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.lamb[name] = [float(i) for i in numbers_raw.split()]
                    elif "Beta" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.beta[name] = [float(i) for i in numbers_raw.split()]
                        self.beta_tot[name] = sum(self.beta[name])

        self.no_zones = len(self.zone_names)


    def write_cross_sections(self, path, tag='', verbose=False):

        if verbose:
            print("writing default cross sections.")

        xsfile = open(self.path)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split("\n")
        for li in range(len(split_text)):
            if "zones" in split_text[li]:
                zones_start = li
            elif ");" in split_text[li]:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):

            if "{" in zones_raw[li].split(' '):

                name = zones_raw[li-1].split(' ')[0]
                 
                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break
                        
                zone = zones_raw[zone_begin:zone_end]
 
                for subli in range(len(zone)):

                    split_line = zone[subli].split(' ')

                    if "IV" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.iv[name]]) + \
                              zone[subli][zone[subli].find(")")-1:]
                    elif "D" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.D[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "nuSigmaEff" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.nu_sigma_f[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "sigmaPow" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.sigma_pow[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "scatteringMatrixP0" in split_line:
                        for gi in range(1,self.no_energy_groups+1):
                            zone[subli+gi] = zone[subli+gi][0:zone[subli+gi].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.scattering_matrix[name][gi-1]]) + \
                              zone[subli+gi][zone[subli+gi].find(")"):]
                        
                    elif "sigmaDisapp" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.sigma_r[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "chiPrompt" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.chi_p[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "chiDelayed" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.chi_d[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "lambda" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.lamb[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "Beta" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.beta[name]]) + \
                              zone[subli][zone[subli].find(")"):]

                zones_raw[zone_begin:zone_end] = zone

        split_text[zones_start+2:zones_end] = zones_raw

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'nuclearData'+tag), 'w')
        of.write(out_text)
        of.close()

    def read_cross_sections_fueltemp(self, verbose=False):

        if verbose:
            print("Reading fuel temperature cross sections.")

        if self.pathfueltemp == "": 
            raise ValueError("A path must be given to the fuel temperature cross sections!")

        xsfile = open(self.pathfueltemp)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split('\n')
        for li in range(len(split_text)):
            split_line = split_text[li].split()
            if "TfuelRef" in split_line:
                self.T_ref = float(split_line[split_line.index("TfuelRef")+1])
            elif "TfuelPerturbed" in split_line:
                self.T_pert = float(split_line[split_line.index("TfuelPerturbed")+1])
            elif "zones" in split_line:
                zones_start = li
            elif ");" in split_line:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):
            if "{" in zones_raw[li]:

                name = zones_raw[li-1].replace(' ','')
                if name not in self.zone_names:
                    raise ValueError("Unknown zone defined in fuel temperature cross sections!")

                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break

                zone = zones_raw[zone_begin:zone_end]

                for subli in range(len(zone)):
                    split_line = zone[subli].split()
                    if "D" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.D_fueltemp[name] = [float(i) for i in numbers_raw.split()]
                    elif "nuSigmaEff" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.nu_sigma_f_fueltemp[name] = [float(i) for i in numbers_raw.split()]
                    elif "sigmaPow" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.sigma_pow_fueltemp[name] = [float(i) for i in numbers_raw.split()]
                    elif "scatteringMatrixP0" in split_line:
                        tmp = []
                        for i in range(self.no_energy_groups):
                            numbers_raw = zone[subli+i+1][zone[subli+i+1].find('(')+1:zone[subli+i+1].find(')')]
                            tmp.append([float(i) for i in numbers_raw.split()])
                                
                        self.scattering_matrix_fueltemp[name] = tmp
                    elif "sigmaDisapp" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.sigma_r_fueltemp[name] = [float(i) for i in numbers_raw.split()]

    def write_cross_sections_fueltemp(self, path, tag='', verbose=False):

        if verbose:
            print("writing default cross sections.")

        xsfile = open(self.pathfueltemp)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split("\n")
        for li in range(len(split_text)):
            if "zones" in split_text[li]:
                zones_start = li
            elif ");" in split_text[li]:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):

            if "{" in zones_raw[li].split(' '):

                name = zones_raw[li-1].split(' ')[0]
                 
                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break
                        
                zone = zones_raw[zone_begin:zone_end]
 
                for subli in range(len(zone)):

                    split_line = zone[subli].split(' ')

                    if "D" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.D_fueltemp[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "nuSigmaEff" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.nu_sigma_f_fueltemp[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "sigmaPow" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.sigma_pow_fueltemp[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "scatteringMatrixP0" in split_line:
                        for gi in range(1,self.no_energy_groups+1):
                            zone[subli+gi] = zone[subli+gi][0:zone[subli+gi].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.scattering_matrix_fueltemp[name][gi-1]]) + \
                              zone[subli+gi][zone[subli+gi].find(")"):]
                    elif "sigmaDisapp" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.sigma_r_fueltemp[name]]) + \
                              zone[subli][zone[subli].find(")"):]

                zones_raw[zone_begin:zone_end] = zone
        split_text[zones_start+2:zones_end] = zones_raw

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'nuclearDataFuelTemp'+tag), 'w')
        of.write(out_text)
        of.close()

    def read_cross_sections_rhocool(self, verbose=False):

        if verbose:
            print("Reading coolant density cross sections.")

        if self.pathrhocool == "": 
            raise ValueError("A path must be given to the coolant density cross sections!")

        xsfile = open(self.pathrhocool)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split('\n')
        for li in range(len(split_text)):
            split_line = split_text[li].split()
            if "rhoCoolRef" in split_line:
                self.rho_ref = float(split_line[split_line.index("rhoCoolRef")+1])
            elif "rhoCoolPerturbed" in split_line:
                self.rho_pert = float(split_line[split_line.index("rhoCoolPerturbed")+1])
            elif "zones" in split_line:
                zones_start = li
            elif ");" in split_line:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):
            if "{" in zones_raw[li]:

                name = zones_raw[li-1].replace(' ','')
                if name not in self.zone_names:
                    raise ValueError("Unknown zone defined in rho coolant cross sections!")

                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break

                zone = zones_raw[zone_begin:zone_end]

                for subli in range(len(zone)):
                    split_line = zone[subli].split()
                    if "D" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.D_rhocool[name] = [float(i) for i in numbers_raw.split()]
                    elif "nuSigmaEff" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.nu_sigma_f_rhocool[name] = [float(i) for i in numbers_raw.split()]
                    elif "sigmaPow" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.sigma_pow_rhocool[name] = [float(i) for i in numbers_raw.split()]
                    elif "scatteringMatrixP0" in split_line:
                        tmp = []
                        for i in range(self.no_energy_groups):
                            numbers_raw = zone[subli+i+1][zone[subli+i+1].find('(')+1:zone[subli+i+1].find(')')]
                            tmp.append([float(i) for i in numbers_raw.split()])
                                
                        self.scattering_matrix_rhocool[name] = tmp
                    elif "sigmaDisapp" in split_line:
                        numbers_raw = zone[subli][zone[subli].find('(')+1:zone[subli].find(')')]
                        self.sigma_r_rhocool[name] = [float(i) for i in numbers_raw.split()]

    def write_cross_sections_rhocool(self, path, tag='', verbose=False):

        if verbose:
            print("writing default cross sections.")

        xsfile = open(self.pathrhocool)
        xs_raw_text = xsfile.read()
        xsfile.close()

        zones_start = 0
        zones_end = 0
        split_text = xs_raw_text.split("\n")
        for li in range(len(split_text)):
            if "zones" in split_text[li]:
                zones_start = li
            elif ");" in split_text[li]:
                zones_end = li

        zones_raw = split_text[zones_start+2:zones_end]

        for li in range(len(zones_raw)):

            if "{" in zones_raw[li].split(' '):

                name = zones_raw[li-1].split(' ')[0]
                 
                zone_begin = li+1
                zone_end = li+1
                for subli in range(li+1,len(zones_raw)):
                    if "}" in zones_raw[subli]:
                        zone_end = subli
                        break
                        
                zone = zones_raw[zone_begin:zone_end]
 
                for subli in range(len(zone)):

                    split_line = zone[subli].split(' ')

                    if "D" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.D_rhocool[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "nuSigmaEff" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.nu_sigma_f_rhocool[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "sigmaPow" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.sigma_pow_rhocool[name]]) + \
                              zone[subli][zone[subli].find(")"):]
                    elif "scatteringMatrixP0" in split_line:
                        for gi in range(1,self.no_energy_groups+1):
                            zone[subli+gi] = zone[subli+gi][0:zone[subli+gi].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.scattering_matrix_rhocool[name][gi-1]]) + \
                              zone[subli+gi][zone[subli+gi].find(")"):]
                    elif "sigmaDisapp" in split_line:
                        zone[subli] = zone[subli][0:zone[subli].find("(")+1] + \
                              ' '.join(["{:.6e}".format(i) for i in self.sigma_r_rhocool[name]]) + \
                              zone[subli][zone[subli].find(")"):]

                zones_raw[zone_begin:zone_end] = zone
        split_text[zones_start+2:zones_end] = zones_raw

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'nuclearDataRhoCool'+tag), 'w')
        of.write(out_text)
        of.close()

    def create_fueltemp_feedback_coefficients(self, verbose=False):

        if verbose:
            print("Assumed fast neutron spectrum: ", self.fast_neutrons)

        dT=0.0
        if self.fast_neutrons:
            dT = np.log(self.T_pert/self.T_ref)
        else:
            dT = np.sqrt(self.T_pert) - np.sqrt(self.T_ref)

        for name in self.D:
            self.alpha_D_fueltemp[name] = (np.asarray(self.D_fueltemp[name]) - np.asarray(self.D[name])) / dT

        for name in self.nu_sigma_f:
            self.alpha_nu_sigma_f_fueltemp[name] = (np.asarray(self.nu_sigma_f_fueltemp[name]) - 
                                                    np.asarray(self.nu_sigma_f[name])) / dT
        for name in self.sigma_r:
            self.alpha_sigma_r_fueltemp[name] = (np.asarray(self.sigma_r_fueltemp[name]) - 
                                                    np.asarray(self.sigma_r[name])) / dT
        for name in self.sigma_pow:
            self.alpha_sigma_pow_fueltemp[name] = (np.asarray(self.sigma_pow[name]) - 
                                                   np.asarray(self.sigma_pow[name])) / dT
        for name in self.scattering_matrix:
            self.alpha_scattering_matrix_fueltemp[name] = (np.asarray(self.scattering_matrix_fueltemp[name]) - 
                                                           np.asarray(self.scattering_matrix[name])) / dT

    def create_rhocool_feedback_coefficients(self, verbose=False):

        drho = self.rho_pert - self.rho_ref

        for name in self.D:
            self.alpha_D_rhocool[name] = (np.asarray(self.D_rhocool[name]) - np.asarray(self.D[name])) / drho
            
        for name in self.nu_sigma_f:
            self.alpha_nu_sigma_f_rhocool[name] = (np.asarray(self.nu_sigma_f_rhocool[name]) - 
                                                    np.asarray(self.nu_sigma_f[name])) / drho
        for name in self.sigma_r:
            self.alpha_sigma_r_rhocool[name] = (np.asarray(self.sigma_r_rhocool[name]) - 
                                                    np.asarray(self.sigma_r[name])) / drho
        for name in self.sigma_pow:
            self.alpha_sigma_pow_rhocool[name] = (np.asarray(self.sigma_pow_rhocool[name]) - 
                                                   np.asarray(self.sigma_pow[name])) / drho
        for name in self.scattering_matrix:
            self.alpha_scattering_matrix_rhocool[name] = (np.asarray(self.scattering_matrix_rhocool[name]) - 
                                                           np.asarray(self.scattering_matrix[name])) / drho

    def get_parameter(self, name, zone, entry, tag=''):

        if (zone != 'noZone') and (zone not in self.zone_names):
            raise ValueError("Zone name:  "+zone+" is missing for the preturbation of cross sections!")

        if (tag == ''):
            if name == "IV":
                return self.iv[zone][entry]
            elif name == 'D':
                return self.D[zone][entry]
            elif name == 'nuSigmaEff':
                return self.nu_sigma_f[zone][entry]
            elif name == 'sigmaPow':
                return self.sigma_pow[zone][entry]
            elif name == 'scatteringMatrixP0':
                from_i = entry // self.no_energy_groups
                to_i = entry % self.no_energy_groups
                return self.scattering_matrix[zone][from_i][to_i]
            elif name == 'sigmaDisapp':
                return self.sigma_r[zone][entry]
            elif name == 'chiPrompt':
                return self.chi_p[zone][entry]
            elif name == 'chiDelayed':
                return self.chi_d[zone][entry]
            elif name == 'lambda':
                return self.lamb[zone][entry]
            elif name == 'Beta':
                return self.beta[zone][entry]
            else:
                raise ValueError("Unknown cross-section to acess!")
        elif (tag == 'fuelTemp'):
            if name == 'D':
                return self.D_fueltemp[zone][entry]
            elif name == 'nuSigmaEff':
                return self.nu_sigma_f_fueltemp[zone][entry]
            elif name == 'sigmaPow':
                return self.sigma_pow_fueltemp[zone][entry]
            elif name == 'scatteringMatrixP0':
                from_i = entry // self.no_energy_groups
                to_i = entry % self.no_energy_groups
                return self.scattering_matrix_fueltemp[zone][from_i][to_i]
            elif name == 'sigmaDisapp':
                return self.sigma_r_fueltemp[zone][entry]
            else:
                raise ValueError("Unknown perturbed cross-section (fuel-temp)!")
        elif (tag == 'rhoCool'):
            if name == 'D':
                return self.D_rhocool[zone][entry]
            elif name == 'nuSigmaEff':
                return self.nu_sigma_f_rhocool[zone][entry]
            elif name == 'sigmaPow':
                return self.sigma_pow_rhocool[zone][entry]
            elif name == 'scatteringMatrixP0':
                from_i = entry // self.no_energy_groups
                to_i = entry % self.no_energy_groups
                return self.scattering_matrix_rhocool[zone][from_i][to_i]
            elif name == 'sigmaDisapp':
                return self.sigma_r_rhocool[zone][entry]
            else:
                raise ValueError("Unknown perturbed cross-section (rho-cool)!")
        else:
            raise ValueError("Unknown perturbation tag!")

    def set_parameter(self, name, zone, entry, value, tag=''):

        if (zone != 'noZone') and (zone not in self.zone_names):
            raise ValueError("Zone name:  "+zone+" is missing for the preturbation of cross sections!")

        if (tag == ''):

            if name == "IV":
                self.iv[zone][entry] = value
            elif name == 'D':
                self.D_fueltemp[zone][entry] = self.D_fueltemp[zone][entry] \
                                               + (value - self.D[zone][entry])
                self.D_rhocool[zone][entry] = self.D_rhocool[zone][entry] \
                                               + (value - self.D[zone][entry])
                self.D[zone][entry] = value
            elif name == 'nuSigmaEff':
                self.nu_sigma_f_fueltemp[zone][entry] = self.nu_sigma_f_fueltemp[zone][entry] \
                                               + (value - self.nu_sigma_f[zone][entry])
                self.nu_sigma_f_rhocool[zone][entry] = self.nu_sigma_f_rhocool[zone][entry] \
                                               + (value - self.nu_sigma_f[zone][entry])
                self.nu_sigma_f[zone][entry] = value
            elif name == 'sigmaPow':
                self.sigma_pow_fueltemp[zone][entry] = self.sigma_pow_fueltemp[zone][entry] \
                                               + (value - self.sigma_pow[zone][entry])
                self.sigma_pow_rhocool[zone][entry] = self.sigma_pow_rhocool[zone][entry] \
                                               + (value - self.sigma_pow[zone][entry])
                self.sigma_pow[zone][entry] = value
            elif name == 'scatteringMatrixP0':
                from_i = entry // self.no_energy_groups
                to_i = entry % self.no_energy_groups
                self.scattering_matrix_fueltemp[zone][from_i][to_i] = \
                                               self.scattering_matrix_fueltemp[zone][from_i][to_i] \
                                               + (value - self.scattering_matrix[zone][from_i][to_i])
                self.scattering_matrix_rhocool[zone][from_i][to_i] = \
                                               self.scattering_matrix_rhocool[zone][from_i][to_i] \
                                               + (value - self.scattering_matrix[zone][from_i][to_i])
                self.scattering_matrix[zone][from_i][to_i] = value
            elif name == 'sigmaDisapp':
                self.sigma_r_fueltemp[zone][entry] = self.sigma_r_fueltemp[zone][entry] \
                                               + (value - self.sigma_r[zone][entry])
                self.sigma_r_rhocool[zone][entry] = self.sigma_r_rhocool[zone][entry] \
                                               + (value - self.sigma_r[zone][entry])
                self.sigma_r[zone][entry] = value
            elif name == 'chiPrompt':
                self.chi_p[zone][entry] = value
            elif name == 'chiDelayed':
                self.chi_d[zone][entry] = value
            elif name == 'lambda':
                self.lamb[zone][entry] = value
            elif name == 'Beta':
                self.beta[zone][entry] = value
            else:
                raise ValueError("Unknown cross-section to acess!")
        elif (tag == 'fuelTemp'):
            if name == 'D':
                self.D_fueltemp[zone][entry] = value
            elif name == 'nuSigmaEff':
                self.nu_sigma_f_fueltemp[zone][entry] = value
            elif name == 'sigmaPow':
                self.sigma_pow_fueltemp[zone][entry] = value
            elif name == 'scatteringMatrixP0':
                from_i = entry // self.no_energy_groups
                to_i = entry % self.no_energy_groups
                self.scattering_matrix_fueltemp[zone][from_i][to_i] = value
            elif name == 'sigmaDisapp':
                self.sigma_r_fueltemp[zone][entry] = value
            else:
                raise ValueError("Unknown perturbed cross-section (fuel-temp)!")
        elif (tag == 'rhoCool'):
            if name == 'D':
                self.D_rhocool[zone][entry] = value
            elif name == 'nuSigmaEff':
                self.nu_sigma_f_rhocool[zone][entry] = value
            elif name == 'sigmaPow':
                self.sigma_pow_rhocool[zone][entry] = value
            elif name == 'scatteringMatrixP0':
                from_i = entry // self.no_energy_groups
                to_i = entry % self.no_energy_groups
                self.scattering_matrix_rhocool[zone][from_i][to_i] = value
            elif name == 'sigmaDisapp':
                self.sigma_r_rhocool[zone][entry] = value
            else:
                raise ValueError("Unknown perturbed cross-section (rho-cool)!")
        else:
            raise ValueError("Unknown perturbation tag!")








