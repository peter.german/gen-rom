
class ROMDictionary:

    def __init__(self, path):

        self.path = path

        self.no_flux_bases = []

        self.no_prec_bases = []

        self.no_velocity_bases = 0

        self.no_pressure_bases = 0

        self.no_supremizer_bases = 0

        self.no_flowres_bases = 0

        self.no_temp_bases = 0

        self.no_fueltemp_bases = 0

        self.no_rhocool_bases = 0

        self.no_nut_bases = 0

        self.no_alphat_bases = 0

    def read_dictionary(self):

        print("Reading ROM dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split()

        for li in range(len(split_text)):
            if "numberOfMomentumBases" == split_text[li]:
                self.no_velocity_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfPressureBases" == split_text[li]:
                self.no_pressure_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfSUPBases" == split_text[li]:
                self.no_supremizer_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfFlowResBases" == split_text[li]:
                self.no_flowres_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfNutBases" == split_text[li]:
                self.no_nut_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfAlphatBases" == split_text[li]:
                self.no_alphat_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfTempBases" == split_text[li]:
                self.no_temp_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfFuelTempDEIMBases" == split_text[li]:
                self.no_fueltemp_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfRhoCoolDEIMBases" == split_text[li]:
                self.no_rhocool_bases = int(split_text[li+1].strip(";"))
                li = li + 1
            elif "numberOfFluxBases" == split_text[li]:
                no_groups = int(split_text[li+1])
                self.no_flux_bases = [int(split_text[li+2+i].replace('(','').replace(")",'').replace(';','')) for i in range(no_groups)]
                li = li + 1 + no_groups
            elif "numberOfPrecBases" == split_text[li]:
                no_groups = int(split_text[li+1])
                self.no_prec_bases = [int(split_text[li+2+i].replace('(','').replace(")",'').replace(';','')) for i in range(no_groups)]
                li = li + 1 + no_groups


        





