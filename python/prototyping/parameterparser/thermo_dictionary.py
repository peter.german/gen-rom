import os

class ThermoDictionary:

    def __init__(self, path):

        self.path = path

        self.rho = 0.0

        self.beta_th = 0.0

        self.T_ref = 0.0

        self.mu = 0.0

        self.Pr = 0.0

        self.cp = 0.0

        self.mag_g = 9.81

    def read_dictionary(self, verbose=False):

        if verbose:
            print("Reading thermophysical properties dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split()

        for li in range(len(split_text)):
            if "rho" == split_text[li]:
                self.rho = float(split_text[li+1].strip(";"))
                li = li + 1
            elif "T0" == split_text[li]:
                self.T_ref = float(split_text[li+1].strip(";"))
                li = li + 1
            elif "beta" == split_text[li]:
                self.beta_th = float(split_text[li+1].strip(";"))
                li = li + 1
            elif "Cp" == split_text[li]:
                self.cp = float(split_text[li+1].strip(";"))
                li = li + 1
            elif "mu" == split_text[li]:
                self.mu = float(split_text[li+1].strip(";"))
                li = li + 1
            elif "Pr" == split_text[li]:
                self.Pr = float(split_text[li+1].strip(";"))

    def write_file(self, path, tag='', verbose=False):

        if verbose:
            print("Reading thermophysical properties dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split("\n")

        for li in range(len(split_text)):

            split_line = split_text[li].split(' ')

            if "rho" in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("rho")+4] + \
                              "{:.6e}".format(self.rho) + ';'
            elif "T0"in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("T0")+3] + \
                              "{:.6e}".format(self.T_ref) + ';'
            elif "beta" in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("beta")+5] + \
                              "{:.6e}".format(self.beta_th) + ';'
            elif "Cp" in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("Cp")+3] + \
                              "{:.6e}".format(self.cp) + ';'
            elif "mu" in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("mu")+3] + \
                              "{:.6e}".format(self.mu) + ';'
            elif "Pr" in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("Pr")+3] + \
                              "{:.6e}".format(self.Pr) + ';'

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'thermophysicalProperties'+tag), 'w')
        of.write(out_text)
        of.close()

    def get_parameter(self, name, zone, entry, tag=''):

        if name == "rho":
             return self.rho
        elif name == 'beta':
             return self.beta_th
        elif name == 'mu':
             return self.mu
        elif name == 'Pr':
             return self.Pr
        elif name == 'cp':
             return self.cp
        elif name == 'T0':
             return self.T_ref
        else:
            raise ValueError("Unknown thermophysical parameter to access!")


    def set_parameter(self, name, zone, entry, value, tag=''):

        if name == "rho":
             self.rho = value
        elif name == 'beta':
             self.beta_th = value
        elif name == 'mu':
             self.mu = value
        elif name == 'Pr':
             self.Pr = value
        elif name == 'cp':
             self.cp = value
        elif name == 'T0':
             self.T_ref = value
        else:
            raise ValueError("Unknown thermophysical parameter to access!")


        





