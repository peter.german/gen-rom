
class ControlDictionary:

    def __init__(self, path):

        self.path = path

        self.binary = False

        self.start_time = "0"

    def read_dictionary(self):

        print("Reading control dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split()

        for li in range(len(split_text)):
            if "writeFormat" == split_text[li]:
                if split_text[li+1].strip(";") == "binary":
                    self.binary = True
                    li = li + 1
            elif "startTime" == split_text[li]:
                self.start_time = split_text[li+1].strip(";")
                li = li + 1

        





