import os

class ReactorStateDictionary:

    def __init__(self, path):

        self.path = path

        self.p_target = 1.0

        self.keff_0 = 1.0

    def read_dictionary(self, verbose=False):

        if verbose:
            print("Reading reactor state dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split()

        for li in range(len(split_text)):
            if "pTarget" == split_text[li]:
                self.p_target = float(split_text[li+1].strip(";"))
                li = li + 1
            elif "keff" == split_text[li]:
                self.keff_0 = float(split_text[li+1].strip(";"))
                li = li + 1

    def write_file(self, path, tag='', verbose=False):

        if verbose:
            print("Writing thermophysical properties dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split("\n")

        for li in range(len(split_text)):

            split_line = split_text[li].split(' ')

            if "pTarget" in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("pTarget")+8] + \
                              "{:.6e}".format(self.pTarget) + ';'
            elif "keff"in split_line:
                split_text[li] = split_text[li][0:split_text[li].find("keff")+5] + \
                              "{:.8e}".format(self.keff_0) + ';'

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'reactorState'+tag), 'w')
        of.write(out_text)
        of.close()

    def get_parameter(self, name, zone, entry, tag=''):

        if name == "pTarget":
             return self.p_target
        elif name == 'keff':
             return self.keff_0
        else:
            raise ValueError("Unknown reactor state parameter to access!")


    def set_parameter(self, name, zone, entry, value, tag=''):

        if name == "pTarget":
             self.p_target = value
        elif name == 'keff':
             self.keff_0 = value
        else:
            raise ValueError("Unknown reactor state parameter to access!")


        





