import os
import sys
import copy
import numpy as np

class Perturbator:

    def __init__(self, original_db, res_dir, param_table_name, 
                 sample_matrix, intervals, tagged_params, save_path):

        self.original_db = original_db

        self.res_dir = res_dir

        self.param_table_name = param_table_name

        self.save_path = save_path

        self.sample_matrix = sample_matrix

        self.real_parameters = np.zeros(sample_matrix.shape)

        self.ns = len(sample_matrix)

        self.nd = None
        if len(sample_matrix) > 0:
            self.nd = len(sample_matrix[0])

        self.intervals = intervals

        self.tagged_params = tagged_params

        self.tagged_indices = []

        self.param_samples = []

    def save_original_db(self):

        i=0
        while i<50000000:

            dirname = "ROMFiles/originalDataBase_"+str(i)

            if not os.path.exists(dirname):

                for inpf in self.original_db:
                    db_dir = self.original_db[inpf].path.replace('constant', dirname) \
                                                       .replace('system', dirname) \
                                                       .replace(inpf,'')
                    if not os.path.exists(db_dir):
                        os.makedirs(db_dir)
                    self.original_db[inpf].write_file(db_dir)
                return
            else:
                i=i+1

        return

    def compute_real_parameters(self):

        min_array = []
        delta_array = []

        self.tagged_indices = []

        counter = 0
        for inpf in self.original_db:

            for zone in self.intervals[inpf]:

                for param in self.intervals[inpf][zone]:

                    is_scalar = isinstance(self.intervals[inpf][zone][param][0], float)

                    if not is_scalar:

                        for entry in self.intervals[inpf][zone][param]:

                            print(entry)

                            if inpf in self.tagged_params:
                                if zone in self.tagged_params[inpf]:
                                    if param in self.tagged_params[inpf][zone]:
                                        self.tagged_indices.append(counter)

                            min_array.append(entry[1][0])
                            delta_array.append(entry[1][1] - entry[1][0])

                            counter += 1
                    else:

                        min_array.append(self.intervals[inpf][zone][param][0])
                        delta_array.append(self.intervals[inpf][zone][param][1] 
                                           - self.intervals[inpf][zone][param][0])

                        if inpf in self.tagged_params:
                            if zone in self.tagged_params[inpf]:
                                if param in self.tagged_params[inpf][zone]:
                                    self.tagged_indices.append(counter)
                        
                        counter += 1

        min_array = np.asarray(min_array)
        delta_array = np.asarray(delta_array)

        self.real_parameters = np.multiply(np.ones(self.sample_matrix.shape), min_array) + \
                               np.multiply(self.sample_matrix, delta_array)

    def write_data_table(self):

        data_table_to_print = []

        if (len(self.res_dir) > 1):

            self.tagged_indices.append(self.nd)

            for eli in range(len(self.real_parameters)):

                for ti in self.res_dir:
                
                    tmp = self.real_parameters[eli].tolist()
                    tmp.append(float(ti))

                    data_table_to_print.append(tmp)
        else:
            data_table_to_print = self.real_parameters.tolist()

        temp_dir = os.path.join(os.getcwd(), 'ROMFiles/')
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)

        file_dir = os.path.join(temp_dir, self.param_table_name) 
        out_file = open(file_dir, 'w')
        out_file.write(str(len(data_table_to_print)) +" "+str(len(self.tagged_indices))+"\n")
        out_file.write("(\n")
        for line in data_table_to_print:
            out_file.write("("+" ".join(["{:.6e}".format(line[i]) for i in self.tagged_indices])+")\n")
        out_file.write(")\n") 
        out_file.close()

        self.tagged_indices = self.tagged_indices[0:-1]

    def create_new_sample(self, sample_i, sample):

        new_db = copy.deepcopy(self.original_db)

        counter = 0
        for inpf in new_db:

            for zone in self.intervals[inpf]:

                for param in self.intervals[inpf][zone]:

                    is_scalar = isinstance(self.intervals[inpf][zone][param][0], float)

                    if not is_scalar:

                        for entry in self.intervals[inpf][zone][param]:

                            new_db[inpf].set_parameter(param, zone, entry[0], sample[counter])
                            counter += 1

                    else:

                        new_db[inpf].set_parameter(param, zone, 0, sample[counter])
                        counter += 1

            new_path = new_db[inpf].path.replace('constant', self.save_path) \
                                        .replace('system', self.save_path) \
                                        .replace(inpf,'')
            if not os.path.exists(new_path):
                os.makedirs(new_path)

            new_db[inpf].write_file(new_path, tag="_"+str(sample_i))

    def create_parameter_samples(self):

        self.save_original_db()

        self.compute_real_parameters()

        self.write_data_table()

        for sample_i in range(len(self.real_parameters)):

            self.create_new_sample(sample_i, self.real_parameters[sample_i])

    def collect_original_parameter_values(self):

        current_params = {}

        for inpf in self.original_db:

            zone_tmp = {}
            for zone in self.intervals[inpf]:

                param_tmp = {}
                for param in self.intervals[inpf][zone]:

                    is_scalar = isinstance(self.intervals[inpf][zone][param][0], float)

                    if not is_scalar:

                        param_list = []

                        for entry in self.intervals[inpf][zone][param]:

                            param_list.append(self.original_db[inpf].get_parameter(param, zone, entry[0]))

                        param_tmp[param] = param_list

                    else:

                        param_val = self.original_db[inpf].get_parameter(param, zone, 0)
                        param_tmp[param] = param_val

                zone_tmp[zone] = param_tmp

            current_params[inpf] = zone_tmp