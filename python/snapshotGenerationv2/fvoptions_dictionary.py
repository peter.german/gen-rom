import os

class fvOptionsDictionary:

    def __init__(self, path):

        self.path = path

        print(path)

        self.ramp_start = 0.0

        self.ramp_end = 0.0

        self.new_factor = 0.0

        self.pump_source = []

    def read_dictionary(self, verbose=False):

        if verbose:
            print("Reading fvOptions.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split('\n')

        for li in range(len(split_text)):
            split_line = split_text[li].split('=')
            if "newFactor" in split_line[0]:
                split_line = split_text[li].split()
                eq_index = split_line.index('=')
                self.new_factor = float(split_line[eq_index+1].replace(';',''))
            elif 'startRampTime' in split_line[0]:
                split_line = split_text[li].split()
                eq_index = split_line.index('=')
                self.ramp_start = float(split_line[eq_index+1].replace(';',''))
            elif 'endRampTime' in split_line[0]:
                split_line = split_text[li].split()
                eq_index = split_line.index('=')
                self.ramp_end = float(split_line[eq_index+1].replace(';',''))
            elif 'pumpSource' in split_line[0]:
                split_line = split_text[li].split('(')
                pump_vector = split_line[1].split(',')
                pump_vector = [float(i.replace(';','').replace(')','')) for i in pump_vector]
                self.pump_vector = pump_vector                

    def write_file(self, path, tag='', verbose=False):

        if verbose:
            print("Writing fvOptions.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split("\n")

        for li in range(len(split_text)):

            split_line = split_text[li].split('=')

            if "newFactor" in split_line[0]:
                split_text[li] = split_text[li][0:split_text[li].find("=")+2] + \
                              "{:.6e}".format(self.new_factor) + ';'
            elif "startRampTime"in split_line[0]:
                split_text[li] = split_text[li][0:split_text[li].find("=")+2] + \
                              "{:.6e}".format(self.ramp_start) + ';'
            elif "endRampTime"in split_line[0]:
                split_text[li] = split_text[li][0:split_text[li].find("=")+2] + \
                              "{:.6e}".format(self.ramp_end) + ';'
            elif "pumpSource"in split_line[0]:
                split_text[li] = split_text[li][0:split_text[li].find("=")+2] + \
                              "vector("+"{:.6e}".format(self.pump_vector[0]) +', ' + \
                                        "{:.6e}".format(self.pump_vector[1]) +', ' + \
                                        "{:.6e}".format(self.pump_vector[2]) + ');'

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'fvOptions'+tag), 'w')
        of.write(out_text)
        of.close()

    def get_parameter(self, name, zone, entry, tag=''):

        if name == "newFactor":
             return self.new_factor
        elif name == 'startRampTime':
             return self.ramp_start
        elif name == 'endRampTime':
             return self.ramp_end
        elif name == 'pumpSource':
             return self.pump_vector[entry]
        else:
            raise ValueError("Unknown fvOption parameter to access!")


    def set_parameter(self, name, zone, entry, value, tag=''):

        if name == "newFactor":
             self.new_factor = value
        elif name == 'startRampTime':
             self.ramp_start = value
        elif name == 'endRampTime':
             self.ramp_end = value
        elif name == 'pumpSource':
             self.pump_vector[entry] = value
        else:
            raise ValueError("Unknown fvOption parameter to access!")


        





