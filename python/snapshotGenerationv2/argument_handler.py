#!/usr/bin/python

import os
import argparse

class ArgumentHandler:

    def __init__(self):

        self.args = None

    def parse_arguments(self):

        parser = argparse.ArgumentParser(description='Generating samples of a QoI using GeNFoam/GeNROM.')
        parser.add_argument('-i', '--input', type=self.test_input, default='snapGenInput',
                            help='The name of the input file which contains the parameters to perturb')
        parser.add_argument('-s', '--sampling', type=str, choices=['LHS','IHS','tensor','uniform'], default='LHS',
                            help='Type of sampling strategy.')
        parser.add_argument('-v', '--verbose', action='store_true', help="Enable output verbosity.")
        parser.add_argument('-np', '--np', type=self.test_procs, default=1, help='Number of threads to use.')
        parser.add_argument('-ns', '--ns', type=int, default=1, help='Number of samples to create.')
        parser.add_argument('-d', '--destination', type=str, default='ROMFiles/snapshots',
                            help='The name of the folder within ROMFiles to save results into.')
        parser.add_argument('-pt', '--paramtable', type=str, default='paramTable',
                            help='The name of the file acting as the parameter table.')
        parser.add_argument('-df', '--datfile', type=str, default=None, 
                            help='The name of the data file from the simulation.')
        parser.add_argument('-p', '--pertdest', type=str, default='ROMFiles/perturbedFiles',
                            help='The name of the folder where the perturbed input files are saved.')
        parser.add_argument('-c', '--command', type=str,
                            help='The command to execute when in production mode.')
        parser.add_argument('-t', '--test', action='store_true', help="Enable test mode.")

        self.args = parser.parse_args()

    def test_procs(self, value):

        ivalue = int(value)

        if ivalue > os.cpu_count():
            raise argparse.ArgumentTypeError(value + ' is larger than the number of processors available for'
                                             + 'the system ('+str(os.cpu_count())+")")

        return ivalue

    def test_input(self, value):

        if not os.path.exists(value):
            raise argparse.ArgumentTypeError(value + ' does not exist in the working directory!')

        return value
