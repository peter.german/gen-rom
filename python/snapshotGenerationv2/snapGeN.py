#!/usr/bin/python

from argument_handler import ArgumentHandler
from input_handler import InputHandler 

from phase_dictionary import PhaseDictionary 
from thermo_dictionary import ThermoDictionary 
from cross_sections import CrossSections
from boundary_dictionary import BoundaryDictionary
from fvoptions_dictionary import fvOptionsDictionary
from rom_dictionary import ROMDictionary
from sampling import Sampling
from perturbator import Perturbator
from production import Production

arg_handler = ArgumentHandler()
arg_handler.parse_arguments()
verbose = arg_handler.args.verbose

if arg_handler.args.verbose:
    print("----------------------------------------------------------------------")
    print("Reading input file.")
    print("----------------------------------------------------------------------")

snapgen_input = InputHandler(arg_handler.args.input)
snapgen_input.read_input_file()

if arg_handler.args.verbose:

  print("----------------------------------------------------------------------")
  print("Reading original dictionaries.")
  print("----------------------------------------------------------------------")

original_db = {}
for inpf in snapgen_input.input_files:
    if inpf == 'nuclearData':
        original_db[inpf] =  CrossSections("./constant/neutroRegion/nuclearData", 
                                           "./constant/neutroRegion/nuclearDataFuelTemp",
                                           "./constant/neutroRegion/nuclearDataRhoCool")
        original_db[inpf].read_cross_sections(verbose)
        original_db[inpf].read_cross_sections_fueltemp(verbose)
        original_db[inpf].read_cross_sections_rhocool(verbose)
    elif inpf == 'phaseProperties':
        original_db[inpf] = PhaseDictionary("./constant/fluidRegion/phaseProperties")
        original_db[inpf].read_dictionary(verbose)
    elif inpf == 'thermophysicalProperties':
        original_db[inpf] = ThermoDictionary("./constant/fluidRegion/thermophysicalProperties")
        original_db[inpf].read_dictionary(verbose)
    elif inpf == 'boundary':
        original_db[inpf] = BoundaryDictionary("./constant/fluidRegion/polyMesh/boundary")
        original_db[inpf].read_dictionary(verbose)
    elif inpf == 'fvOptions':
        original_db[inpf] = fvOptionsDictionary("./constant/fluidRegion/fvOptions")
        original_db[inpf].read_dictionary(verbose)
    elif inpf == 'ROMDict':
        original_db[inpf] = ROMDictionary("./system/ROMDict")
        original_db[inpf].read_dictionary(verbose)
    else:
        raise ValueError("Unknown parameter file name!"
                         " Check your dictionary in the input file!")

if arg_handler.args.verbose:

  print("----------------------------------------------------------------------")
  print("Preparing sample design. Chosen method: "+arg_handler.args.sampling)
  print("----------------------------------------------------------------------")

sampler = Sampling(arg_handler.args.sampling, snapgen_input.no_parameters, arg_handler.args.ns)
sampler.compute_design()


if arg_handler.args.verbose:

  print("----------------------------------------------------------------------")
  print("Perturbing original parameters and saving new input files.")
  print("----------------------------------------------------------------------")

pert = Perturbator(original_db, snapgen_input.result_folder, arg_handler.args.paramtable,
                   sampler.sample_matrix, snapgen_input.parameters, snapgen_input.tagged_parameters,
                   arg_handler.args.pertdest)
pert.create_parameter_samples()

if arg_handler.args.verbose:

  print("----------------------------------------------------------------------")
  print("Creating snapshots.")
  print("----------------------------------------------------------------------")

pert = Production(pert, snapgen_input.result_folder, arg_handler.args.datfile, arg_handler.args.command,
                  snapgen_input.results_to_print, arg_handler.args.destination, verbose=verbose)
pert.create_snapshots()






