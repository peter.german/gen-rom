import os

class PhaseDictionary:

    def __init__(self, path):

        self.path = path

        self.zone_names = []

        self.no_zones = 0

        self.gamma = {}

        self.momentum_source = {}

        self.heat_source = {}

        self.heat_exchange_coeff = {}

        self.external_T = {}

        self.vol_surface = {}

        self.flowres_A = {}

        self.flowres_B = {}

        self.Dh = {}

    def read_dictionary(self, verbose=False):

        if verbose:
            print("Reading phase properties disctionary.")

        dictfile = open(self.path)
        dict_text = dictfile.read()
        dictfile.close()

        split_text = dict_text.split()

        struct_prop_start = 0
        struct_prop_end = 0

        drag_models_start = 0
        drag_models_end = 0

        heat_transfer_models_start = 0
        heat_transfer_models_end = 0

        for li in range(len(split_text)):
            if "structureProperties" == split_text[li]:
                struct_prop_start = li+2

                li += 2
                bracket_counter = 1
                while(bracket_counter > 0):
                    if split_text[li] == '{':
                        bracket_counter += 1
                    if split_text[li] == '}':
                        bracket_counter -= 1
                    li += 1
                struct_prop_end = li
            if "dragModels" == split_text[li]:

                drag_models_start = li+2

                li += 2
                bracket_counter = 1
                while(bracket_counter > 0):
                    if split_text[li] == '{':
                        bracket_counter += 1
                    if split_text[li] == '}':
                        bracket_counter -= 1
                    li += 1
                drag_models_end = li
            if "heatTransferModels" == split_text[li]:

                heat_transfer_models_start = li+2

                li += 2
                bracket_counter = 1
                while(bracket_counter > 0):
                    if split_text[li] == '{':
                        bracket_counter += 1
                    if split_text[li] == '}':
                        bracket_counter -= 1
                    li += 1
                heat_transfer_models_end = li


        structural_part = split_text[struct_prop_start:struct_prop_end]
        drag_models_part = split_text[drag_models_start:drag_models_end]
        heat_transfer_part = split_text[heat_transfer_models_start:heat_transfer_models_end]

        bracket_counter = 0
        for li in range(len(structural_part)):
            if bracket_counter==0 and structural_part[li]=='{':
                tmp_names = structural_part[li-1].split(':')
                [self.zone_names.append(i.strip("\"")) for i in tmp_names]
                sub_counter = 1
                sub_li = li
                while(sub_counter > 0):
                    sub_li += 1
                    if "volumeFraction" == structural_part[sub_li]:
                        for i in tmp_names:
                            self.gamma[i.strip("\"")]=1.0-float(structural_part[sub_li+1].strip(";"))
                    if "Dh" == structural_part[sub_li]:
                        for i in tmp_names:
                            self.Dh[i.strip("\"")]=float(structural_part[sub_li+1].strip(";"))
                    if "volumetricArea" == structural_part[sub_li]:
                        for i in tmp_names:
                            self.vol_surface[i.strip("\"")]=float(structural_part[sub_li+1].strip(";"))
                    if "T" == structural_part[sub_li]:
                        for i in tmp_names:
                            self.external_T[i.strip("\"")]=float(structural_part[sub_li+1].strip(";"))
                    if "momentumSource" == structural_part[sub_li]:
                        for i in tmp_names:
                            self.momentum_source[i.strip("\"")]=[float(i.replace('(','').replace(')','').replace(';','')) 
                                                       for i in structural_part[sub_li+1:sub_li+4]]
                    elif structural_part[sub_li]=='{':
                        sub_counter += 1
                    elif structural_part[sub_li]=='}':
                        sub_counter -= 1
            if structural_part[li]=='{':
                bracket_counter += 1
            elif structural_part[li]=='}':
                bracket_counter -= 1

        bracket_counter = 0
        for li in range(len(drag_models_part)):
            if bracket_counter==0 and drag_models_part[li]=='{':
                tmp_names = drag_models_part[li-1].split(':')
                sub_counter = 1
                sub_li = li
                while(sub_counter > 0):
                    sub_li += 1
                    if "coeff" == drag_models_part[sub_li]:
                        for i in tmp_names:
                            self.flowres_A[i.strip("\"")]=float(drag_models_part[sub_li+1].strip(";"))
                    elif "exp" == drag_models_part[sub_li]:
                        for i in tmp_names:
                            self.flowres_B[i.strip("\"")]=float(drag_models_part[sub_li+1].strip(";"))
                    elif drag_models_part[sub_li]=='{':
                        sub_counter += 1
                    elif drag_models_part[sub_li]=='}':
                        sub_counter -= 1
            if drag_models_part[li]=='{':
                bracket_counter += 1
            elif drag_models_part[li]=='}':
                bracket_counter -= 1

        bracket_counter = 0
        for li in range(len(heat_transfer_part)):
            if bracket_counter==0 and heat_transfer_part[li]=='{':
                tmp_names = heat_transfer_part[li-1].split(':')
                sub_counter = 1
                sub_li = li
                while(sub_counter > 0):
                    sub_li += 1
                    if "value" == heat_transfer_part[sub_li]:
                        for i in tmp_names:
                            self.heat_exchange_coeff[i.strip("\"")]=float(heat_transfer_part[sub_li+1].strip(";"))
                    elif heat_transfer_part[sub_li]=='{':
                        sub_counter += 1
                    elif heat_transfer_part[sub_li]=='}':
                        sub_counter -= 1
            if heat_transfer_part[li]=='{':
                bracket_counter += 1
            elif heat_transfer_part[li]=='}':
                bracket_counter -= 1

        self.no_zones = len(self.zone_names)

    def write_file(self, path, tag='', verbose=False):

        dictfile = open(self.path)
        dict_text = dictfile.read()
        dictfile.close()

        split_text = dict_text.split("\n")

        struct_prop_start = 0
        struct_prop_end = 0

        drag_models_start = 0
        drag_models_end = 0

        heat_transfer_models_start = 0
        heat_transfer_models_end = 0

        for li in range(len(split_text)):

            split_line = split_text[li].split()

            if "structureProperties" in split_line:

                struct_prop_start = li+2

                li += 2
                bracket_counter = 1
                while(bracket_counter > 0):
                    bracket_counter += split_text[li].count('{')
                    bracket_counter -= split_text[li].count('}')
                    li += 1
                struct_prop_end = li

            if "dragModels" in split_line:

                drag_models_start = li+2

                li += 2
                bracket_counter = 1
                while(bracket_counter > 0):
                    bracket_counter += split_text[li].count('{')
                    bracket_counter -= split_text[li].count('}')
                    li += 1
                drag_models_end = li

            if "heatTransferModels" in split_line:

                heat_transfer_models_start = li+2

                li += 2
                bracket_counter = 1
                while(bracket_counter > 0):
                    bracket_counter += split_text[li].count('{')
                    bracket_counter -= split_text[li].count('}')
                    li += 1
                heat_transfer_models_end = li

        structural_part = split_text[struct_prop_start:struct_prop_end]
        drag_models_part = split_text[drag_models_start:drag_models_end]
        heat_transfer_part = split_text[heat_transfer_models_start:heat_transfer_models_end]

        bracket_counter = 0
        for li in range(len(structural_part)):
            if bracket_counter==0 and '{' in structural_part[li]:
                
                tmp_names = [i.replace("\"",'').replace(' ','') for i in structural_part[li-1].split(':')]
                
                sub_counter = 1
                sub_li = li
                while(sub_counter > 0):
                    sub_li += 1

                    split_line = structural_part[sub_li].split()

                    if "volumeFraction" in split_line:
                        structural_part[sub_li] = structural_part[sub_li][0:structural_part[sub_li].find("volumeFraction")+len("volumeFraction")+1] + \
                              "{:.6e}".format(1.0-self.gamma[tmp_names[0]]) + ';'
                    if "Dh" in split_line:
                        structural_part[sub_li] = structural_part[sub_li][0:structural_part[sub_li].find("Dh")+len("Dh")+1] + \
                              "{:.6e}".format(self.Dh[tmp_names[0]]) + ';'
                    if "volumetricArea" in split_line:
                        structural_part[sub_li] = structural_part[sub_li][0:structural_part[sub_li].find("volumetricArea")+len("volumetricArea")+1] + \
                              "{:.6e}".format(self.vol_surface[tmp_names[0]]) + ';'
                    if "T" in split_line:
                        structural_part[sub_li] = structural_part[sub_li][0:structural_part[sub_li].find("T")+len("T")+1] + \
                              "{:.6e}".format(self.external_T[tmp_names[0]]) + ';'
                    if "momentumSource" in split_line:
                        structural_part[sub_li] = structural_part[sub_li][0:structural_part[sub_li].find("momentumSource")+len("momentumSource")+1] + \
                              "("+" ".join(["{:.6e}".format(i) for i in self.momentum_source[tmp_names[0]]]) + ');'

                    sub_counter += structural_part[sub_li].count('{')
                    sub_counter -= structural_part[sub_li].count('}')

            bracket_counter += structural_part[li].count('{')
            bracket_counter -= structural_part[li].count('}')

        bracket_counter = 0
        for li in range(len(drag_models_part)):
            if bracket_counter==0 and '{' in drag_models_part[li]:

                tmp_names = [i.replace("\"",'').replace(' ','') for i in drag_models_part[li-1].split(':')]

                sub_counter = 1
                sub_li = li
                while(sub_counter > 0):
                    sub_li += 1
                    split_line = drag_models_part[sub_li].split()
                    if "coeff" in  split_line:
                        drag_models_part[sub_li] = drag_models_part[sub_li][0:drag_models_part[sub_li].find("coeff")+len("coeff")+1] + \
                              "{:.6e}".format(self.flowres_A[tmp_names[0]]) + ';'
                    elif "exp" in  split_line:
                        drag_models_part[sub_li] = drag_models_part[sub_li][0:drag_models_part[sub_li].find("exp")+len("exp")+1] + \
                              "{:.6e}".format(self.flowres_B[tmp_names[0]]) + ';'

                    sub_counter += drag_models_part[sub_li].count('{')
                    sub_counter -= drag_models_part[sub_li].count('}')

            bracket_counter += drag_models_part[li].count('{')
            bracket_counter -= drag_models_part[li].count('}')

        bracket_counter = 0
        for li in range(len(heat_transfer_part)):
            if bracket_counter==0 and '{' in heat_transfer_part[li]:

                tmp_names = [i.replace("\"",'').replace(' ','') for i in heat_transfer_part[li-1].split(':')]

                sub_counter = 1
                sub_li = li
                while(sub_counter > 0):
                    sub_li += 1
                    split_line = heat_transfer_part[sub_li].split()
                    if "value" in  split_line:
                        heat_transfer_part[sub_li] = heat_transfer_part[sub_li][0:heat_transfer_part[sub_li].find("value")+len("value")+1] + \
                              "{:.6e}".format(self.heat_exchange_coeff[tmp_names[0]]) + ';'

                    sub_counter += heat_transfer_part[sub_li].count('{')
                    sub_counter -= heat_transfer_part[sub_li].count('}')

            bracket_counter += heat_transfer_part[li].count('{')
            bracket_counter -= heat_transfer_part[li].count('}')

        split_text[struct_prop_start:struct_prop_end] = structural_part    
        split_text[drag_models_start:drag_models_end] = drag_models_part   
        split_text[heat_transfer_models_start:heat_transfer_models_end] = heat_transfer_part 

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'phaseProperties'+tag), 'w')
        of.write(out_text)
        of.close()


    def get_parameter(self, name, zone, entry, tag=''):

        if (zone != 'noZone') and (zone not in self.zone_names):
            raise ValueError("Zone name:  "+zone+" is missing for the preturbation of phaseProperties!")

        if name == "volFraction":
            return self.gamma[zone]
        elif name == 'momentumSource':
            return self.momentum_source[zone][entry]
        elif name == 'heatSource':
            return self.heat_source[zone]
        elif name == 'value':
            return self.heat_exchange_coeff[zone]
        elif name == 'T':
            return self.external_T[zone]
        elif name == 'volSurface':
            return self.vol_surface[zone]
        elif name == 'coeff':
            return self.flowres_A[zone]
        elif name == 'exp':
            return self.flowres_B[zone]
        elif name == 'Dh':
            return self.Dh[zone]
        else:
            raise ValueError("Unknown phase propery to access! "+name)

    def set_parameter(self, name, zone, entry, value, tag=''):

        if (zone != 'noZone') and (zone not in self.zone_names):
            raise ValueError("Zone name:  "+zone+" is missing for the preturbation of phaseProperties!")

        if name == "volFraction":
            self.gamma[zone] = value
        elif name == 'momentumSource':
            self.momentum_source[zone][entry] = value
        elif name == 'heatSource':
            self.heat_source[zone] = value
        elif name == 'value':
            self.heat_exchange_coeff[zone] = value
        elif name == 'T':
            self.external_T[zone] = value
        elif name == 'volSurface':
            self.vol_surface[zone] = value
        elif name == 'coeff':
            self.flowres_A[zone] = value
        elif name == 'exp':
            self.flowres_B[zone] = value
        elif name == 'Dh':
            self.Dh[zone] = value
        else:
            raise ValueError("Unknown phase propery to access! "+name)