class BoundaryDictionary:

    def __init__(self, path):

        self.path = path

        self.bd_names = []

        self.no_bds = None

    def read_dictionary(self, verbose=False):

        if verbose:
            print("Reading boundary disctionary.")

        dictfile = open(self.path)
        dict_text = dictfile.read()
        dictfile.close()

        split_text = dict_text.split()

        for wi in range(len(split_text)):
            if '{' in split_text[wi]:
                if 'FoamFile' not in split_text[wi-1].split('{')[0]:
                    self.bd_names.append(split_text[wi-1].split('{')[0])

        self.no_bds = len(self.bd_names)

