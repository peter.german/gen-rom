import sys
import random
import math
import itertools
import pyDOE
import numpy as np

class Sampling:

    def __init__(self, design_type, n_dims, n_points, duplication=1, seed=17):

        self.sample_matrix = []

        self.design_type = design_type

        self.n_dims = n_dims

        self.n_points = n_points

        self.duplication = duplication

        np.random.seed(seed)

    def compute_design(self):

        if self.design_type == "IHS":
            self.compute_ihs(self.n_dims, self.n_points, self.duplication)
        elif self.design_type == "LHS":
            self.compute_lhs(self.n_dims, self.n_points)
        elif self.design_type == "tensor":
            self.compute_tensor(self.n_dims, self.n_points)
        elif self.design_type == "uniform":
            self.compute_uniform(self.n_dims, self.n_points)        

    def compute_ihs(self, dim_num, point_num, duplication):

        np.random.seed(seed)

        opt = point_num/pow(point_num,(1.0/dim_num))

        x = np.zeros((dim_num, point_num))
        x[:,point_num-1] = np.random.randint(point_num, size=dim_num)

        avail = np.zeros((dim_num, point_num))

        for j in range(0,point_num):
            avail[:,j] = j

        for i in range(0,dim_num):
            avail[i,int(x[i,point_num-1])] = point_num-1

        for count in range(point_num-2, 0, -1):

            cd = (count+1)*duplication

            coll_list = np.zeros(cd)
            point = np.zeros((dim_num,cd))

            for i in range(0, dim_num):
                for k in range(0, duplication):
                    coll_list[k*(count+1):(k+1)*(count+1)-1] = avail[i,0:count];

                pks=np.random.uniform(0,1, size=cd)

                for k in range(cd-1, 0, -1):
                    point_index = int(math.floor(k*pks[k]))
                    point[i,k] = coll_list[point_index]
                    coll_list[point_index] = coll_list[k]

            min_all = sys.float_info.max
            best = 0

            for k in range(0,cd):

                min_can = sys.float_info.max

                for j in range(count+1,point_num):

                    dist = 0.0

                    for i in range(0,dim_num):

                        dist = dist + (point[i,k]-x[i,j])**2

                    dist = math.sqrt(dist)
                    min_can = min(min_can, dist)


                if abs(min_can-opt) < min_all:
                    min_all = abs (min_can-opt)
                    best = k

            x[:,count] = point[:,best]

            for i in range(0,dim_num):
                for j in range(0,point_num):
                    if avail[i,j] == x[i,count]:
                        avail[i,j] = avail[i,count]

        x[:,0] = avail[:,0]

        x = (x+0.5)/point_num

        self.sample_matrix = np.transpose(x)

    def compute_lhs(self, dim_num, point_num):

        self.sample_matrix = pyDOE.lhs(dim_num, samples=point_num, criterion="c")

    def compute_tensor(self, dim_num, point_num):

        if math.isclose(pow(point_num, 1/dim_num), round(pow(point_num, 1/dim_num))):

            pointperdim = int(round(pow(point_num, 1/dim_num)))

            singlevector = np.linspace(0, 1, pointperdim)

            x = np.zeros((dim_num, point_num))

            perm = list(itertools.product(singlevector, repeat = dim_num))

            for permElem in range(len(perm)):
                x[:,permElem] = perm[permElem]

            self.sample_matrix = np.transpose(x)

        else:
            raise ValueError("Check your input, the number of points is not" 
                             "an exponent of the number of dimensions!")

    def compute_uniform(self, dim_num, point_num):

        self.sample_matrix = np.random.uniform(low=0.0, high=1.0, size=(point_num, dim_num))



