import sys
import os
import subprocess
import copy
import shutil
from operator import itemgetter
from pathlib import Path

class Production:

    def __init__(self, perturbator, res_src, dat_filename, command,
                 fields_to_save, save_to, verbose=False):

        self.perturbator = perturbator

        self.no_samples = perturbator.ns

        self.res_src = res_src

        self.dat_filename = dat_filename

        self.command = command

        self.fields_to_save = fields_to_save

        self.save_to = save_to

        self.verbose = verbose

    def create_snapshots(self):

        transient = False
        if len(self.res_src) > 1:
            transient = True

        for sample_i in range(self.no_samples):

            if (self.verbose):
                print("#############################################################")
                print(" Running sample: " + str(sample_i))
                print("#############################################################")

            self.write_current_parameters(sample_i)

            self.copy_input_files(sample_i)

            self.run_simulation()

            self.copy_results(sample_i)

        self.restore_original()

    def copy_input_files(self, sample_i):

        for ifn in self.perturbator.original_db:

            filename = ifn + "_"+str(sample_i)

            for path in Path(self.perturbator.save_path).rglob(filename):

                cpfrom = path
                cpto = self.perturbator.original_db[ifn].path
                shutil.copyfile(cpfrom, cpto)

                if(ifn == 'nuclearData'):
                    cpfromfueltemp = str(path).replace('nuclearData','nuclearDataFuelTemp')
                    cpfromrhocool  = str(path).replace('nuclearData','nuclearDataRhoCool')

                    cptofueltemp = self.perturbator.original_db[ifn].pathfueltemp
                    cptorhocool  = self.perturbator.original_db[ifn].pathrhocool

                    shutil.copyfile(cpfromfueltemp, cptofueltemp)
                    shutil.copyfile(cpfromrhocool, cptorhocool)

    def copy_results(self, sample_i):

        abs_i = sample_i*len(self.res_src)

        for time_dir in self.res_src:

            temp_dir = os.path.join(os.getcwd(), time_dir)

            for root, dirs, files in os.walk(temp_dir):
                for name in files:

                    if os.path.splitext(name)[0] in self.fields_to_save:

                        ind = self.fields_to_save.index(os.path.splitext(name)[0])

                        rel_dir = os.path.relpath(root, temp_dir)

                        dir_name = os.path.join(os.getcwd(), self.save_to, str(abs_i), rel_dir)
                        if not os.path.exists(dir_name):
                            os.makedirs(dir_name)

                        shutil.copyfile(os.path.join(root, name),
                                        os.path.join(dir_name, os.path.splitext(name)[0]))

            abs_i += 1

        dat_dir_name = os.path.join(os.getcwd(), "ROMFiles", "datFiles", str(sample_i))
        if not os.path.exists(dat_dir_name):
            os.makedirs(dat_dir_name)
        if self.dat_filename != None:
            shutil.copyfile(os.path.join(os.getcwd(),self.dat_filename),
                            os.path.join(dat_dir_name, self.dat_filename.split("/")[-1]))

    def restore_original(self):

        for inpf in self.perturbator.original_db:

            self.perturbator.original_db[inpf].write_file(
                    self.perturbator.original_db[inpf].path.replace(inpf,''))

    def write_current_parameters(self, sample_i):

        tagged_indices = self.perturbator.tagged_indices

        tagged_params = self.perturbator.real_parameters[sample_i][tagged_indices]

        temp_dir = os.path.join(os.getcwd(), 'ROMFiles/currentParameters')
        out_file = open(temp_dir, 'w')

        out_file.write(str(len(tagged_params))+" ")
        out_file.write("("+" ".join(["{:.6e}".format(i) for i in tagged_params])+")\n")
        out_file.close()

    def run_simulation(self):

        command_run = subprocess.call(self.command)

        if command_run == 1:
            sys.exit()
