import os

class ROMDictionary:

    def __init__(self, path):

        self.path = path

        self.pump_times = []

        self.pump_mags = []

    def read_dictionary(self, verbose=False):

        if verbose:
            print("Reading ROM dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split('\n')

        for li in range(len(split_text)):

            split_line = split_text[li].split()

            if "pumpMags" in split_line:
                bs = split_text[li].index('(')
                be = split_text[li].index(')')
                self.pump_mags = [float(i) for i in split_text[li][bs+1:be].split(' ')]
            elif "pumpTimes" in split_line:
                bs = split_text[li].index('(')
                be = split_text[li].index(')')
                self.pump_times = [float(i) for i in split_text[li][bs+1:be].split(' ')]

    def write_file(self, path, tag='', verbose=False):

        if verbose:
            print("Writing ROM dictionary.")

        dictfile = open(self.path)
        dict_raw_text = dictfile.read()
        dictfile.close()

        split_text = dict_raw_text.split("\n")

        for li in range(len(split_text)):

            split_line = split_text[li].split()

            if "pumpMags" in split_line:
                mags = ''
                for i in self.pump_mags:
                    mags = mags+"{:.6e}".format(i)+' '
                split_text[li] = split_text[li][0:split_text[li].find("pumpMags")+9] + \
                              '(' + mags + ');'
            elif "pumpTimes" in split_line:
                times = ''
                for i in self.pump_times:
                    times = times+"{:.6e}".format(i)+' '
                split_text[li] = split_text[li][0:split_text[li].find("pumpTimes")+10] + \
                              '(' + times + ');'

        out_text = "\n".join(split_text)
        
        of = open(os.path.join(path,'ROMDict'+tag), 'w')
        of.write(out_text)
        of.close()

    def get_parameter(self, name, zone, entry, tag=''):

        if name == "pumpMags":
             return self.pump_mags[entry]
        elif name == 'pumpTimes':
             return self.pump_times[entry]
        else:
            raise ValueError("Unknown ROM parameter to access!")


    def set_parameter(self, name, zone, entry, value, tag=''):

        if name == "pumpMags":
             self.pump_mags[entry] = value 
        elif name == 'pumpTimes':
             self.pump_times[entry] = value
        else:
            raise ValueError("Unknown ROM parameter to access!")


        





