#!/usr/bin/python

import os
import math
import argparse
import importlib.util

class InputHandler:

    def __init__(self, filename):

        self.filename = filename

        self.result_folder = []

        self.results_to_print = []

        self.input_files = []

        self.parameters = {}

        self.tagged_parameters = {}

        self.tagged_indices = []

        self.no_parameters = 0

    def read_input_file(self):

        self.parameters = {}
        self.no_parameters = 0
        self.input_files = []

        loader = importlib.machinery.SourceFileLoader('input', self.filename)
        spec = importlib.util.spec_from_loader(loader.name, loader)
        mod = importlib.util.module_from_spec(spec)
        loader.exec_module(mod)

        self.parameters = mod.pertIntervals

        try:
            self.tagged_parameters = mod.taggedParameters
        except:
            self.tagged_parameters = mod.pertIntervals

        self.result_folder = mod.resultDir

        resultDir = []
        for item in self.result_folder:
            if math.isclose(item, round(item)):
                resultDir.append(str(int(round(item))))
            else:
                resultDir.append(str(round(item,4)))
        self.result_folder = resultDir

        print(resultDir)

        self.results_to_print = mod.resultsToPrint

        for inpf in mod.pertIntervals:

            self.input_files.append(inpf)

            for zone in mod.pertIntervals[inpf]:

                for parameter in mod.pertIntervals[inpf][zone]:

                    is_scalar = isinstance(mod.pertIntervals[inpf][zone][parameter][0], float)

                    if not is_scalar:

                        for entry in mod.pertIntervals[inpf][zone][parameter]:

                            self.no_parameters += 1

                    else:

                        self.no_parameters += 1
