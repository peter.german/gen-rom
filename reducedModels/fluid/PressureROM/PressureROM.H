/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    PressureROM

Description
    Derived class handling the Reduced Order Models of pressure equations.

Author
    Peter German, peter.german@tamu.edu, grmnptr@gmail.com

SourceFiles
    PressureROM.C

\*---------------------------------------------------------------------------*/

#ifndef PressureROM_H
#define PressureROM_H

#include "volFields.H"
#include "RectangularMatrix.H"
#include "fvCFD.H"
#include "fvMatrixExt.H"
#include "ReducedModel.H"

#include "FlowResDEIM.H"

#include "FluidPropertiesDataBase.H"
#include "PorousMediumDataBase.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class PressureROM Declaration
\*---------------------------------------------------------------------------*/


class PressureROM : public ReducedModel<scalar>
{

private:

    //- Private data

        PtrList<volVectorField> gradOwnBase_;

        PtrList<volVectorField>* UBase_;

        List<scalar>* URedSol_;

        RectangularMatrix<scalar> redDivMatrix_;

        //- reference to the fluid properties
        const FluidPropertiesDataBase& fluidDB_;

        //- reference to the porous meidum properties
        const PorousMediumDataBase& porousDB_;

    // Private Member Functions

public:

    // Constructors

        PressureROM
        (
            word name, 
            fvMesh& inpMesh,
            Time& runTime,
            const FluidPropertiesDataBase& fluidDB,
            const PorousMediumDataBase& porousDB
        );

    // Destructor

        ~PressureROM();

    // Public Member Functions

        //- Computing the gradient of the basis vectors
        void computeGradBase();

        //- Creating the reduced Laplacian matrix
        void createDivergenceMx();

        //- Updating the cross solutions
        void updateVelocitySolution(List<scalar>* URedSol)
        {
            URedSol_ = URedSol;
        }

        //- Updating the cross bases
        void updateVelocityBase(PtrList<volVectorField>& UBases)
        {
            UBase_ = &UBases;
        }

        RectangularMatrix<scalar>& divMatrix()
        {
            return(redDivMatrix_);
        }

        void writeDivMatrix();

        void readDivMatrix();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
