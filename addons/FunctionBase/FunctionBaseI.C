/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     | Version:     3.2
    \\  /    A nd           | Web:         http://www.foam-extend.org
     \\/     M anipulation  | For copyright notice see file Copyright
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    FunctionBase

Description
     An abstract class of a generic function. Only used for 
    inharitance purposes.

\*---------------------------------------------------------------------------*/

template class Foam::FunctionBase<scalar, scalar>;

template class Foam::FunctionBase<vector, scalar>;

template class Foam::FunctionBase<scalar, vector>;

template class Foam::FunctionBase<vector, vector>;

template class Foam::FunctionBase<List<scalar>, scalar>;

template class Foam::FunctionBase<List<vector>, scalar>;

// ************************************************************************* //
