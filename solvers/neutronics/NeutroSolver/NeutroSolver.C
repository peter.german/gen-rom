/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    NeutroSolver

Description
    Abstract base class for neutronics solvers.

\*---------------------------------------------------------------------------*/

#include "NeutroSolver.H"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

//- Construct using all of the matrices
Foam::NeutroSolver::NeutroSolver
(
    fvMesh& neutroMesh,
    fvMesh& fluidMesh,
    Time& runTime,
    IOdictionary& ROMDict,
    bool fuelTemperatureFeedback,
    bool rhoCoolFeedback
)
    :
    neutroMesh_(neutroMesh),
    fluidMesh_(fluidMesh),
    runTime_(runTime),
    ROMDict_(ROMDict),
    residual_(1.0),
    residualOld_(1.0),
    xsDataBase_(neutroMesh_),
    reactorState_(neutroMesh_),
    liquidFuel_(runTime_.controlDict().lookupOrDefault("liquidFuel", false)),
    fuelTemperatureFeedback_(fuelTemperatureFeedback),
    rhoCoolFeedback_(rhoCoolFeedback)
{
    fileOutput_.reset
    (
        new OFstream(runTime.path()+"/GeN-ROM.dat")
    );
    fileOutput_() << "time(s);";
    fileOutput_() << "keff(-);";
    fileOutput_() << "power(W);";
    fileOutput_() << endl;

    Dalbedo.reset
    (
        new volScalarField
        (
            IOobject
            (
                "Dalbedo",
                runTime_.timeName(),
                neutroMesh_,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            neutroMesh_,
            dimensionedScalar("", dimensionSet(0,1,0,0,0,0,0), 1.0),
            zeroGradientFvPatchScalarField::typeName
        )
    );

    fluxStarAlbedo.reset
    (
        new volScalarField
        (
            IOobject
            (
                "fluxStarAlbedo",
                runTime_.timeName(),
                neutroMesh_,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            neutroMesh_,
            dimensionedScalar("", dimensionSet(0,-2,-1,0,0,0,0), 0.0),
            zeroGradientFvPatchScalarField::typeName
        )
    );

    tolerance_ = ROMDict_.lookupOrDefault<scalar>("neutroTolerance", 1e-8);


}

// * * * * * * * * * * * * * * * * Destructors * * * * * * * * * * * * * * * //

Foam::NeutroSolver::~NeutroSolver()
{}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * Public Member Functions  * * * * * * * * * * * //

void Foam::NeutroSolver::createMapping()
{
    mapping_.reset
    (
        new meshToMesh
        (
            neutroMesh_,
            fluidMesh_,
            Foam::meshToMesh::interpolationMethod(2),
            Foam::meshToMesh::procMapMethod::pmAABB,
            false
        )
    );
}

void Foam::NeutroSolver::readInputData()
{
    Info << "############################################################" << endl;
    Info << "Reading inpud Data for ROM." << endl;
    Info << "############################################################" << endl;

    Info << "- Reading reactor state." << endl;
    reactorState_.readReactorState();

    Info << "- Reading nominal cross sections." << endl;
    xsDataBase_.readCrossSections();

    Info << "- Reading cross sections with fuel temperature." << endl;
    xsDataBase_.readCrossSectionsFuelTemp();

    Info << "- Reading cross sections with coolant density." << endl;
    xsDataBase_.readCrossSectionsRhoCool();

    Info << "- Reading fluid properties. " << endl;
    fluidPropertiesDataBase_.readThermoPhysicalProperties();

    Info << "- Reading porous medium properties. " << endl;
    porousMediumDataBase_.readPorousMediumProperties();
}

void Foam::NeutroSolver::createReducedObjects()
{
    Info << "############################################################" << endl;
    Info << "Creating ROM Objects for eigenvalue neutronics equations." << endl;
    Info << "############################################################" << endl;
    Info << "Creating flux ROM objects. ";

    label noEnergyGroups = xsDataBase_.noEnergyGroups();

    fluxROM_.clear();
    fluxROM_.resize(noEnergyGroups);
    forAll(fluxROM_, groupInd)
    {
        fluxROM_.set
        (
            groupInd,
            new NeutROM
            (
                groupInd,
                "fluxStar"+name(groupInd),
                neutroMesh_,
                runTime_,
                xsDB(),
                porousMediumDataBase_,
                fluidPropertiesDataBase_,
                fuelTemperatureFeedback_,
                rhoCoolFeedback_
            )
        );
    }

    Info<< "Done." << endl;

    Info << "Creating precursor ROM objects. ";

    label noPrecGroups = xsDataBase_.noPrecGroups();

    precROM_.clear();
    precROM_.resize(noPrecGroups);
    forAll(precROM_, groupInd)
    {
        precROM_.set
        (
            groupInd,
            new PrecROM
            (
                groupInd,
                "precStar"+name(groupInd),
                neutroMesh_,
                runTime_,
                xsDB(),
                fluidPropertiesDataBase_,
                porousMediumDataBase_,
                liquidFuel_,
                fuelTemperatureFeedback_,
                rhoCoolFeedback_
            )
        );
    }

    Info<< "Done." << endl;
}

void Foam::NeutroSolver::linkReducedObjects()
{
    Info << "############################################################" << endl;
    Info << "Linking ROM objects." << endl;
    Info << "############################################################" << endl;

    Info << "Linking flux ROMs:" << endl;

    forAll(fluxROM_, gIndex1)
    {
        List<List<scalar>* > crossFluxSols(0);
        List<PtrList<volScalarField>* > crossFluxBases(0);

        List<List<scalar>* > precSols(0);
        List<PtrList<volScalarField>* > precBases(0);

        Info << "  Flux, energy group " << gIndex1+1 << "/" << fluxROM_.size() <<endl;

        forAll(fluxROM_, gIndex2)
        {
            if (gIndex1 != gIndex2)
            {
                crossFluxSols.append(fluxROM_[gIndex2].solution());
                crossFluxBases.append(&(fluxROM_[gIndex2].ownBase()));
            }
        }

        forAll(precROM_, precIndex)
        {
            precSols.append(precROM_[precIndex].solution());
            precBases.append(&(precROM_[precIndex].ownBase()));
        }

        fluxROM_[gIndex1].updateCrossFluxBases(crossFluxBases);
        fluxROM_[gIndex1].updateCrossFluxSolutions(crossFluxSols);

        fluxROM_[gIndex1].updatePrecBases(precBases);
        fluxROM_[gIndex1].updatePrecSolutions(precSols);
    }

    List<List<scalar>* > crossFluxSols(0);
    List<PtrList<volScalarField>* > crossFluxBases(0);

    forAll(fluxROM_, gIndex)
    {
        crossFluxSols.append(fluxROM_[gIndex].solution());
        crossFluxBases.append(&(fluxROM_[gIndex].ownBase()));
    }

    forAll(precROM_, gIndex)
    {

        Info << "  Precursor group " << gIndex+1 << "/" << precROM_.size();

        precROM_[gIndex].updateCrossFluxSolutions(crossFluxSols);
        precROM_[gIndex].updateCrossFluxBases(crossFluxBases);

        Info << " Done." << endl;
    }
}

void Foam::NeutroSolver::createReducedBases()
{
    Info << "############################################################" << endl;
    Info << "Creating Reduced Bases for neutron flux / precursor equations." << endl;
    Info << "############################################################" << endl;

    label noSnapshots = ROMDict_.lookupOrDefault("noSnapshots", 1);

    label baseSelectionStrategy = ROMDict_.lookupOrDefault("baseSelectionStrategy", 1);

    scalar enLimit = ROMDict_.lookupOrDefault("enLimit", 1.0);

    bool adjoint(false);
    if (fluxROM_[0].ROMName() == "adjointFluxStar0")
    {
        adjoint = true;
    }

    word fluxBaseName = "numberOfFluxBases";
    word precBaseName = "numberOfPrecBases";
    if(adjoint)
    {
        fluxBaseName = "numberOfAdjointFluxBases";
        precBaseName = "numberOfAdjointPrecBases";
    }

    const List<label> numberOfFluxBases = ROMDict_.lookupOrDefault
    (
        fluxBaseName,
        List<label>(fluxROM_.size(), 1)
    );

    const List<label> numberOfPrecBases = ROMDict_.lookupOrDefault
    (
        precBaseName,
        List<label>(precROM_.size(), 1)
    );

    forAll(fluxROM_, gIndex)
    {
        fluxROM_[gIndex].readSnapshots(noSnapshots);
        fluxROM_[gIndex].createBases
        (
            baseSelectionStrategy,
            enLimit,
            numberOfFluxBases[gIndex]
        );

        fluxROM_[gIndex].saveBases();
        fluxROM_[gIndex].saveEigenValues();

        fluxROM_[gIndex].initSolVectorsL2Projection();

        fluxROM_[gIndex].initApproximateSolution();
    }

    forAll(precROM_, gIndex)
    {
        precROM_[gIndex].readSnapshots(noSnapshots);
        precROM_[gIndex].createBases
        (
            baseSelectionStrategy,
            enLimit,
            numberOfPrecBases[gIndex]
        );

        precROM_[gIndex].saveBases();
        precROM_[gIndex].saveEigenValues();

        precROM_[gIndex].initSolVectorsL2Projection();

        precROM_[gIndex].initApproximateSolution();

    }

    Info<< "Done." << endl << endl;
}

void Foam::NeutroSolver::readReducedBases()
{
    Info << "############################################################" << endl;
    Info << "Reading Reduced Bases for neutron flux/precursor equations." << endl;
    Info << "############################################################" << endl;

    bool adjoint(false);
    if (fluxROM_[0].ROMName() == "adjointFluxStar0")
    {
        adjoint = true;
    }

    word fluxBaseName = "numberOfFluxBases";
    word precBaseName = "numberOfPrecBases";
    if(adjoint)
    {
        fluxBaseName = "numberOfAdjointFluxBases";
        precBaseName = "numberOfAdjointPrecBases";
    }

    const List<label> numberOfFluxBases = ROMDict_.lookupOrDefault
    (
        fluxBaseName,
        List<label>(fluxROM_.size(), 1)
    );

    const List<label> numberOfPrecBases = ROMDict_.lookupOrDefault
    (
        precBaseName,
        List<label>(precROM_.size(), 1)
    );

    forAll(fluxROM_, gIndex)
    {
        fluxROM_[gIndex].readBases(numberOfFluxBases[gIndex]);

        fluxROM_[gIndex].initSolVectorsL2Projection();

        fluxROM_[gIndex].initApproximateSolution();
    }

    forAll(precROM_, gIndex)
    {
        precROM_[gIndex].readBases(numberOfPrecBases[gIndex]);

        precROM_[gIndex].initSolVectorsL2Projection();

        precROM_[gIndex].initApproximateSolution();
    }

    Info<< "Done." << endl << endl;
}

void Foam::NeutroSolver::createReducedOperators()
{
    Info << "############################################################" << endl;
    Info << "Creating and projecting neutronics ROM matrices." << endl;
    Info << "############################################################" << endl;

    forAll(fluxROM_, groupI)
    {
        Info << " - Neutron energy group: " << groupI+1 << endl;

        Info << " - - Reduced stiffness matrices. ";
        fluxROM_[groupI].createLaplTensor();
        Info << "Done." << endl;

        Info << " - - Reduced mass matrices. ";
        fluxROM_[groupI].createMassTensor();
        Info << "Done." << endl;

        Info << " - - Reduced cross mass matrices. ";
        fluxROM_[groupI].createCrossMassTensor();
        Info << "Done." << endl;

        Info << " - - Boundary terms. ";
        fluxROM_[groupI].createBoundaryTerms();
        Info << "Done." << endl;

        Info << " - - Integral contributions. ";
        fluxROM_[groupI].createIntContrib();
        Info << "Done." << endl;

        Info << " - - Precursor mass matrices. ";
        fluxROM_[groupI].createPrecTensor();
        Info << "Done." << endl;
    }

    forAll(precROM_, groupI)
    {
        Info << " - Precursor group: " << groupI+1 << endl;

        Info << " - - Reduced mass matrices. ";
        precROM_[groupI].createMassTensor();
        Info << "Done." << endl;

        Info << " - - Reduced cross mass matrices. ";
        precROM_[groupI].createFluxTensor();
        Info << "Done." << endl;
    }

    if(liquidFuel_)
    {
        volVectorField U
        (
            IOobject
            (
                "UDarcy",
                runTime_.timeName(),
                fluidMesh_,
                IOobject::READ_IF_PRESENT,
                IOobject::NO_WRITE
            ),
            fluidMesh_,
            dimensionedVector("", dimensionSet(0,1,-1,0,0,0,0), vector(0.0,0.0,0.0)),
            zeroGradientFvPatchVectorField::typeName
        );

        volScalarField alphat
        (
            IOobject
            (
                "alphat",
                runTime_.timeName(),
                fluidMesh_,
                IOobject::READ_IF_PRESENT,
                IOobject::NO_WRITE
            ),
            fluidMesh_,
            dimensionedScalar("", dimensionSet(0,2,-1,0,0,0,0), 0.0),
            zeroGradientFvPatchScalarField::typeName
        );

        volVectorField UNeutro
        (
            IOobject
            (
                "UNeutro",
                runTime_.timeName(),
                neutroMesh_,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            neutroMesh_,
            dimensionedVector("", dimensionSet(0,1,-1,0,0,0,0), vector(0.0,0.0,0.0)),
            zeroGradientFvPatchVectorField::typeName
        );

        volScalarField alphatNeutro
        (
            IOobject
            (
                "alphatNeutro",
                runTime_.timeName(),
                neutroMesh_,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            neutroMesh_,
            dimensionedScalar("", dimensionSet(0,2,-1,0,0,0,0), 0.0),
            zeroGradientFvPatchScalarField::typeName
        );

        createMapping();

        mapping_().mapTgtToSrc
        (
            alphat,
            plusEqOp<scalar>(),
            alphatNeutro.primitiveFieldRef()
        );
        alphatNeutro.correctBoundaryConditions();

        mapping_().mapTgtToSrc
        (
            U,
            plusEqOp<vector>(),
            UNeutro.primitiveFieldRef()
        );
        UNeutro.correctBoundaryConditions();

        forAll(precROM_, groupI)
        {
            Info << " - - Reduced advection matrix. ";
            precROM_[groupI].createAdvectionMatrix(UNeutro);
            Info << "Done." << endl;

            Info << " - - Reduced advection matrix BD internal. ";
            precROM_[groupI].createAdvectionBDInternalTerms(UNeutro);
            Info << "Done." << endl;

            Info << " - - Reduced laminar diffusion tensor. ";
            precROM_[groupI].createLaminarDiffusionTensor();
            Info << "Done." << endl;

            Info << " - - Reduced trubulent diffusion matrix. ";
            precROM_[groupI].createTurbulentDiffusionMatrix(alphatNeutro);
            Info << "Done." << endl;
        }
    }
}

void Foam::NeutroSolver::readReducedOperators()
{
    Info << "############################################################" << endl;
    Info << "Creating and projecting neutronics ROM matrices." << endl;
    Info << "############################################################" << endl;

    forAll(fluxROM_, groupI)
    {
        Info << " - Neutron energy group: " << groupI+1 << endl;

        Info << " - - Reduced stiffness matrices. ";
        fluxROM_[groupI].readLaplTensor();
        Info << "Done." << endl;

        Info << " - - Reduced mass matrices. ";
        fluxROM_[groupI].readMassTensor();
        Info << "Done." << endl;

        Info << " - - Reduced cross mass matrices. ";
        fluxROM_[groupI].readCrossMassTensor();
        Info << "Done." << endl;

        Info << " - - Boundary terms. ";
        fluxROM_[groupI].readBoundaryTerms();
        Info << "Done." << endl;

        Info << " - - Integral contributions. ";
        fluxROM_[groupI].readIntContrib();
        Info << "Done." << endl;

        Info << " - - Precursor mass matrices. ";
        fluxROM_[groupI].readPrecTensor();
        Info << "Done." << endl;
    }

    forAll(precROM_, groupI)
    {
        Info << " - Precursor group: " << groupI+1 << endl;

        Info << " - - Reduced mass matrices. ";
        precROM_[groupI].readMassTensor();
        Info << "Done." << endl;

        Info << " - - Reduced cross mass matrices. ";
        precROM_[groupI].readFluxTensor();
        Info << "Done." << endl;

        if(liquidFuel_)
        {
            Info << " - - Reduced advection matrix. ";
            precROM_[groupI].readAdvectionMatrix();
            Info << "Done." << endl;

            Info << " - - Reduced advection matrix BD internal. ";
            precROM_[groupI].readAdvectionBDInternalTerms();
            Info << "Done." << endl;

            Info << " - - Reduced laminar diffusion tensor. ";
            precROM_[groupI].readLaminarDiffusionTensor();
            Info << "Done." << endl;

            Info << " - - Reduced turbulent diffusion matrix. ";
            precROM_[groupI].readTurbulentDiffusionMatrix();
            Info << "Done." << endl;
        }
    }
}

void Foam::NeutroSolver::timeUpdateAndReconstruct()
{
    forAll(fluxROM_, gIndex)
    {
        fluxROM_[gIndex].updateOldRedSol();
    }

    forAll(precROM_, gIndex)
    {
        precROM_[gIndex].updateOldRedSol();
    }

    if(fileOutput_.valid())
    {
        fileOutput_().precision(10);
        fileOutput_() << runTime_.time().value() << ";";

        fileOutput_().precision(8);
        fileOutput_() << keff_ << ";";

        fileOutput_().precision(8);
        fileOutput_() << powTot_ << ";";
        fileOutput_() << endl;
    }

    if (runTime_.outputTime())
    {
        forAll(fluxROM_, gIndex)
        {
            fluxROM_[gIndex].reconstructApproxSol();
        }

        forAll(precROM_, gIndex)
        {
            precROM_[gIndex].reconstructApproxSol();
        }
    }

    powTotOld_ = powTot_;
}



// ************************************************************************* //
