/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    BeffSolver

Description
    Implementation of a forward and adjoint neutronics solve to determine the 
    effective delayed neutron fraction.

Author
    Peter German, peter.german@tamu.edu, grmnptr@gmail.com

SourceFiles
    BeffSolver.C

\*---------------------------------------------------------------------------*/

#ifndef BeffSolver_H
#define BeffSolver_H

#include "fvCFD.H"
#include "RectangularMatrix.H"

#include "DiffusionEv.H"
#include "AdjointDiffusionEv.H"
#include "BeffHandler.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class Reduced Model Declaration
\*---------------------------------------------------------------------------*/

class BeffSolver : public Solver
{

private:

    // Private Attributes

    //- Reference to the nutronics mesh
    fvMesh& neutroMesh_;

    //- Reference to the nutronics mesh
    fvMesh& fluidMesh_;

    //- Reference to the runtime control (easier synchronisation with FOM)
    Time& runTime_;
    
    //- Dictionary holding necessary information about the ROM run settings
    IOdictionary& ROMDict_;

    //- Forward neutronics solver
    autoPtr<DiffusionEv> forwardEv_;

    //- Adjoint neutronics solver
    autoPtr<AdjointDiffusionEv> adjointEv_;

    //- Handler used to generate Beff
    autoPtr<BeffHandler> beffHandler_;

    // Private functions

public:

    // Constructors

        BeffSolver
        (
            fvMesh& neutroMesh,
            fvMesh& fluidMesh,
            Time& runTime,
            IOdictionary& ROMDict
        );

    // Destructor

        ~BeffSolver();

    // Public Member Functions

        //- Reading input parameters
        virtual void readInputData() override;

        //- Creating reduced objects
        virtual void createReducedObjects() override;

        //- Linking reduced objects
        virtual void linkReducedObjects() override;

        //- Creating the bases in the reduced bases
        virtual void createReducedBases() override;

        //- Reading the bases in the reduced bases
        virtual void readReducedBases() override;

        //- Creating the reduced operators
        virtual void createReducedOperators() override;

        //- Reading the reduced operators
        virtual void readReducedOperators() override;

        //- stepping solution and reconstructing approximate fields
        virtual void timeUpdateAndReconstruct() override;

        //- Solving the nonlinear system (Picard iteration)
        virtual scalar solve() override;

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
