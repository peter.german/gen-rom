/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    TwoEqLamSteadyFluidROMSolver

Description
    Implementation of a two-equation steady-state laminar fluid dynamics solver.

Author
    Peter German, peter.german@tamu.edu, grmnptr@gmail.com

SourceFiles
    TwoEqLamSteadyFluidROMSolver.C

\*---------------------------------------------------------------------------*/

#ifndef TwoEqLamSteadyFluidROMSolver_H
#define TwoEqLamSteadyFluidROMSolver_H

#include "fvCFD.H"
#include "RectangularMatrix.H"

#include "FluidSolver.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class Reduced Model Declaration
\*---------------------------------------------------------------------------*/


class TwoEqLamSteadyFluidROMSolver : public FluidSolver
{

protected:

    // Private Attributes
    
        //- Number of pressure coefficients
        label nPCoeffs_; 
    
        //- Number of velocity coefficients
        label nUCoeffs_;
    
        //- Number of all coefficients together
        label sumCoeffs_;

    // Private functions

        //- Filling the momentum equation related part of the system
        //- matrix and right hand side
        virtual void fillUPart();
    
        //- Filling the "pressure" equation related part of the system
        //- matrix and right hand side. SUP approach
        virtual void fillPPart();

        //- Filling up combined solution vector
        virtual void fillCombinedSolution() override;

        //- Setting up the system matrix in the iteration
        virtual void setupNonlinearSystem();

        //- Updating the solutions in PROM and UROM 
        virtual void updateComponentSolution();

public:

    // Constructors

        TwoEqLamSteadyFluidROMSolver
        (
            Time& runTime,
            IOdictionary& ROMDict,
            fvMesh& fluidMesh
        );

    // Destructor

        ~TwoEqLamSteadyFluidROMSolver();

    // Public Member Functions

        //- Linking reduced objects (allow them to see each other)
        virtual void linkReducedObjects() override;

        //- Creating the bases in the reduced bases
        virtual void createReducedBases() override;

        //- Redaing the bases from files
        virtual void readReducedBases() override;

        //- Creating the reduced operators
        virtual void createReducedOperators() override;

        //- Reading the reduced operators
        virtual void readReducedOperators() override;

        //- Solving the nonlinear system (Picard iteration)
        virtual scalar solve() override;

        //- Initializing system matrix and right hand side
        virtual void initNonlinearSystem();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
