/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    OneEqLamSteadyFluidROMSolver

Description
    Implementation of a one-equation steady-state laminar fluid dynamics solver.

\*---------------------------------------------------------------------------*/

#include "OneEqLamSteadyFluidROMSolver.H"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

//- Construct using all of the matrices
Foam::OneEqLamSteadyFluidROMSolver::OneEqLamSteadyFluidROMSolver
(
    Time& runTime,
    IOdictionary& ROMDict,
    fvMesh& fluidMesh
)
    :
    FluidSolver(runTime, ROMDict, fluidMesh)
{}

// * * * * * * * * * * * * * * * * Destructors * * * * * * * * * * * * * * * //

Foam::OneEqLamSteadyFluidROMSolver::~OneEqLamSteadyFluidROMSolver()
{}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

void Foam::OneEqLamSteadyFluidROMSolver::fillUPart()
{
    PtrList<FlowResDEIM>& flowResDEIM(mROM_().flowResDEIM());

    forAll(flowResDEIM, zone)
    {
        flowResDEIM[zone].calcCoeffs();
        flowResDEIM[zone].sumVectors();
    }

    mROM_().sumConvTensor();

    mROM_().sumContErrTensor();

    RectangularMatrix<scalar>& convMatrix(mROM_().convMatrix());

    RectangularMatrix<scalar>& contErrMatrix(mROM_().contErrMatrix());

    RectangularMatrix<scalar>& diffMatrix(mROM_().diffMatrix());

    RectangularMatrix<scalar>& devMatrix(mROM_().devMatrix());

    RectangularMatrix<scalar>& pressGradMatrix(mROM_().pressGradMatrix());

    List<RectangularMatrix<scalar> >& penaltyMatrices(mROM_().penaltyMatrices());

    List<List<scalar> >& penaltyVectors(mROM_().penaltyVectors());

    List<List<scalar> >& pumpSrc(mROM_().pumpSrc());

    List<scalar>& buoyancySrc(mROM_().boussinesqSrc());

    const scalar& mu = fluidPropertiesDataBase_.mu();

    const scalar& rho = fluidPropertiesDataBase_.rho();

    const scalar& beta = fluidPropertiesDataBase_.beta();

    const scalar magG = mag(fluidPropertiesDataBase_.g());

    const PtrList<scalar>& pumpVecMags =
                            porousMediumDataBase_.pumpMomentumSourceMags();

    /*
    Info << "adv mx " << rho*convMatrix << endl;

    Info << "cont mx " << -rho*contErrMatrix << endl;

    Info << "dev mx " << -mu*devMatrix << endl;

    Info << "diff mx " << -mu*diffMatrix << endl;

    Info << "press grad " << pressGradMatrix << endl;

    forAll(pumpSrc, zone)
    {
        Info << "pump src" << pumpVecMags[zone]*pumpSrc[zone] << endl;
    }

    forAll(flowResDEIM, zone)
    {
        Info << "flow red" << flowResDEIM[zone].redFlowRes() << endl;


    }
    Info << rho*magG*beta*buoyancySrc << endl;

    */


    for(label rowInd(0); rowInd < nUCoeffs_; rowInd++)
    {
        for(label colInd(0); colInd < nUCoeffs_; colInd++)
        {
            systemMatrix_[rowInd][colInd] =
                    rho*convMatrix[rowInd][colInd]
                    - rho*contErrMatrix[rowInd][colInd]
                    - mu*devMatrix[rowInd][colInd]
                    - mu*diffMatrix[rowInd][colInd]
                    + pressGradMatrix[rowInd][colInd];

            forAll(velocityBDMags_, bdInd)
            {
                systemMatrix_[rowInd][colInd] +=
                    velocityPenalties_[bdInd]*penaltyMatrices[bdInd][rowInd][colInd];
            }

        }

        systemRHS_[rowInd] = rho*magG*beta*buoyancySrc[rowInd];

        forAll(pumpSrc, zone)
        {
            systemRHS_[rowInd] += pumpVecMags[zone]*pumpSrc[zone][rowInd];
        }

        forAll(flowResDEIM, zone)
        {
            systemRHS_[rowInd] += flowResDEIM[zone].redFlowRes()[rowInd];
        }

        forAll(velocityBDMags_, bdInd)
        {
            systemRHS_[rowInd] +=
                velocityPenalties_[bdInd]*velocityBDMags_[bdInd]*penaltyVectors[bdInd][rowInd];
        }
    }

    // Info << systemRHS_ << endl;
}

void Foam::OneEqLamSteadyFluidROMSolver::setupNonlinearSystem()
{
    Info << "Filling U-related component." << endl;
    fillUPart();
}

void Foam::OneEqLamSteadyFluidROMSolver::fillCombinedSolution()
{
    combinedSolution_ = *(mROM_().solution());
}

void Foam::OneEqLamSteadyFluidROMSolver::updateComponentSolution()
{
    // Info << "old solution " << (*mROM_().solution()) <<endl;
    // Info << "new solution " << combinedSolution_ <<endl;
    forAll(combinedSolution_, coeffIndex)
    {
        scalar oldSolution = (*mROM_().solution())[coeffIndex];
        scalar deltaSolution = (combinedSolution_[coeffIndex] - oldSolution);

        (*mROM_().solution())[coeffIndex] = oldSolution + relaxFactor_*deltaSolution;
        (*pROM_().solution())[coeffIndex] = oldSolution + relaxFactor_*deltaSolution;
    }
    // Info << "updated solution " << (*mROM_().solution()) <<endl;
}

// * * * * * * * * * * * * * Public Member Functions  * * * * * * * * * * * //

void Foam::OneEqLamSteadyFluidROMSolver::linkReducedObjects()
{
    Info << "############################################################" << endl;
    Info << "Linking ROM objects." << endl;
    Info << "############################################################" << endl;

    Info << "Linking momentum ROM. " << endl;

    mROM_().updatePressureBase(pROM_().ownBase());

    Info << "Linking Flow Resistance. " << endl;

    mROM_().linkFlowResistance();
}

void Foam::OneEqLamSteadyFluidROMSolver::createReducedBases()
{
    Info << "############################################################" << endl;
    Info << "Creating Reduced Bases for momentum/pressure PPE equations." << endl;
    Info << "############################################################" << endl;

    label noSnapshots = ROMDict_.lookupOrDefault("noSnapshots", 1);

    label baseSelectionStrategy = ROMDict_.lookupOrDefault("baseSelectionStrategy", 1);

    scalar enLimit = ROMDict_.lookupOrDefault("enLimit", 1.0);

    label numberOfMomentumBases = ROMDict_.lookupOrDefault("numberOfMomentumBases", 1);

    label numberOfFlowResBases = ROMDict_.lookupOrDefault("numberOfFlowResBases", 1);

    PtrList<FlowResDEIM>& flowResDEIM(mROM_().flowResDEIM());

    mROM_().readSnapshots(noSnapshots);
    forAll(flowResDEIM, zoneIndex)
    {
        flowResDEIM[zoneIndex].readSnapshots(noSnapshots);
    }
    pROM_().readSnapshots(noSnapshots);

    mROM_().createBases
    (
        baseSelectionStrategy,
        enLimit,
        numberOfMomentumBases
    );

    mROM_().saveBases();
    mROM_().saveEigenValues();

    forAll(flowResDEIM, zoneIndex)
    {
        flowResDEIM[zoneIndex].createBases
        (
            baseSelectionStrategy,
            enLimit,
            numberOfFlowResBases
        );

        flowResDEIM[zoneIndex].selectCells();
        flowResDEIM[zoneIndex].printSelected();

        flowResDEIM[zoneIndex].selectMaterialProperties();

        flowResDEIM[zoneIndex].saveBases();
        flowResDEIM[zoneIndex].saveEigenValues();
    }

    pROM_().createBases
    (
        mROM_().rightSingVectors(),
        mROM_().eigenValues(),
        1,
        enLimit,
        mROM_().ownBase().size()
    );

    pROM_().saveBases();
    pROM_().saveEigenValues();

    mROM_().initSolVectorsL2Projection();
    mROM_().initApproximateSolution();

    pROM_().initSolVectorsL2Projection();
    pROM_().initApproximateSolution();

    mROM_().reconstructApproxSol();
    pROM_().reconstructApproxSol();

    mROM_().appFullSolution().write();
    pROM_().appFullSolution().write();

    initNonlinearSystem();
}

void Foam::OneEqLamSteadyFluidROMSolver::readReducedBases()
{
    Info << "############################################################" << endl;
    Info << "Reading Reduced Bases for momentum/pressure equations." << endl;
    Info << "############################################################" << endl;

    label numberOfMomentumBases = ROMDict_.lookupOrDefault("numberOfMomentumBases", 1);

    label numberOfFlowResBases = ROMDict_.lookupOrDefault("numberOfFlowResBases", 1);

    PtrList<FlowResDEIM>& flowResDEIM(mROM_().flowResDEIM());

    mROM_().readBases(numberOfMomentumBases);
    forAll(flowResDEIM, zoneIndex)
    {
        flowResDEIM[zoneIndex].readBases(numberOfFlowResBases);
    }
    pROM_().readBases(numberOfMomentumBases);

    mROM_().initSolVectorsL2Projection();
    mROM_().initApproximateSolution();

    pROM_().initSolVectorsL2Projection();
    pROM_().initApproximateSolution();

    mROM_().reconstructApproxSol();
    pROM_().reconstructApproxSol();

    mROM_().appFullSolution().write();
    pROM_().appFullSolution().write();

    initNonlinearSystem();
}

void Foam::OneEqLamSteadyFluidROMSolver::initNonlinearSystem()
{
    Info << "############################################################" << endl;
    Info << "Initializing nonlinear system." << endl;
    Info << "############################################################" << endl;

    nUCoeffs_ = mROM_().solution()->size();

    systemMatrix_.setSize(nUCoeffs_, nUCoeffs_);

    systemRHS_.resize(nUCoeffs_, 0.0);

    combinedSolution_ = *(mROM_().solution());

    combinedSolutionOld_ = combinedSolution_;
}

scalar Foam::OneEqLamSteadyFluidROMSolver::solve()
{
    Info << "############################################################" << endl;
    Info << "Solving One-equation Fluid ROM." << endl ;
    Info << "############################################################" << endl;

    fillCombinedSolution();

    List<scalar> diffVec(combinedSolution_);
    List<scalar> initialSolution(combinedSolution_);

    label itIndex(0);

    // Info << "Updating component solutions." << endl;
    // updateComponentSolution();

    Info << "Entering Non-linear solve." << endl;


    // PtrList<FlowResDEIM>& flowResDEIM(mROM_().flowResDEIM());
    // forAll(flowResDEIM, frI)
    // {
    //     flowResDEIM[frI].testFlowResDEIM(&mROM_().ownSnaps());
    // }


    relDiff_ = 1.0;
    while(relDiff_ > tolerance_)
    {
        Info << "Setting up system." << endl;

        setupNonlinearSystem();

        fillCombinedSolution();

        combinedSolutionOld_ = combinedSolution_;

        Info << "Solving linear system." << endl;

        // Info << systemMatrix_ <<endl;

        Foam::solve
        (
            combinedSolution_,
            SquareMatrix<scalar>(systemMatrix_),
            systemRHS_
        );

        Info << combinedSolution_ <<endl;

        updateComponentSolution();

        diffVec = combinedSolutionOld_ - combinedSolution_;

        scalar diff = 0.0;
        scalar vecNorm = 0.0;
        forAll(diffVec, entryIndex)
        {
            diff += pow(diffVec[entryIndex],2);
            vecNorm += pow(combinedSolution_[entryIndex],2);
        }

        if(vecNorm > SMALL)
            relDiff_ = sqrt(diff/vecNorm);
        else
            relDiff_ = 0.0;

        Info << " Rel. Diff: " << relDiff_ << " " << itIndex << endl;

        itIndex++;
    }

    Info << "Num. of it. needed for the solution of the red. sys.: " <<
            itIndex << endl;

    diffVec = initialSolution - combinedSolution_;
    scalar diffFinal = 0.0;
    scalar vecNormFinal = 0.0;
    forAll(diffVec, entryIndex)
    {
        diffFinal += pow(diffVec[entryIndex],2);
        vecNormFinal += pow(initialSolution[entryIndex],2);
    }
    if(vecNormFinal > SMALL)
        diffFinal = sqrt(diffFinal/vecNormFinal);
    else
        diffFinal = 1.0;

    return(diffFinal);
}

// ************************************************************************* //
