/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    TurbulentFluidBeff

Description
    Implementation of coupled forward neutronics, adjoint neutronics and 
    fluid dynamics solvers in the turbulent flow domain. Additionally, the 
    effective delayed neutron fraction is determined.

Author
    Peter German, peter.german@tamu.edu, grmnptr@gmail.com

SourceFiles
    TurbulentFluidBeff.C

\*---------------------------------------------------------------------------*/

#ifndef TurbulentFluidBeff_H
#define TurbulentFluidBeff_H

#include "fvCFD.H"
#include "RectangularMatrix.H"

#include "FluidSolver.H"
#include "NeutroSolver.H"
#include "Solver.H"

#include "PressureROM.H"
#include "MomentumROM.H"
#include "FlowResDEIM.H"    

#include "FluidPropertiesDataBase.H"
#include "PorousMediumDataBase.H"

#include "OneEqTurbSteadyFluidROMSolver.H"
#include "TwoEqTurbSteadyFluidROMSolver.H"
#include "DiffusionEv.H"
#include "AdjointDiffusionEv.H"
#include "BeffHandler.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class Reduced Model Declaration
\*---------------------------------------------------------------------------*/

class TurbulentFluidBeff : public Solver
{

private:

    // Private Attributes

        //- Reference to the runtime control (easier synchronisation with FOM)
        Time& runTime_;
    
        //- Dictionary holding necessary information about the ROM run settings
        IOdictionary& ROMDict_;

        //- Reference to fluid mesh
        fvMesh& fluidMesh_;

        //- Reference to neutro mesh
        fvMesh& neutroMesh_;
    
        //- Residual in the current iteration
        scalar residual_;
    
        //- Residual in the previous iteration
        scalar residualOld_;
    
        //- Tolerance for the iteration
        scalar tolerance_;

        //- Forward neutronics solver
        autoPtr<DiffusionEv> forwardEv_;
    
        //- Adjoint neutronics solver
        autoPtr<AdjointDiffusionEv> adjointEv_;
    
        //- Handler used to generate Beff
        autoPtr<BeffHandler> beffHandler_;

        //- Fluid ROM solver
        autoPtr<FluidSolver> fluidSolver_;

        //- Pointer list to transformed velocity bases
        PtrList<volVectorField> transformedUBase_;

        //- Number of fluid equations to solve
        label noFluidEqs_;

        //- Typo of fluid solver to use (steady/transient)
        word fluidSolverType_;

        //- Typo of neutro solver to use (eigenvalue/transient)
        word neutroSolverType_;

    // Private functions

        void setupAlphatRBFInterpolation();

        void readAlphatRBFInterpolation();
    
public:

    // Constructors

        TurbulentFluidBeff
        (
            Time& runTime,
            IOdictionary& ROMDict,
            fvMesh& fluidMesh,
            fvMesh& neutroMesh
        );

    // Destructor

        ~TurbulentFluidBeff();

    // Public Member Functions

       //- Reading inputs from files
        virtual void readInputData() override;

        //- Creating reduced objects (PROM and MROM)
        virtual void createReducedObjects() override;

        //- Linking reduced objects (allow them to see each other)
        virtual void linkReducedObjects() override;

        //- Creating the bases in the reduced bases
        virtual void createReducedBases() override;

        //- Redaing the bases from files
        virtual void readReducedBases() override;

        //- Creating the reduced operators
        virtual void createReducedOperators() override;

        //- Reading the reduced operators
        virtual void readReducedOperators() override;

        //- stepping solution and reconstructing approximate fields
        virtual void timeUpdateAndReconstruct() override;

        //- Solving the nonlinear system (Picard iteration)
        virtual scalar solve() override;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
