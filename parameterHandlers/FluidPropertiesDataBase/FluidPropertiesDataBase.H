/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2013-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    FluidPropertiesDataBase

Description
    Class hangling the I/O of fluid dynamics model parameters.

Author
    Peter German, peter.german@tamu.edu, grmnptr@gmail.com

SourceFiles
    FluidPropertiesDataBase.C

\*---------------------------------------------------------------------------*/

#ifndef FluidPropertiesDataBase_H
#define FluidPropertiesDataBase_H

#include "IFstream.H"
#include "fvCFD.H"
#include "fvMatrixExt.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class FluidPropertiesDataBase Declaration
\*---------------------------------------------------------------------------*/


class FluidPropertiesDataBase
{

private:

    //- Private data

        //- Prandt number
        scalar Pr_;

        //- Density
        scalar rho_;

        //- Specific heat
        scalar cp_;

        //-Molecular viscosity
        scalar mu_;

        //- Gravitational acceleration vector
        vector g_;

        //- Reference temperature
        scalar Tref_;

        //- Reference density
        scalar rhoRef_;

        //- Reference heights
        scalar hRef_;

        //- Volumetric expansion coefficient
        scalar beta_;

    // Private Member Functions

public:

    // Constructors

        FluidPropertiesDataBase();

    // Destructor

        ~FluidPropertiesDataBase();

    // Public Member Functions

        void readThermoPhysicalProperties();

        void readBoussinesqProperties();

        scalar Pr() const
        {
            return Pr_;
        }

        scalar rho() const
        {
            return rho_;
        }

        scalar mu() const
        {
            return mu_;
        }

        scalar cp() const
        {
            return cp_;
        }

        vector g() const
        {
            return g_;
        }

        scalar Tref() const
        {
            return Tref_;
        }

        scalar rhoRef() const
        {
            return rhoRef_;
        }

        scalar hRef() const
        {
            return hRef_;
        }

        scalar beta() const
        {
            return beta_;
        }
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
