#!/usr/bin/python
import os
import numpy as np
import math

# The number of samples to create
noSamples = 5
startIndex = 0

# Defining the fields it analyze
fieldsToCompare = ["UDarcy",
                  "p_rgh",
                  "fluxStar0",
                  "fluxStar1",
                  "fluxStar2",
                  "fluxStar3",
                  "fluxStar4",
                  "fluxStar5",
                  "precStar0",
                  "precStar1",
                  "precStar2",
                  "precStar3",
                  "precStar4",
                  "precStar5",
                  "precStar6",
                  "precStar7"]

shift = [0,1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

shiftIndex = [i*0 for i in shift]

# Weighting function name (field that it reads). It should be in the
# directory as the FOM.
weightingFunction = "V"

# Defining the directories of the files of FOM
FOMDir = os.getcwd()+"/ROMFiles/testFOM"

# Defining the directories of the files of ROM
ROMDir = os.getcwd()+"/ROMFiles/testROM"
