# Steady Laminar Fluid Dynamics and Neutronics Example 

<h2> Overview </h2>

The goal of this tutorial is to showcase how to create a POD-RB ROM for a multigroupd diffusion 
k-eigenvalue problem coupled to a fluid dynamics simulation
using GeN-ROM. We chose a 2D model of the Molten Salt Fast Reactor (MSFR). The problem has 
six energy groups, involves the drift of the precursors and assumes laminar fluid flow. For more 
information on the problem, see [[1]](#german2021genrom). The fundamental steps needed to generate a POD-RB ROM 
for coupled diffusion k-eigenvalue and fluid dynamics problems using GeN-ROM are:
* **Generating snapshots:** Using a predefined desing, we solve a higher 
  fidelity model at different model parameter values
  and save certain fields to create
  global basis functions for the ROM generation. 
* **Generating reduced operators:** We use the snapshots to extract the global basis functions and to reduce the 
  dimensionality of the operators of the higher fidelity model. We then save the basis 
  functions and reduced operators for further use.
  With this, the ROM is ready to use.
* **Testing the ROM:**
  In this step, we solve the higher fidelity model and the ROM together at multiple new parameter combinations.
  Then, we take results of the ROM and compare them to those of the FOM. We usually comapre based on the 
  mean/maximum relative L<sup>2 </sup> errors over the test set. In this scenario we will compare the
  mean/maximum difference in the effective multiplication as well.

These steps are all performed automatically if the user navigates into the given tutorial folder and executes the 
*Allrun* script:
```
cd tutorials/steady-laminar-fluid-neutro
./Allrun
```
This test case involves 8 uncertain parameters: the diffusion coefficients  
and the fission neutron yield times fission cross section 
of the core region in energy group 1-3 together the dynamic viscosity of the 
fluid and the volumetric force exerted by the pump.
Altogether 10 snapshots are generated together with 5 additional solves to produce samples for 
testing the ROM. The higher fidelity model is executed using 4 threads by default, while the 
ROM solves are executed in serial.
If an error is encountered one may want to clean up the directory before trying to rerun the case using:
```
./Allclean
```
The following subsections discuss the different steps in detail.

<h2> Creating snapshots </h2>

To generate snapshots, we utilize a *python* script which can modify OpenFOAM/GeN-Foam 
input files and collect results. This script is reused for most of the steps in this process.
First we create a copy of the rootCase as:
```
cd tutorials/steady-laminar-fluid-neutro
cp -r rootCase snapshot_generation
cd snapshot_generation
```
Then we generate **10 snapshots** with an **LHS** sampling and save them in the **ROMFiles/snapshots** subdirectory 
by executing the script as follows:
```
python ../../../python/snapshotGenerationv2/snapGeN.py -i snapGenInput -s LHS -ns 10 -d ROMFiles/snapshots -c "./run.sh"
```
In this case the *-c* argument is the command the script uses to execute the case. 
*run.sh* is shell script which executes the case in parallel using 4 threads.
The model parameters which are used in this case (cross sections, diffusion coefficients, 
pumping force, dynamic viscosity) are defined in the *snapGenInput* input file 
(also uses python syntax) together with the solution fields the script should collect. 
Furthermore, we see that the *-s* argument has been used to select the 
sampling for the model parameters. In this scenario we used Latin Hypercube Sampling (LHS).
For further information on the behavior of the script, we refer the reader to:
```
python ../../../python/snapshotGenerationv2/snapGeN.py -h
```

<h2> Generating reduced operators </h2>

Once the snapshots are at our discposal, we can create the global basis functions.
For this, we create a copy of the previous work directory:
```
cp -r snapshot_generation rom_generation
cd rom_generation
```
At this point the user needs to take a look at the *system/ROMDict* dictionary file which 
contains the control parameters for the ROM. To make sure that the basis functions and the operators are 
set to be generated (instead of read). Thus, we set the following two parameters:
```
readOperators false;
readBases false;
```
One can also select the number of basis functions per physics and the type of solvers to use for the ROM. 
To ensure that the ROM is not solved, one can increase *deltaT* in *system/controlDict* to 100.
Once these conditions are met, we generate the ROM by simply executing:
```
GeN-ROM
```

<h2> Creating test samples </h2>

Now we turn our attention to testing the ROM. First, we need the reference samples
from the higher fidelity model. This can be done by copying the previously created 
case directory into a new one:
```
cp -r rom_generation fom_test
cd fom_test
```
Then, exactly like in case of the snapshot generation we use the python script to create **5** test samples
and save them into the **ROMFiles/testFOM** directory. **(Also, if you changed the final time of the 
execution, make sure you reset to the original so the test samples are saved from the same directory!)**
```
python ../../../python/snapshotGenerationv2/snapGeN.py -i snapGenInput -s LHS -ns $no_test -d ROMFiles/testFOM -c "./run.sh" -df processor0/GeN-Foam.dat
```
In this scenario we also would like to save the converged multiplication factor 
of the problem. This means that we need to store the **GeN-Foam.dat** files, which can be achieved by 
adding another argument when calling the python script:
```
-df processor0/GeN-Foam.dat
``` 
which will take the multiplication factor from the folder corresponding to processor 0.

<h2> Testing the ROM </h2>

Finally, the last step of the process involves testing the ROM. 
Luckily we can use the same python script to execute GeN-ROM. We start with the usual process:
```
cp -r fom_test rom_test
cd rom_test
```
Then, one needs to change the read parameters in *system/ROMDict* to ensure that 
the reduced bases and operators are not generated every time GeN-ROM is executed:
```
readOperators true;
readBases true;
```
Once this is done, the user shall set the *deltaT* parameter in *system/controlDict*
to 50 (or whatever the final time is), to ensure that the ROM is executed only once (simple eigenvalue problem).
Following this, we can simply use the python script to collect the ROM results
into the **ROMFiles/testROM** directory:
```
python ../../../python/snapshotGenerationv2/snapGeN.py -i snapGenInputROM -s LHS -ns 5 -d ROMFiles/testROM -c GeN-ROM -df GeN-ROM.dat
```
Note that a different input file has been used for the script that ensures that the 
ROM fields are collected in this scenario. Furthermore, we save the multiplication factors from the ROMs
by storing the **GeN-ROM.dat** files which can be set up using the *-df* argument of the script.

Lastly, to compare the results from the FOM and the ROM in an L<sup>2</sup> sense, we need the 
cell volumes. Luckily, OpenFOAM can be used to generate these as:
```
cd ROMFiles/testFOM
ln -s ../../system .
ln -s ../../constant .
postProcess -func writeCellVolumes -region fluidRegion
postProcess -func writeCellVolumes -region neutroRegion
cd ../..
```

Using these we can call another python script to generate the statistics of the relative L<sup>2</sup> error:
```
python ../../../python/TestingROM/TestROM.py
```
This requires a *testROMInput.py* file in the directory which has been already set up for this tutorial. 
One can modify this file to choose which fields need to be compared and where they are located. 
Additionally, we would like to generate the absolute errors in the effective multiplication factors 
from the corresponding FOM-ROM simulation pairs. Another python script has been provided for this purpose and 
can be called as:
```
python keffTest.py
```
If everything is executed correctly and if the user choses to keep 
the number of modes in the predefined *ROMDict*, one should be able to observe the following results:
```
-------------------------------------------------------------------------------
 Reading controls from testROMInput.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 Comparing the fields in the given directories.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 Printing statistics.
-------------------------------------------------------------------------------
UDarcy mean error: 0.0004900788651048532, max error: 0.0006002773228245142
p_rgh mean error: 0.0009572706580881025, max error: 0.0013554670493194023
fluxStar0 mean error: 0.0006645569321359589, max error: 0.0012699525631366304
fluxStar1 mean error: 0.000897835355638598, max error: 0.001974359036592485
fluxStar2 mean error: 0.0010350542514030755, max error: 0.002330905506534517
fluxStar3 mean error: 0.0009775314340961167, max error: 0.0022099852850078856
fluxStar4 mean error: 0.0009285553785472938, max error: 0.0021043182956644864
fluxStar5 mean error: 0.0009067399916846714, max error: 0.0020567506516026117
precStar0 mean error: 0.002024113653645821, max error: 0.0030284206691675824
precStar1 mean error: 0.003221514853096419, max error: 0.005310382918941323
precStar2 mean error: 0.0038777752333487457, max error: 0.006427659975089762
precStar3 mean error: 0.005506261181275745, max error: 0.007217350364613075
precStar4 mean error: 0.005330906640945171, max error: 0.006887664409199186
precStar5 mean error: 0.005756861289192165, max error: 0.00840478444515273
precStar6 mean error: 0.0048853683121069325, max error: 0.008309749838122438
precStar7 mean error: 0.0033410218045382733, max error: 0.006458759766617423
```
for relative global error, and
```
Average dev (pcm):  0.3329999999945876
Maximum dev (pcm):  0.5299999999985872

```
for the effective multiplication factor. Lastly, the script outputs the 
relative L<sup>2</sup> differences in .txt files for every single sample with the following notation:
```
<field_name>Error.txt
```
## References
<a id="german2021genrom">[1]</a> German, P., Tano, M., Fiorina, C., & Ragusa, J. C. (2022). GeN-ROM—An OpenFOAM®-based multiphysics reduced-order modeling framework for the analysis of Molten Salt Reactors. _Progress in Nuclear Energy_, 146, 104148.

