# Tutorials 

<h2>Multigroup Diffusion k-eigenvalue ROM</h2> 

This tutorial showcases the fundamental steps of setting up and testing a ROM for 
standalone, steady-state neutronics problems. A ROM is created for a 3D Light Water Reactor (LWR) 
benchmark core. Click [here](k-eigenvalue/k-eigenvalue.md) for the walkthrough.

<h2>Steady-state Laminar Fluid Dynamics ROM</h2> 

This tutorial showcases the fundamental steps of setting up and testing a ROM for 
standalone fluid dynamics problems. Click [here](steady-laminar-fluid/steady-laminar-fluid.md) for the description.

<h2>Steady-state Laminar Coupled Fluid Dynamics and Neutronics ROM</h2> 

This tutorial showcases the fundamental steps of setting up and testing a ROM for 
a coupled fluid dynamics and neutronics simulation of a Molten Salt Reactor (MSR). 
Click [here](steady-laminar-fluid-neutro/steady-laminar-fluid-neutro.md) for the walkthrough.
