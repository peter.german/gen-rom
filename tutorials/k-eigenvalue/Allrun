#!/bin/sh
cd ${0%/*} || exit 1    # Run from this directory

# Source tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

no_snapshots=20
no_test=5

cases="snapshot_generation fom_test rom_generation rom_test"

runCloneCase()
{
    printf "Cloning $2 to case $1..."
    cp -r $2 $1
    printf "$2 cloned!\n"
}

setROMGeneration()
{
    runCloneCase $1 $2
    printf "Setting up ROM generation...\n"
    foamDictionary $1/system/controlDict -entry deltaT -set 100
    foamDictionary $1/system/controlDict -entry adjustTimeStep -set false
    foamDictionary $1/system/ROMDict -entry readBases -set false
    foamDictionary $1/system/ROMDict -entry readOperators -set false
    printf "Case set!\n\n"
}

setROMTest()
{
    runCloneCase $1 $2
    printf "Setting test cases with ROM...\n"
    foamDictionary $1/system/controlDict -entry deltaT -set 50
    foamDictionary $1/system/controlDict -entry adjustTimeStep -set false
    foamDictionary $1/system/ROMDict -entry readBases -set true
    foamDictionary $1/system/ROMDict -entry readOperators -set true
    printf "Case set!\n\n"
}

for caseName in $cases
do
    printf "Preparing to run case $caseName...\n"

    case "$caseName" in
    "snapshot_generation")
        printf "Preparing snapshot generation...\n"
        runCloneCase $caseName rootCase
        printf "Running snapshot generation...\n"
        cd $caseName
        python ../../../python/snapshotGenerationv2/snapGeN.py -i snapGenInput -s LHS -ns $no_snapshots -d ROMFiles/snapshots -c "./run.sh"
        cd ..
        ;;
    "fom_test")
        printf "FOM test prepared...\n"
        runCloneCase $caseName snapshot_generation
        printf "Running FOM test generation...\n"
        cd $caseName
        python ../../../python/snapshotGenerationv2/snapGeN.py -i snapGenInput -s LHS -ns $no_test -d ROMFiles/testFOM -c "./run.sh" -df processor0/GeN-Foam.dat
        cd ROMFiles/testFOM
        ln -s ../../constant .
        ln -s ../../system .
        postProcess -func writeCellVolumes -region neutroRegion
        cd ../../..
        ;;
    "rom_generation")
        printf "ROM generation prepared...\n"
        setROMGeneration $caseName fom_test
        printf "Running ROM generation...\n"
        cd $caseName
        GeN-ROM
        cd ..
        ;;
    "rom_test")
        printf "ROM test prepared...\n"
        setROMTest $caseName rom_generation
        printf "Running ROM test...\n"
        cd $caseName
        python ../../../python/snapshotGenerationv2/snapGeN.py -i snapGenInputROM -s LHS -ns $no_test -d ROMFiles/testROM -c GeN-ROM -df GeN-ROM.dat
        python ../../../python/TestingROM/TestROM.py
        python keffTest.py
        cd ..
        ;;
    esac

done

#------------------------------------------------------------------------------
