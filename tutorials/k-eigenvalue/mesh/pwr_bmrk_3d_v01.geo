// ----------------------------------------------------------------------------
// 3D Model of PWR benchmark for k-eff
// calculations. Every dimension is in cms.
// ----------------------------------------------------------------------------

factor = 2;

lc = 1.5;
epsilon = 1e-2;

assemblyWidth = {10, 20, 20, 20, 20, 20, 20, 20, 20};

noAssembliesInEachRow = {9, 9, 9, 9, 8, 8, 7, 6, 4};

cellPerLayer = {1*factor, 2*factor, 2*factor, 2*factor, 2*factor, 2*factor, 2*factor, 2*factor, 2*factor};

// ----------------------------------------------------------------------------
// Material IDs:
// ----------------------------------------------------------------------------
// Fuel1: 0
// Fuel1+Rod: 1
// Fuel2: 2
// Reflector: 3
// Reflector+Rod: 4

materialList = {0, 1, 2, 3, 4};

Delete Physicals;
For materialIndex In {0:#materialList[]-1}
	Physical Volume(materialList[materialIndex]) = {};
EndFor

// ----------------------------------------------------------------------------
// Reactor layout
// ----------------------------------------------------------------------------

layerThicknesses = {20, 260, 80, 20};
layerResolution = {1*factor, 13*factor, 4*factor, 1*factor};

reactorMap2D~{0} = {3, 3, 3, 3, 3, 3, 3, 3, 3,
	                  3, 3, 3, 3, 3, 3, 3, 3, 3,
	                  3, 3, 3, 3, 3, 3, 3, 3, 3,
	                  3, 3, 3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3};

reactorMap2D~{1} = {1, 0, 0, 0, 1, 0, 0, 2, 3,
	                  0, 0, 0, 0, 0, 0, 0, 2, 3,
	                  0, 0, 0, 0, 0, 0, 2, 2, 3,
	                  0, 0, 0, 0, 0, 0, 2, 3, 3,
                    1, 0, 0, 0, 1, 2, 2, 3,
                    0, 0, 0, 0, 2, 2, 3, 3,
                    0, 0, 2, 2, 2, 3, 3,
                    2, 2, 2, 3, 3, 3,
                    3, 3, 3, 3};

reactorMap2D~{2} = {1, 0, 0, 0, 1, 0, 0, 2, 3,
	                  0, 0, 0, 0, 0, 0, 0, 2, 3,
	                  0, 0, 1, 0, 0, 0, 2, 2, 3,
	                  0, 0, 0, 0, 0, 0, 2, 3, 3,
                    1, 0, 0, 0, 1, 2, 2, 3,
                    0, 0, 0, 0, 2, 2, 3, 3,
                    0, 0, 2, 2, 2, 3, 3,
                    2, 2, 2, 3, 3, 3,
                    3, 3, 3, 3};

reactorMap2D~{3} = {4, 3, 3, 3, 4, 3, 3, 3, 3,
	                  3, 3, 3, 3, 3, 3, 3, 3, 3,
	                  3, 3, 4, 3, 3, 3, 3, 3, 3,
	                  3, 3, 3, 3, 3, 3, 3, 3, 3,
                    4, 3, 3, 3, 4, 3, 3, 3,
                    3, 3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3};

// ----------------------------------------------------------------------------
// Preparing a bottom layer for the 3D extraction
// ----------------------------------------------------------------------------

p0 = newp; Point(p0) = {0, 0, 0, lc};

leftLines = {};
endPoint = p0;

For rowIndex In {0:#noAssembliesInEachRow[]-1}

    out[]= Extrude {0, -assemblyWidth[rowIndex], 0}
                   {Point{endPoint};
                    Layers{cellPerLayer[rowIndex]};
                    Recombine;};
    leftLines += out[1];
    endPoint = out[0];

EndFor

bottomFaces ={};
For rowIndex In {0:#noAssembliesInEachRow[]-1}

    lineToExtrude = leftLines[rowIndex];

    For columnIndex In {0:noAssembliesInEachRow[rowIndex]-1}
    	out[]=Extrude {assemblyWidth[columnIndex], 0, 0}
    	              {Line{lineToExtrude};
    	               Layers{cellPerLayer[columnIndex]};
    	               Recombine;};
    	lineToExtrude = out[0];
    	bottomFaces += out[1];
    EndFor

EndFor

// ----------------------------------------------------------------------------
// Creating the 3D geometry with the boundary surfaces
// ----------------------------------------------------------------------------

Physical Surface("bottom") = bottomFaces[];
Physical Surface("symmetry1") = {};
Physical Surface("symmetry2") = {};
Physical Surface("cylinder") = {};

volumes~{0} = {};
volumes~{1} = {};
volumes~{2} = {};
volumes~{3} = {};
volumes~{4} = {};

surfacesToExtrude[] = bottomFaces[];
For layerIndex In {0:#layerThicknesses[]-1}

	faceIndex=0;
	For rowIndex In {0:#noAssembliesInEachRow[]-1}
	    For columnIndex In {0:noAssembliesInEachRow[rowIndex]-1}

			  out[]=Extrude {0, 0, layerThicknesses[layerIndex]}
		              	  {Surface{surfacesToExtrude[faceIndex]};
		                   Layers{layerResolution[layerIndex]};
		                   Recombine;};
		    volumes~{reactorMap2D~{layerIndex}[faceIndex]} += out[1];
		    surfacesToExtrude[faceIndex] = out[0];

		    If(columnIndex==0)
    	    	Physical Surface("symmetry1") += out[2];
    		EndIf

    		If(rowIndex==0)
    	    	Physical Surface("symmetry2") += out[5];
    		EndIf

    		If(rowIndex<#noAssembliesInEachRow[]-1)
    			If(columnIndex>noAssembliesInEachRow[rowIndex+1]-1)
    	    		Physical Surface("cylinder") += out[3];
    	    	EndIf
    		EndIf

    		If(rowIndex==#noAssembliesInEachRow[]-1)
    			Physical Surface("cylinder") += out[3];
    		EndIf

		    faceIndex = faceIndex + 1;

		EndFor

		Physical Surface("cylinder") += out[4];

	EndFor

EndFor

Physical Surface("top") = surfacesToExtrude[];

Physical Volume("fuel1") = volumes~{0}[];
Physical Volume("fuel_control_rod") = volumes~{1}[];
Physical Volume("fuel2") = volumes~{2}[];
Physical Volume("moderator") = volumes~{3}[];
Physical Volume("moderator_control_rod") = volumes~{4}[];
