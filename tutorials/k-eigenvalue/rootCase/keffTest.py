#!/usr/bin/python
import os
import sys
import numpy as np
from statistics import mean

FOMName = "GeN-Foam.dat"
ROMName = "GeN-ROM.dat"

folderName = "ROMFiles/datFiles/"

numberofCases = 5

diff = []
for i in range(numberofCases):

  FOMFile = np.genfromtxt(folderName+str(i)+"/"+FOMName,delimiter=";", skip_header=2)
  ROMFile = np.genfromtxt(folderName+str(i)+"/"+ROMName,delimiter=";", skip_header=1)

  diff.append(abs(FOMFile[-1][1]-ROMFile[1]))

print("Average dev (pcm): ", mean(diff)*1e5)
print("Maximum dev (pcm): ", max(diff)*1e5)
