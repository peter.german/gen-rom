#!/usr/bin/python
import os
import numpy as np
import math

# The number of samples to create 
noSamples = 5
startIndex = 0

# Defining the fields it analyze
fieldsToCompare = ["UDarcy","p_rgh"]

shift = [0,1]

shiftIndex = [0,0]

# Weighting function name (field that it reads). It should be in the
# directory as the FOM. 
weightingFunction = "V"

# Defining the directories of the files of FOM
FOMDir = os.getcwd()+"/ROMFiles/testFOM"

# Defining the directories of the files of ROM
ROMDir = os.getcwd()+"/ROMFiles/testROM"
