#!/bin/bash

rm -r processor* 0.* 1* 2* 3* 4* 5* 6* 7* 8* 9*

decomposePar -region fluidRegion
decomposePar -region neutroRegion
decomposePar -region thermoMechanicalRegion

mpirun -np 4 GeN-Foam -parallel

reconstructPar -region fluidRegion
